/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QtGui"

#include "QTmWidget.h"

QTmWidget::QTmWidget(QWidget *parent)
    : QWidget(parent)
    , m_surface(0)
{
#ifdef QT_DEBUG_TM_SURFACE
    qDebug () << "QTmWidget::QTmWidget:" << "(Object constructed) at" << this;
#endif
    setFocusPolicy(Qt::StrongFocus);
}

/*!
    Empty
*/
QTmWidget::~QTmWidget() {
    deallocateSurface();
#ifdef QT_DEBUG_TM_SURFACE
    qDebug () << "QTmWidget::~QTmWidget:" << "(Object destroyed) at" << this;
#endif
}


/*!
  Reimplemented from QWidget::sizeHint()
*/
QSize QTmWidget::sizeHint() {
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmWidget::sizeHint:" << "Provide Size Hint";
#endif
    QReadLocker rLocker(&m_surfaceResizeLock);
    if(m_surface != 0)
        return m_surface->size();
    else
        return QSize();
}


/*!
    Empty
*/
uchar* QTmWidget::allocateSurface(const QSize &s, QTmGlobal::TmColorFormat colorFormat) {
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmWidget::allocateSurface:" << "Allocate surface (" << s << "," << colorFormat << ")";
#endif

    if(m_surface && m_surfaceTmColorFormat == colorFormat &&
            m_surface->width() == s.width() &&
            m_surface->height() == s.height())
        return (uchar*) m_surface->bits();

    QImage *tmp;
    switch (colorFormat) {
    case QTmGlobal::RGB888:
        tmp = new QImage(s, QImage::Format_RGB32);
        break;
    case QTmGlobal::RGB565:
        tmp = new QImage(s, QImage::Format_RGB16);
        break;
    case QTmGlobal::RGB555:
        tmp = new QImage(s, QImage::Format_RGB555);
        break;
    case QTmGlobal::RGB444:
    case QTmGlobal::RGB343:
        tmp = new QImage(s, QImage::Format_RGB444);
        break;
    default:
        qWarning() << "QTmWidget:" << "Unknown color format";
        return 0;
    }
    //Fill in background color
#ifdef QT_DEBUG_TM_SURFACE
    tmp->fill(255);
#else
    tmp->fill(0);
#endif

    QWriteLocker wLocker(&m_surfaceResizeLock);
    if (m_surface) delete m_surface;

    m_surfaceTmColorFormat = colorFormat;
    m_surface = tmp;

    m_transform = QTmSurface::getTransformation(size(), m_surface->size());
    m_transformInverted = m_transform.inverted();

    return (uchar*) m_surface->bits();
}

/*!
    Empty
*/
void QTmWidget::deallocateSurface()
{
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmWidget::deallocateSurface:" << "Deallocate surface";
#endif
    QWriteLocker wLocker(&m_surfaceResizeLock);
    if (m_surface) {
        delete m_surface;
        m_surface = 0;
    }
    m_transform.reset();
    m_transformInverted.reset();
}

/*!
    Empty
*/
void QTmWidget::updateSurface(const QRect &updateRect) {
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmWidget::updateSurface:" << "Framebuffer Update";
#endif
    update(m_transform.mapRect(updateRect));
}

/*!
  Reimplemented from QWidget::paintEvent()
  Sends the paint \a event to the internal handler.
*/
void QTmWidget::paintEvent(QPaintEvent *event) {
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmWidget::paintEvent:" << "Paint Event";
#endif
    QReadLocker rLocker(&m_surfaceResizeLock);
    if (m_surface && isVisible()) {
        QPainter p(this);
        p.setRenderHints(QPainter::SmoothPixmapTransform, true);
//        p.setRenderHints(QPainter::Antialiasing |
//                               QPainter::SmoothPixmapTransform |
//                               QPainter::HighQualityAntialiasing, true);
        QRect tRect = m_transformInverted.mapRect(event->rect()).intersect(m_surface->rect());
        tRect.adjust(-1, -1, 1, 1);
        // set transformation
        p.setTransform(m_transform);
        // draw surface
        p.drawImage(tRect, (*m_surface), tRect);
    }
}

/*!
  Reimplemented from QWidget::mousePressEvent()
  Sends the mouse move \a event to the internal handler.
*/
void QTmWidget::mousePressEvent(QMouseEvent *event) {
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmWidget::mousePressEvent:" << "Mouse Press Event" << event->pos();
#endif
    QTmPointerEvent pointerEvent(m_transformInverted.map(event->pos()), true);
    emit tmPointerEvent(&pointerEvent);
}

/*!
  Reimplemented from QWidget::mouseReleaseEvent()
  Sends the mouse move \a event to the internal handler.
*/
void QTmWidget::mouseReleaseEvent(QMouseEvent *event) {
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmWidget::mouseReleaseEvent:" << "Mouse Release Event" << event->pos();
#endif
    QTmPointerEvent pointerEvent(m_transformInverted.map(event->pos()), false);
    emit tmPointerEvent(&pointerEvent);
}

/*!
  Reimplemented from QWidget::mouseMoveEvent()
  Sends the mouse move \a event to the internal handler.
*/
void QTmWidget::mouseMoveEvent(QMouseEvent *event) {
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmWidget::mouseMoveEvent:" << "Mouse Move Event";
#endif
    QTmPointerEvent pointerEvent(m_transformInverted.map(event->pos()), true);
    emit tmPointerEvent(&pointerEvent);
}

/*!
  Reimplemented from QWidget::keyPressEvent()
  Modifies the key if ShiftModifier flag is set in the \a event is pressed and sends it to the VNC client.
*/
void QTmWidget::keyPressEvent(QKeyEvent *event) {
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmWidget::keyPressEvent:" << "Key Press Event" << event->key();
#endif
    QTmKeyEvent keyEvent(QTmSurface::translateKey(event->key(), event->modifiers()), true);
    emit tmKeyEvent(&keyEvent);
}

/*!
  Reimplemented from QWidget::keyReleaseEvent()
  Sends the \a event key to the VNC client.
*/
void QTmWidget::keyReleaseEvent(QKeyEvent *event) {
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmWidget::keyReleaseEvent:" << "Key Release Event";
#endif
    QTmKeyEvent keyEvent(QTmSurface::translateKey(event->key(), event->modifiers()), false);
    emit tmKeyEvent(&keyEvent);
}

/*!
  Reimplemented from QWidget::keyReleaseEvent()
  Sends the \a event key to the VNC client.
*/
void QTmWidget::resizeEvent(QResizeEvent* event) {
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmWidget::resizeEvent:" << "New size" << event->size();
#endif
    QWidget::resizeEvent(event);

    QWriteLocker wLocker(&m_surfaceResizeLock);
    if (!m_surface)
        return;

    m_transform = QTmSurface::getTransformation(event->size(), m_surface->size());
    m_transformInverted = m_transform.inverted();
}
