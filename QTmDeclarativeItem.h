/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#ifndef QTMDECLARATIVEITEM_H
#define QTMDECLARATIVEITEM_H

#include <QDeclarativeItem>
#include <QReadWriteLock>
#include "QTmRfbClient.h"
#include "QTmAppInfo.h"
#include "QTmDisplayInfo.h"
#include "QTmSurface.h"

class QTmDeclarativeItem : public QDeclarativeItem, QTmSurface {

    Q_OBJECT

public:
    QTmDeclarativeItem(QDeclarativeItem *parent = 0);
    ~QTmDeclarativeItem();

    virtual uchar* allocateSurface(const QSize& s, QTmGlobal::TmColorFormat colorFormat);
    virtual void deallocateSurface();
    virtual void updateSurface(const QRect& updateRect);

    virtual void  paint(QPainter *, const QStyleOptionGraphicsItem *, QWidget *);

    virtual void  mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void  mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    virtual void  mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    virtual void  keyPressEvent(QKeyEvent *event);
    virtual void  keyReleaseEvent(QKeyEvent *event);

    virtual void  geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry);

    QPoint surfaceToSource(QPoint point);

signals:
    void tmPointerEvent(QTmPointerEvent *pointerEvent);
//    void tmTouchEvent(QList<touchEvent_t> *list);
    void tmKeyEvent(QTmKeyEvent *keyEvent);

private:

    QImage                   *m_surface;            // If you copy this, do a deep-copy ... shallow-copy will stop working eventualy
    QTmGlobal::TmColorFormat  m_surfaceTmColorFormat;
    QReadWriteLock            m_surfaceResizeLock;
    QRectF                    m_myRect;
    QTransform                m_transform;
    QTransform                m_transformInverted;
};

#endif // QTMDECLARATIVEITEM_H
