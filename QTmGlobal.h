/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#ifndef QTMGLOBAL_H
#define QTMGLOBAL_H

#if defined(QT_BUILD_TERMINALMODE_LIB)
#define Q_TM_EXPORT Q_DECL_EXPORT
#else
#define Q_TM_EXPORT Q_DECL_IMPORT
#endif

#include <QUrl>
#include <QObject>
#include <QMetaType>

#ifdef QT_DECLARATIVE_LIB
#include <QtDeclarative>
#endif

//Specifies the relative local path where the icons received from the
//Terminal Mode Server are stored
#define QT_TM_CACHE_PATH  "./cache/"

class QTmGlobal : public QObject {
    Q_OBJECT
    Q_ENUMS(TmAppCategory)
    Q_ENUMS(TmVisualCategory)
    Q_ENUMS(TmVisualRules)
    Q_ENUMS(TmAudioCategory)
    Q_ENUMS(TmTrustLevel)
    Q_ENUMS(TmStatus)
    Q_ENUMS(TmResourceStatus)
    Q_ENUMS(TmDirection)
    Q_ENUMS(TmColorFormat)
    Q_ENUMS(TmRemoteProtocol)

public:
    explicit QTmGlobal(QObject *parent = 0); /* {;}*/

    enum TmDeviceStatus {
        statusUnknown  = 0,
        statusDisabled = 2,
        statusEnabled  = 3
    };

    enum TmRotationStatus {
        rotationUnknown  = 0,
        rotationAbs0     = 4,
        rotationAbs90    = 5,
        rotationAbs180   = 6,
        rotationAbs270   = 7
    };

    enum TmOrientationStatus {
        orientationUnknown   = 0,
        orientationLandscape = 2,
        orientationPortrait  = 3
    };

    enum TmDeviceElement {
        keyLock,
        deviceLock,
        screenSaver,
        nightMode,
        voiceInput,
        micInput,
        distraction
    };

    enum TmAppCategory {
        AppUnknown                 = 0x00000000,

        AppUi                      = 0x00010000,
        AppUiHomeScreen            = 0x00010001,
        AppUiMenu                  = 0x00010002,
        AppUiNotification          = 0x00010003,
        AppUiApplicationList       = 0x00010004,
        AppUiSettings              = 0x00010005,

        AppPhone                   = 0x00020000,
        AppPhoneContactList        = 0x00020001,
        AppPhoneCallLog            = 0x00020002,

        AppMedia                   = 0x00030000,
        AppMediaMusic              = 0x00030001,
        AppMediaVideo              = 0x00030002,
        AppMediaGaming             = 0x00030003,
        AppMediaImage              = 0x00030004,

        AppMessaging               = 0x00040000,
        AppMessagingSms            = 0x00040001,
        AppMessagingMms            = 0x00040002,
        AppMessagingEmail          = 0x00040003,

        AppNavigation              = 0x00050000,

        AppBrowser                 = 0x00060000,
        AppApplicationStore        = 0x00060001,

        AppProductivity            = 0x00070000,
        AppProductivityViewer      = 0x00070001,
        AppProductivityEditor      = 0x00070002,

        AppInformation             = 0x00080000,
        AppInfoNews                = 0x00080001,
        AppInfoWeather             = 0x00080002,
        AppInfoStocks              = 0x00080003,
        AppInfoTravel              = 0x00080004,
        AppInfoSports              = 0x00080005,
        AppInfoClock               = 0x00080006,

        AppSocialNetworking        = 0x00090000,

        AppPim                     = 0x000A0000,
        AppPimCalendar             = 0x000A0001,
        AppPimNotes                = 0x000A0002,

        AppUiLess                  = 0xF0000000,
        AppUiLessAudioServer       = 0xF0000001,
        AppUiLessAudioClient       = 0xF0000002,
        AppUiLessVoiceCommand      = 0xF0000010,

        AppSystem                  = 0xFFFF0000,
        AppSystemPinDeviceUnlock   = 0xFFFF0001,
        AppSystemPinBluetooth      = 0xFFFF0002,
        AppSystemPassword          = 0xFFFF000F,
        AppSystemVoiceConfirmation = 0xFFFF0010,
    };

    enum TmVisualCategory {
        VisualText                 = 0x00000001,
        VisualVideo                = 0x00000002,
        VisualImage                = 0x00000004,
        VisualVectorGraphics       = 0x00000008,
        VisualThreeDGraphics       = 0x00000010,
        VisualUserInterface        = 0x00000020,
        VisualCarMode              = 0x00010000,
        VisualMisc                 = 0x80000000,
    };


    enum TmVisualRules {
        VisualMinFontSize          = 0x00000001,
        VisualNoVideo              = 0x00000002,
        VisualNoAutoScrolling      = 0x00000004,
        VisualMaxFeedbackTime      = 0x00000008,
    };

    enum TmAudioCategory {
        AudioPhone                 = 0x00000001,
        AudioMediaOut              = 0x00000002,
        AudioMediaIn               = 0x00000004,
        AudioVoiceCommandOut       = 0x00000008,
        AudioVoiceCommandIn        = 0x00000010,
        AudioMisc                  = 0x80000000,
    };

    enum TmTrustLevel {
        TrustUnknown               = 0x0000,
        TrustUser                  = 0x0040,
        TrustSelfRegistered        = 0x0060,
        TrustRegistered            = 0x0080,
        TrustCertified             = 0x00A0,
    };

    enum TmStatus {
        StatusUnknown,
        StatusForeground,
        StatusBackground,
        StatusNotrunning,
    };

    enum TmResourceStatus {
        ResourceFree,
        ResourceBusy,
        ResourceNA,
    };

    enum TmDirection {
        DirectionIn,
        DirectionOut,
        DirectionBi,
        DirectionNone,
    };

    enum TmColorFormat {
        RGBNone,
        RGB888,
        RGB565,
        RGB555,
        RGB444,
        RGB343,
        Gray16,
        Gray8
    };

    enum TmRemoteProtocol {
        VNC,
        RTP,
        BT_A2DP,
        BT_HFP,
        DAP,
        NONE,
        PROPRIETARY,
    };

    enum TmKeyEvent {
        XK_Knob_2D_0_shift_right       = 0x30000000,
        XK_Knob_2D_0_shift_left        = 0x30000001,
        XK_Knob_2D_0_shift_up          = 0x30000002,
        XK_Knob_2D_0_shift_up_right    = 0x30000003,
        XK_Knob_2D_0_shift_up_left     = 0x30000004,
        XK_Knob_2D_0_shift_down        = 0x30000005,
        XK_Knob_2D_0_shift_down_right  = 0x30000006,
        XK_Knob_2D_0_shift_down_left   = 0x30000007,
        XK_Knob_2D_0_shift_push        = 0x30000008,
        XK_Knob_2D_0_shift_pull        = 0x30000009,
        XK_Knob_2D_0_rotate_x          = 0x3000000A,
        XK_Knob_2D_0_rotate_X          = 0x3000000B,
        XK_Knob_2D_0_rotate_y          = 0x3000000C,
        XK_Knob_2D_0_rotate_Y          = 0x3000000D,
        XK_Knob_2D_0_rotate_z          = 0x3000000E,
        XK_Knob_2D_0_rotate_Z          = 0x3000000F,

        XK_Knob_2D_1_shift_right       = 0x30000010,
        XK_Knob_2D_1_shift_left        = 0x30000011,
        XK_Knob_2D_1_shift_up          = 0x30000012,
        XK_Knob_2D_1_shift_up_right    = 0x30000013,
        XK_Knob_2D_1_shift_up_left     = 0x30000014,
        XK_Knob_2D_1_shift_down        = 0x30000015,
        XK_Knob_2D_1_shift_down_right  = 0x30000016,
        XK_Knob_2D_1_shift_down_left   = 0x30000017,
        XK_Knob_2D_1_shift_push        = 0x30000018,
        XK_Knob_2D_1_shift_pull        = 0x30000019,
        XK_Knob_2D_1_rotate_x          = 0x3000001A,
        XK_Knob_2D_1_rotate_X          = 0x3000001B,
        XK_Knob_2D_1_rotate_y          = 0x3000001C,
        XK_Knob_2D_1_rotate_Y          = 0x3000001D,
        XK_Knob_2D_1_rotate_z          = 0x3000001E,
        XK_Knob_2D_1_rotate_Z          = 0x3000001F,

        XK_Knob_2D_2_shift_right       = 0x30000020,
        XK_Knob_2D_2_shift_left        = 0x30000021,
        XK_Knob_2D_2_shift_up          = 0x30000022,
        XK_Knob_2D_2_shift_up_right    = 0x30000023,
        XK_Knob_2D_2_shift_up_left     = 0x30000024,
        XK_Knob_2D_2_shift_down        = 0x30000025,
        XK_Knob_2D_2_shift_down_right  = 0x30000026,
        XK_Knob_2D_2_shift_down_left   = 0x30000027,
        XK_Knob_2D_2_shift_push        = 0x30000028,
        XK_Knob_2D_2_shift_pull        = 0x30000029,
        XK_Knob_2D_2_rotate_x          = 0x3000002A,
        XK_Knob_2D_2_rotate_X          = 0x3000002B,
        XK_Knob_2D_2_rotate_y          = 0x3000002C,
        XK_Knob_2D_2_rotate_Y          = 0x3000002D,
        XK_Knob_2D_2_rotate_z          = 0x3000002E,
        XK_Knob_2D_2_rotate_Z          = 0x3000002F,

        XK_Knob_2D_3_shift_right       = 0x30000030,
        XK_Knob_2D_3_shift_left        = 0x30000031,
        XK_Knob_2D_3_shift_up          = 0x30000032,
        XK_Knob_2D_3_shift_up_right    = 0x30000033,
        XK_Knob_2D_3_shift_up_left     = 0x30000034,
        XK_Knob_2D_3_shift_down        = 0x30000035,
        XK_Knob_2D_3_shift_down_right  = 0x30000036,
        XK_Knob_2D_3_shift_down_left   = 0x30000037,
        XK_Knob_2D_3_shift_push        = 0x30000038,
        XK_Knob_2D_3_shift_pull        = 0x30000039,
        XK_Knob_2D_3_rotate_x          = 0x3000003A,
        XK_Knob_2D_3_rotate_X          = 0x3000003B,
        XK_Knob_2D_3_rotate_y          = 0x3000003C,
        XK_Knob_2D_3_rotate_Y          = 0x3000003D,
        XK_Knob_2D_3_rotate_z          = 0x3000003E,
        XK_Knob_2D_3_rotate_Z          = 0x3000003F,

        XK_ITU_Key_0                   = 0x30000100,
        XK_ITU_Key_1                   = 0x30000101,
        XK_ITU_Key_2                   = 0x30000102,
        XK_ITU_Key_3                   = 0x30000103,
        XK_ITU_Key_4                   = 0x30000104,
        XK_ITU_Key_5                   = 0x30000105,
        XK_ITU_Key_6                   = 0x30000106,
        XK_ITU_Key_7                   = 0x30000107,
        XK_ITU_Key_8                   = 0x30000108,
        XK_ITU_Key_9                   = 0x30000109,
        XK_ITU_Key_Asterix             = 0x3000010A,
        XK_ITU_Key_Pound               = 0x3000010B,

        XK_Device_Phone_call           = 0x30000200,
        XK_Device_Phone_end            = 0x30000201,
        XK_Device_Soft_left            = 0x30000202,
        XK_Device_Soft_middle          = 0x30000203,
        XK_Device_Soft_right           = 0x30000204,
        XK_Device_Application          = 0x30000205,
        XK_Device_Ok                   = 0x30000206,
        XK_Device_Delete               = 0x30000207,
        XK_Device_Zoom_in              = 0x30000208,
        XK_Device_Zoom_out             = 0x30000209,
        XK_Device_Clear                = 0x3000020A,
        XK_Device_Forward              = 0x3000020B,
        XK_Device_Backward             = 0x3000020C,
        XK_Device_Home                 = 0x3000020D,
        XK_Device_Search               = 0x3000020E,
        XK_Device_Menu                 = 0x3000020F,

        XK_Function_Key_0              = 0x30000300,
        XK_Function_Key_1              = 0x30000301,
        XK_Function_Key_2              = 0x30000302,
        XK_Function_Key_3              = 0x30000303,
        XK_Function_Key_4              = 0x30000304,
        XK_Function_Key_5              = 0x30000305,
        XK_Function_Key_6              = 0x30000306,
        XK_Function_Key_7              = 0x30000307,
        XK_Function_Key_8              = 0x30000308,
        XK_Function_Key_9              = 0x30000309,
        XK_Function_Key_10             = 0x3000030A,
        XK_Function_Key_11             = 0x3000030B,
        XK_Function_Key_12             = 0x3000030C,
        XK_Function_Key_13             = 0x3000030D,
        XK_Function_Key_14             = 0x3000030E,
        XK_Function_Key_15             = 0x3000030F,
        XK_Function_Key_16             = 0x30000310,
        XK_Function_Key_17             = 0x30000311,
        XK_Function_Key_18             = 0x30000312,
        XK_Function_Key_19             = 0x30000313,
        XK_Function_Key_20             = 0x30000314,
        XK_Function_Key_21             = 0x30000315,
        XK_Function_Key_22             = 0x30000316,
        XK_Function_Key_23             = 0x30000317,
        XK_Function_Key_24             = 0x30000318,
        XK_Function_Key_25             = 0x30000319,
        XK_Function_Key_26             = 0x3000031A,
        XK_Function_Key_27             = 0x3000031B,
        XK_Function_Key_28             = 0x3000031C,
        XK_Function_Key_29             = 0x3000031D,
        XK_Function_Key_30             = 0x3000031E,
        XK_Function_Key_31             = 0x3000031F,

        XK_Function_Key_255            = 0x300003FF,

        XK_Multimedia_Play             = 0x30000400,
        XK_Multimedia_Pause            = 0x30000401,
        XK_Multimedia_Stop             = 0x30000402,
        XK_Multimedia_Forward          = 0x30000403,
        XK_Multimedia_Rewind           = 0x30000404,
        XK_Multimedia_Next             = 0x30000405,
        XK_Multimedia_Previous         = 0x30000406,
        XK_Multimedia_Mute             = 0x30000407,
        XK_Multimedia_Unmute           = 0x30000408,
        XK_Multimedia_Photo            = 0x30000409,
    };
};

#ifdef QT_DECLARATIVE_LIB
QML_DECLARE_TYPE(QTmGlobal);
#endif


#endif // QTMGLOBAL_H
