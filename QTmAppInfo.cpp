/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QTmAppInfo.h"

/*!
  \class QTmAppInfo
  \brief Holds information about the properties of the advertised remote application.

  This class provides information about the application category and the trust level of that information.
  The information is typically associated with a remote application. The Terminal Mode client uses
  this and other information to decide, whether the application can be shown on the client"s display or not.

  The QTmAppInfo instance is used from the following components:
  \list
  \o QTmRemoteServer - as part of every advertised application
  \o QTmRfbServer - pras part of the VNC Context Information Pseudo Encoding.
  \endlist

  The class does not have a public constructor, but is rather instantiated from the QTmRemoteServer or
  the QTmRfbServer class instance.
 */
/*!
  \property QTmAppInfo::category
  \brief Holds the application category information.
  The upper 16 bit of the application category give the generic category, whereas the lower 16 bit define
  different sub-categories. The supported categories are defined in QTmGlobal.h.
  The default value is QTmGlobal::AppUnknown, i.e. the application is not known.
 */
/*!
  \property QTmAppInfo::trust
  \brief Holds the trust level of the application information.
  The application category can be provided from different sources at the Terminal Mode server, which are more or less
  trustworthy than others. The trust value therefore specifies, from which source the category information is
  originating from. The supported trust levels are defined in QTmGlobal.h.
  Default value is QTmGlobal::TrustUnknown, i.e. the application category should not be trusted.
 */

/*!
  Class constructor (private).
 */
QTmAppInfo::QTmAppInfo() :
        m_category(0),
        m_trust(QTmGlobal::TrustUnknown) {
}

int QTmAppInfo::category() {
    return m_category;
}
void QTmAppInfo::setCategory(int category) {
    m_category = category;
}

QTmGlobal::TmTrustLevel QTmAppInfo::trust() {
    return m_trust;
}
void QTmAppInfo::setTrust(QTmGlobal::TmTrustLevel trust) {
    m_trust = trust;
}
