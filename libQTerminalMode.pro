# -------------------------------------------------
# Project libqterminalmode
# -------------------------------------------------
QT += network \
    xml
contains(QT_CONFIG, declarative):QT += declarative
contains(QT_CONFIG, dbus):QT += dbus
TARGET = QtTerminalMode
TEMPLATE = lib
DEFINES += QT_BUILD_TERMINALMODE_LIB

debug: {
# DEFINES += QT_DEBUG_TM
DEFINES += QT_DEBUG_TM_PERFORMANCE_COUNTER
# DEFINES += QT_DEBUG_TM_RFBCLIENT
# DEFINES += QT_DEBUG_TM_NETWORK_OBSERVER
# DEFINES += QT_DEBUG_TM_CONFIGURATION
# DEFINES += QT_DEBUG_TM_UPNP
# DEFINES += QT_DEBUG_TM_UPNP_HTTP
# DEFINES += QT_DEBUG_TM_REMOTE_SERVER
# DEFINES += QT_DEBUG_TM_SURFACE
}

MOC_DIR = moc
CONFIG += debug
OBJECTS_DIR = obj
INCLUDEPATH += ./
INCLUDEPATH += ./doc
SOURCES += QTmGlobal.cpp \
    QTmUpnpControlPoint.cpp \
    QTmNetworkObserver.cpp \
    QUpnpHttpServer.cpp \
    QTmApplication.cpp \
    QTmIcon.cpp \
    QTmAudioInfo.cpp \
    QTmDisplayInfo.cpp \
    QTmAppInfo.cpp \
    QTmRemotingInfo.cpp \
    QTmRemoteServer.cpp \
    QTmClientRules.cpp \
    QTmClientProfile.cpp \
    QTmClient.cpp \
    QTmWidget.cpp \
    QTmDeclarativeItem.cpp \
    QTmRfbClient.cpp \
    QTmSurface.cpp \
    QTmTouchEvent.cpp \
    QTmPointerEvent.cpp \
    QTmKeyEvent.cpp \
    QTmConfiguration.cpp

# contains(QT_CONFIG, declarative):SOURCES += qdeclarativeterminalmode.cpp
HEADERS += QTmUpnpControlPoint.h \
    QTmGlobal.h \
    QTmNetworkObserver.h \
    QUpnpHttpServer.h \
    QTmApplication.h \
    QTmIcon.h \
    QTmAudioInfo.h \
    QTmDisplayInfo.h \
    QTmAppInfo.h \
    QTmRemotingInfo.h \
    QTmRemoteServer.h \
    QTmClientProfile.h \
    QTmClientRules.h \
    QTmClient.h \
    QTmWidget.h \
    QTmDeclarativeItem.h \
    QTmRfbClient.h \
    QTmSurface.h \
    QTmTouchEvent.h \
    QTmPointerEvent.h \
    QTmKeyEvent.h \
    QTmConfiguration.h

# contains(QT_CONFIG, declarative):HEADERS += qdeclarativeterminalmode.h
headers.files = $$HEADERS
isEmpty(INSTALLPREFIX) {
    headers.path = $$[QT_INSTALL_HEADERS]/$$TARGET
    target.path = $$[QT_INSTALL_LIBS]
} else {
    headers.path = $$INSTALLPREFIX/include/$$TARGET
    target.path = $$INSTALLPREFIX/lib/
}
INSTALLS += headers
INSTALLS += target
win32:LIBS += -lws2_32
wince:LIBS += -lws2
