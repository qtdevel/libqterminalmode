/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#ifndef QTMCONFIGURATION_H
#define QTMCONFIGURATION_H

#include <QObject>
#include <QHash>

class QTmConfiguration : QObject {

    Q_OBJECT

public:

    enum QTmConfig {
        ConfigRfbVersionMajor,      //Major RFB version (Server)
        ConfigRfbVersionMinor,      //Minor RFB version (Server)
        ConfigTmVersionMajor,       //Major TM version (Server)
        ConfigTmVersionMinor,       //Minor TM version (Server)
        ConfigIncrementalUpdate,    //Support Incremental Updates (Client)
        ConfigRunLengthEncoding,    //Support Run-Length Encoding (Client)
        ConfigContextInfoEncoding,  //Support Context Information Pseudo Encoding (Client)
        ConfigTerminalModeEncoding, //Support Terminal Mode Pseudo Encoding (Client)
        ConfigDesktopSizeEncoding,  //Support Desktop Size Pseudo Encoding
        ConfigKeyboardLanguage,     //Keyboard Language (Server & Client)
        ConfigKeyboardCountry,      //Keyboard Country (Server & Client)
        ConfigUiLanguage,           //UI Language (Server & Client)
        ConfigUiCountry,            //UI Country (Server & Client)
        ConfigEventKnobKey,         //Knob key event mask (Server & Client)
        ConfigEventDeviceKey,       //Device key event mask (Server & Client)
        ConfigEventMediaKey,        //Media key event mask (Server & Client)
        ConfigEventItuKeypad,       //ITU keypad support (Server & Client)
        ConfigVirtualKeyboard,      //Virtual keyboard support (Server & Client)
        ConfigKeyEventListing,      //Key event listing support (Server & Client)
        ConfigKeyEventMapping,      //Key event mapping support (Server & Client)
        ConfigEventFunctionKeys,    //Number of supported function keys (Server & Client)
        ConfigEventPointer,         //Pointer event support (Server & Client)
        ConfigEventTouch,           //Touch event support (Server & Client)
        ConfigButtonMask,           //Button mask (Server & Client)
        ConfigTouchNumber,          //Number of simultaneous touch events (Server & Client)
        ConfigPressureMask,         //Pressure mask (Server & Client)
        ConfigColorARGB888,         //Support for ARGB888 (Server)
        ConfigColorRGB888,          //Support for RGB888 (Server)
        ConfigColorRGB565,          //Support for RGB565 (Server)
        ConfigColorRGB555,          //Support for RGB555 (Server)
        ConfigColorRGB444,          //Support for RGB444 (Server)
        ConfigColorRGB343,          //Support for RGB343 (Server)
        ConfigColorGray16,          //Support for 16-bit gray (Server)
        ConfigColorGray8,           //Support for 8-bit gray (Server)
        ConfigFbOrientation,        //Framebuffer orientation (Server & Client)
        ConfigFbRotation,           //Framebuffer rotation (Server & Client)
        ConfigFbUpScaling,          //Framebuffer up-scaling (Server & Client)
        ConfigFbDownScaling,        //Framebuffer down-scaling (Server & Client)
        ConfigFbReplacement,        //Framebuffer replacement with empty framebuffer (Server & Client)
        ConfigFbColorFormat,        //Remote Framebuffer color format (Server & Client)
        ConfigFbWidth,              //Remote Framebuffer width in pixel (Server)
        ConfigFbHeight,             //Remote Framebuffer height in pixel (Server)
        ConfigFbUpdates,            //Framebuffer updates enabled (Client)
        ConfigPixelWidth,           //Relative pixel width (Server)
        ConfigPixelHeight,          //Relative pixel height (Server)
        ConfigRequestedFbWidth,     //Requested framebuffer width in pixel (Client)
        ConfigRequestedFbHeight,    //Requested framebuffer height in pixel (Client)
        ConfigDisplayPixelWidth,    //Display width in pixel (Client)
        ConfigDisplayPixelHeight,   //Display height in pixel (Client)
        ConfigDisplaySizeWidth,     //Physical display width (Client)
        ConfigDisplaySizeHeight,    //Physical display height (Client)
        ConfigDisplayDistance,      //Physical display distance (Client)
        ConfigDeviceKeyLock,        //Key Lock (Server)
        ConfigDeviceDeviceLock,     //Device lock (Server)
        ConfigDeviceScreenSaver,    //Screen saver (Server)
        ConfigDeviceNightMode,      //Night mode (Server)
        ConfigDeviceVoiceInput,     //Voice input (Server)
        ConfigDeviceMicInput,       //Mic input (Server)
        ConfigDeviceDistraction,    //Driver distraction (Server)
        ConfigDeviceRotation,       //Framebuffer rotation (Server)
        ConfigDeviceOrientation,    //Framebuffer orientation (Server)
    };

    QTmConfiguration();
    QTmConfiguration(QString label);
    void         setConfiguration(QTmConfig key, unsigned int value = 0);
    unsigned int configuration(QTmConfig key, unsigned int defaultValue = 0);

private:
    QHash<QTmConfig, unsigned int>   m_configMap;
    QString                          m_label;
};

#endif // QTMCONFIGURATION_H
