/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QTmAudioInfo.h"

/*!
  \class QTmAudioInfo
  \brief Holds additional information about the audio properties of the advertised remote application.

  This class provides information about the audio properties of a remote application and
  the trust level of that information. Audio information is used for the audio server and client modules only.
  The audio properties provide information about

  \list
  \o Audio type - in case of RTP as the remote protocol, the audio type tells the RTP payload being available / supported.
  \o Audio category - tells whether the audio stream is phone call audio or media audio etc.
  \o Audio rules - tells whether the audio stream is following specific guidelines.
     No guidelines have been defined at this point
  \endlist

  The Terminal Mode client uses this together with other information to identify the audio module to use for the audio connection, based on the
  given properties. The QTmAppInfo instance is used from the QTmRemoteServer,as part of every advertised audio application.
  The class does not have a public constructor, but is rather instantiated from the QTmRemoteServer class instance.
 */
/*!
  \property QTmAudioInfo::type
  \brief Holds the audio type.
  In case of RTP audio streaming, the audio type defines the used RTP payload type.
 */
/*!
  \property QTmAudioInfo::category
  \brief Holds the audio category.
  The audio category can be any OR combination of the following values:
  \list
   \o QTmGlobal::AudioPhone - Phone call audio (bi-directional)
   \o QTmGlobal::AudioMediaOut - Media audio (output)
   \o QTmGlobal::AudioMediaIn - Media audio (input)
   \o QTmGlobal::AudioVoiceCommandOut - Voice command (output)
   \o QTmGlobal::AudioVoiceCommandIn - Voice command (input)
   \o QTmGlobal::AudioMisc - Other audio (not specified)
  \endlist
  An RTP server will provide only audio output, whereas an RTP client will only receive audio input data.
 */
/*!
  \property QTmAudioInfo::rules
  \brief Holds the audio rule set applied.
  Till this point, no audio rules have been defined. The default value is zero (0).
 */
/*!
  \property QTmAudioInfo::trust
  \brief Holds the trust level information for the audio information.
  The audio info can be provided from different sources at the Terminal Mode server, which are more or less
  trustworthy than others. The trust value therefore specifies, from which source the information is
  originating from. The supported trust levels are defined in QTmGlobal.h.
  Default value is QTmGlobal::TrustUnknown, i.e. the information should not be trusted.
 */

/*!
  Class constructor (private).
 */
QTmAudioInfo::QTmAudioInfo() :
        m_type(0),
        m_category(0),
        m_rules(0),
        m_trust(QTmGlobal::TrustUnknown) {
}

int QTmAudioInfo::type() {
    return m_type;
}
void QTmAudioInfo::setType(int type) {
    m_type = type;
}

int QTmAudioInfo::category() {
    return m_category;
}
void QTmAudioInfo::setCategory(int category) {
    m_category = category;
}

int QTmAudioInfo::rules() {
    return rules();
}
void QTmAudioInfo::setRules(int rules) {
    m_rules = rules;
}

QTmGlobal::TmTrustLevel QTmAudioInfo::trust() {
    return m_trust;
}
void QTmAudioInfo::setTrust(QTmGlobal::TmTrustLevel trust) {
    m_trust = trust;
}

