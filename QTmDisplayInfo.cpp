/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QTmDisplayInfo.h"

/*!
  \class QTmDisplayInfo
  \brief Holds additional information about the visual display properties of the advertised remote application.

  This class provides information about the display (i.e. the visual) properties of a remote application and
  the trust level of that information. The display propoerties are
  \list
  \o Content category - flags the content types the application is using
  \o Content rules - flags the distraction rules, which are followed.
  \endlist
  The display information is used to decide, whether the application can be shown on the client"s display or not.
  The QTmDisplayInfo instance is used from the following components:
  \list
  \o QTmRemoteServer - as part of every advertised application
  \o QTmRfbServer - as part of the VNC Context Information Pseudo Encoding.
  \endlist
  The class does not have a public constructor, but is rather instantiated from the QTmRemoteServer or
  the QTmRfbServer class instance.
 */
/*!
  \property QTmDisplayInfo::category READ category)
  \brief Holds the information about the content category.

  The visual category can be any OR combination of the categories defined in QTmGlobal::TmVisualCategory.
  The QTmRemoteServer will provide display category information for the entire applicaitons,
  whereas the QTmRfbServer may provide fine granular context information of sub-window rectangles.
 */
/*!
  \property QTmDisplayInfo::rules    READ rules)
  \brief Holds the information about the driver distraction rules being followed.

  The visual rules can be any OR combination of the rules defined in QTmGlobal::TmVisualRules.
  Default value is 0 (zero). The QTmRemoteServer will provide diplay rule information for the entire applicaitons,
  whereas the QTmRfbServer may provide fine granular context information of sub-window rectangles.
 */
/*!
  \property QTmDisplayInfo::trust    READ trust)
  \brief Holds the trust level information of the display category information.

  The display info can be provided from different sources at the Terminal Mode server, which are more or less
  trustworthy than others. The trust value therefore specifies, from which source the information is
  originating from. The supported trust levels are defined in QTmGlobal::TmTrustLevel.
  Default value is QTmGlobal::TrustUnknown, i.e. the information should not be trusted.
 */

/*!
  Class constructor.
 */
QTmDisplayInfo::QTmDisplayInfo() :
        m_category(0),
        m_rules(0),
        m_trust(QTmGlobal::TrustUnknown) {
}

int QTmDisplayInfo::category() {
    return m_category;
}

void QTmDisplayInfo::setCategory(int category) {
    m_category = category;
}

int QTmDisplayInfo::rules() {
    return m_rules;
}

void QTmDisplayInfo::setRules(int rules) {
    m_rules = rules;
}

QTmGlobal::TmTrustLevel QTmDisplayInfo::trust() {
    return m_trust;
}

void QTmDisplayInfo::setTrust(QTmGlobal::TmTrustLevel trust) {
    m_trust = trust;
}
