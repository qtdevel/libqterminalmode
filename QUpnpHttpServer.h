/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/
#ifndef QUPNPHTTPSERVER_H
#define QUPNPHTTPSERVER_H

#include <QtNetwork/QTcpServer>
#include <QtCore/QByteArray>
#include <QtCore/QHash>
#include <QtCore/QMutex>

class QUpnpPropertyChangeListener
{
    public:
        virtual void processPropertyChangeEvent(const QString &name, const QString &value) = 0;
};

//Implements the HTTP Server for UPnP Control Point
//Used for receiving UPnP event notifications using GENA
class QUpnpHttpServer : public QTcpServer
{
    Q_OBJECT
public:
    /**
     * Class constructor
     * @param[in] parent Parent
     */
    explicit QUpnpHttpServer(QObject *parent = 0);

     /**
      * Class destructor
      */
    ~QUpnpHttpServer();
    void incomingConnection(int socket);

    void addPropertyChangeListener(QString sid, QUpnpPropertyChangeListener *listener);
    void removePropertyChangeListener(QString sid);

private slots:
    void readClient();
    void discardClient();

private:
    int processEventMessage(QByteArray& buf);

    QHash<QString, QUpnpPropertyChangeListener*> m_propChangeListenerHash;
    QMutex m_propChangeListenerMutex;
};
#endif // QUPNPHTTPSERVER_H
