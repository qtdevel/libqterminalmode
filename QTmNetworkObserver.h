/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#ifndef QTMNETWORKOBSERVER_H
#define QTMNETWORKOBSERVER_H

#include <QtCore/QObject>
#include <QtCore/QMultiHash>
#include <QtCore/QMutex>
#include <QtNetwork/QNetworkAddressEntry>
#include <QtNetwork/QNetworkInterface>
#include <QtNetwork/QNetworkConfigurationManager>

class QTmNetworkObserver : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool monitorIPv4 READ monitorIPv4 WRITE setMonitorIPv4)
    Q_PROPERTY(bool monitorIPv6 READ monitorIPv6 WRITE setMonitorIPv6)

public:
    explicit QTmNetworkObserver(QObject *parent = 0, bool monitorIPv4 = true, bool monitorIPv6 = false);
    ~QTmNetworkObserver();

    QNetworkConfiguration::BearerType bearerType(QString interfaceName);

signals:
    void addedAddress(QString name, QNetworkAddressEntry address);
    void removedAddress(QNetworkAddressEntry address);

public:
    bool monitorIPv4();
    bool monitorIPv6();
    void setMonitorIPv4(bool enabled);
    void setMonitorIPv6(bool enabled);

public slots:
    void checkNetworks();

private slots:
    void addConfiguration(const QNetworkConfiguration &config);
    void removeConfiguration(const QNetworkConfiguration &config);
    void updateConfiguration(const QNetworkConfiguration &config);

private:
    void update(const QString &name);
    void remove(const QString &name);

    bool m_monitorIPv4;
    bool m_monitorIPv6;
    QNetworkInterface interfaceFromName(const QString &name);
    QNetworkConfigurationManager m_configManager;
    QMultiHash<QString, QNetworkAddressEntry> m_addressHash;
    QMutex m_addressMutex;
};

#endif // QTMNETWORKOBSERVER_H
