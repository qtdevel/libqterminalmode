/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#ifndef QTMRFBCLIENT_H
#define QTMRFBCLIENT_H

#include <QTcpSocket>
#include <QMutex>
#include <QObject>
#include <QHash>
#include <QUrl>

#include "QTmConfiguration.h"
#include "QTmAppInfo.h"
#include "QTmDisplayInfo.h"
#include "QTmSurface.h"
#include "QTmTouchEvent.h"
#include "QTmKeyEvent.h"
#include "QTmPointerEvent.h"


class QTmRfbClient : public QObject {

    Q_OBJECT
    Q_PROPERTY(bool sessionActive READ sessionActive)
    Q_PROPERTY(QUrl sessionUrl    READ sessionUrl)

public:

    enum QTmSessionStatus {
        RFB_SESSION_NONE,
        RFB_SESSION_CONNECTED_ACTIVE,
        RFB_SESSION_CONNECTED_PASSIVE,
        RFB_SESSION_SERVER_VERSION,
        RFB_SESSION_SECURITY_TYPES,
        RFB_SESSION_SECURITY_RESPONSE,
        RFB_SESSION_SERVER_INIT,
        ERROR_RFB_SESSION_DISCONNECTED,
        ERROR_WRONG_SERVER_VERSION,
        ERROR_WRONG_SECURITY_TYPES,
        ERROR_ZERO_SECURITY_TYPES,
        ERROR_WRONG_FRAMEBUFFER_ENCODING,
        ERROR_WRONG_MSG_TYPE,
        ERROR_WRONG_FSM_STATE,
        ERROR_CANNOT_ALLOCATE_SURFACE,
    };

    QTmRfbClient(QObject *parent = 0);
    ~QTmRfbClient();

    void      setClientConfig(QTmConfiguration::QTmConfig key, unsigned int value = 0);
    unsigned int clientConfig(QTmConfiguration::QTmConfig key, unsigned int defaultValue = 0);
    unsigned int serverConfig(QTmConfiguration::QTmConfig key, unsigned int defaultValue = 0);

    void clientCutText(QString text);
    void triggerFramebufferUpdates();

    void clientDisplayConfig();
    void clientEventConfig();
    void keyEventListingRequest(bool enable);
    void deviceStatusRequest();
    void eventMappingRequest(unsigned int clientKey, unsigned int serverKey);
    void virtualKeyboardTriggerRequest(bool enable);
    void contentAttestationRequest(char *nounce, char attestation, char signature, char key, char *session_key);
    void framebufferAlternativeTextRequest(unsigned short int max_length);
    void framebufferBlockingNotification(QRect fbArea, unsigned int appId, unsigned int reason);
    void audioBlockingNotification(unsigned int appId, unsigned int reason);

    void setSurface(QTmSurface *surface);
    QSize setServerFbSize(QSize size);
    QSize setClientFbSize(QSize size, bool scaling);

    bool sessionStart(QUrl url);
    void sessionStop();
    bool sessionActive();
    QUrl sessionUrl();
    QTmSessionStatus sessionStatus();
    QHash<unsigned int, unsigned int> *eventMap();

public slots:
    void keyEvent(QTmKeyEvent *event);
    void pointerEvent(QTmPointerEvent *pointerEvent);
    void touchEvent(QList<QTmTouchEvent *> *touchEvent);

signals:
    void sessionConnected(QString serverName);
    void sessionDisconnected();
    void framebufferResized(QSize size);
    void displayConfigurationReceived();
    void eventConfigurationReceived();
    void eventMappingReceived(unsigned int clientKey, unsigned int serverKey);
    void deviceStatusReceived(unsigned int deviceStatus);
    void keyEventListReceived(QList<unsigned int> *list, unsigned int counter);
    void virtualKeyboardTriggerReceived(QPoint point, bool remove);
    void framebufferAlternativeTextReceived(unsigned int appId, QString text);
    void contextInformationReceived(QRect area, int appId, QTmAppInfo *appCategory, QTmDisplayInfo *conCategory);

private:
    void protocolHandshake();
    void securityHandshake();
    void securityResponse();
    void clientInit();
    void serverInit();
    void setEncodings(unsigned int encoding);
    void setPixelFormat();
    void serverCutText();
    void setColourMapEntries();
    void bell();
    void framebufferUpdateRequest(bool incremental);
    void framebufferUpdate();

    int  serverDisplayConfig();
    int  serverEventConfig();
    int  keyEventListing();
    int  deviceStatus();
    int  eventMapping();
    int  virtualKeyboardTrigger();
    int  contentAttestation();
    int  framebufferAlternativeText();

    void desktopSizePseudoEncoding(QSize framebufferSize);
    void contextInformationPseudoEncoding(unsigned char *cmdBuffer);
    void rawEncoding(int x, int y, int width, int height);
#ifdef QT_DEBUG_TM_PERFORMANCE_COUNTER
    int  rleEncoding(int x, int y, int width, int height);
#else
    void rleEncoding(int x, int y, int width, int height);
#endif

    int  receiveData(int length, char *buffer);
    int  transmitData(int length, char *buffer);

private slots:
    void protocolFsm();

private:
    bool                               m_connected;
    QHash<unsigned int, unsigned int> *m_eventMap;
    QTmSurface                        *m_surface;
    QUrl                               m_url;
    QTcpSocket                        *m_socket;

    QTmConfiguration                  *m_rfbConfigClient;
    QTmConfiguration                  *m_rfbConfigServer;
    QMutex                             m_mutex;
    QTmSessionStatus                   m_rfbSessionStatus;
    unsigned int                       m_keyEventCounter;
    unsigned char                     *m_framebuffer;

};

#endif // QTMRFBCLIENT_H
