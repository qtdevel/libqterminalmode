/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QtGui"

#include "QTmDeclarativeItem.h"

QTmDeclarativeItem::QTmDeclarativeItem(QDeclarativeItem *parent)
    : QDeclarativeItem(parent)
    , m_surface(0)
{
#ifdef QT_DEBUG_TM_SURFACE
    qDebug () << "QTmDeclarativeItem (object constructed) at" << this;
#endif
    setFlag(QGraphicsItem::ItemHasNoContents, false);
    setFlag(QGraphicsItem::ItemIsFocusable, true);
    setAcceptedMouseButtons(Qt::LeftButton);
}

/*!
    Empty
*/
QTmDeclarativeItem::~QTmDeclarativeItem()
{
    deallocateSurface();
#ifdef QT_DEBUG_TM_SURFACE
    qDebug () << "QTmDeclarativeItem (object destroyed) at" << this;
#endif
}


/*!
    Empty
*/
uchar* QTmDeclarativeItem::allocateSurface(const QSize &s, QTmGlobal::TmColorFormat colorFormat)
{
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmDeclarativeItem:" << "Allocate surface (" << s << "," << colorFormat << ")";
#endif

    if(m_surface && m_surfaceTmColorFormat == colorFormat &&
            m_surface->width() == s.width() &&
            m_surface->height() == s.height())
        return (uchar*) m_surface->bits();

    QImage *tmp;
    switch (colorFormat) {
    case QTmGlobal::RGB888:
        tmp = new QImage(s, QImage::Format_RGB32);
        break;
    case QTmGlobal::RGB565:
        tmp = new QImage(s, QImage::Format_RGB16);
        break;
    case QTmGlobal::RGB555:
        tmp = new QImage(s, QImage::Format_RGB555);
        break;
    case QTmGlobal::RGB444:
    case QTmGlobal::RGB343:
        tmp = new QImage(s, QImage::Format_RGB444);
        break;
    default:
        qWarning() << "QTmDeclarativeItem:" << "Unknown color format";
        return 0;
    }
    //Fill in background color
#ifdef QT_DEBUG_TM_SURFACE
    tmp->fill(255);
#else
    tmp->fill(0);
#endif

    QWriteLocker wLocker(&m_surfaceResizeLock);
    if (m_surface) delete m_surface;

    m_surfaceTmColorFormat = colorFormat;
    m_surface = tmp;

    m_transform = QTmSurface::getTransformation(m_myRect.size().toSize(), m_surface->size());
    m_transformInverted = m_transform.inverted();

    return (uchar*) m_surface->bits();
}

/*!
    Empty
*/
void QTmDeclarativeItem::deallocateSurface()
{
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmDeclarativeItem:" << "Deallocate surface";
#endif
    QWriteLocker wLocker(&m_surfaceResizeLock);
    if (m_surface) {
        delete m_surface;
        m_surface = 0;
    }
    m_transform.reset();
    m_transformInverted.reset();
    update();
}

/*!
    Empty
*/
void QTmDeclarativeItem::updateSurface(const QRect &updateRect)
{
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmDeclarativeItem:" << "Framebuffer Update";
#endif
    Q_UNUSED(updateRect);
    update();
}

/*!
  Reimplemented from QDeclarativeItem::paintEvent()
  Sends the paint \a event to the internal handler.
*/
void QTmDeclarativeItem::paint(QPainter *p, const QStyleOptionGraphicsItem *, QWidget *)
{
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmDeclarativeItem:" << "Paint Event";
#endif
    QReadLocker rLocker(&m_surfaceResizeLock);
    if (m_surface && isVisible()) {
        p->save();
        p->setRenderHints(QPainter::Antialiasing |
                          QPainter::SmoothPixmapTransform |
                          QPainter::HighQualityAntialiasing, true);
        QRect tRect = m_surface->rect();
        // set transformation
        p->setTransform(m_transform, true);
        // draw surface
        //p->drawImage(tRect, (*m_surface), tRect);
        //TODO draw image instead of pixmap
        QPixmap pixmap = QPixmap::fromImage(*m_surface);
        p->drawPixmap(tRect, pixmap, tRect);
        p->restore();
    }
}

/*!
  Reimplemented from QDeclarativeItem::mousePressEvent()
  Sends the mouse move \a event to the internal handler.
*/
void QTmDeclarativeItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmDeclarativeItem:" << "Mouse Press Event";
#endif
    QTmPointerEvent pointerEvent(m_transformInverted.map(event->pos()).toPoint(), true);
    emit tmPointerEvent(&pointerEvent);
}

/*!
  Reimplemented from QDeclarativeItem::mouseReleaseEvent()
  Sends the mouse move \a event to the internal handler.
*/
void QTmDeclarativeItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmDeclarativeItem:" << "Mouse Release Event";
#endif
    QTmPointerEvent pointerEvent(m_transformInverted.map(event->pos()).toPoint(), false);
    emit tmPointerEvent(&pointerEvent);
}

/*!
  Reimplemented from QDeclarativeItem::mouseMoveEvent()
  Sends the mouse move \a event to the internal handler.
*/
void QTmDeclarativeItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmDeclarativeItem:" << "Mouse Move Event";
#endif
    QTmPointerEvent pointerEvent(m_transformInverted.map(event->pos()).toPoint(), true);
    emit tmPointerEvent(&pointerEvent);
}

/*!
  Reimplemented from QDeclarativeItem::keyPressEvent()
  Modifies the key if ShiftModifier flag is set in the \a event is pressed and sends it to the VNC client.
*/
void QTmDeclarativeItem::keyPressEvent(QKeyEvent *event)
{
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmDeclarativeItem:" << "Key Press Event";
#endif
    QTmKeyEvent keyEvent(QTmSurface::translateKey(event->key(), event->modifiers()), true);
    emit tmKeyEvent(&keyEvent);
}

/*!
  Reimplemented from QDeclarativeItem::keyReleaseEvent()
  Sends the \a event key to the VNC client.
*/
void QTmDeclarativeItem::keyReleaseEvent(QKeyEvent *event) {
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmDeclarativeItem:" << "Key Release Event";
#endif
    QTmKeyEvent keyEvent(QTmSurface::translateKey(event->key(), event->modifiers()), false);
    emit tmKeyEvent(&keyEvent);
}

void QTmDeclarativeItem::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
#ifdef QT_DEBUG_TM_SURFACE
    qDebug() << "QTmDeclarativeItem:" << "Geometry Changed" << newGeometry;
#endif
    Q_UNUSED(oldGeometry);
    QDeclarativeItem::geometryChanged(newGeometry, oldGeometry);

    QWriteLocker wLocker(&m_surfaceResizeLock);
    m_myRect = newGeometry;

    if (!m_surface)
        return;

    m_transform = QTmSurface::getTransformation(m_myRect.size().toSize(), m_surface->size());
    m_transformInverted = m_transform.inverted();
}

QPoint QTmDeclarativeItem::surfaceToSource(QPoint point)
{
    return m_transformInverted.map(point);
}
