/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/
#ifndef QTMCLIENTPROFILE_H
#define QTMCLIENTPROFILE_H

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QUrl>

#include "QTmGlobal.h"
#include "QTmClientRules.h"

class QTmClientProfile {

public:
                                 QTmClientProfile(QString clientId, QObject *parent = 0);
                                ~QTmClientProfile();

    QString                      clientId();
    void                         setClientName(QString clientName);
    QString                      clientName();
    void                         setManufacturerName(QString manufacturerName);
    QString                      manufacturerName();
    void                         setModelName(QString modelName);
    QString                      modelName();
    void                         setModelNumber(QString modelNumber);
    QString                      modelNumber();
    void                         setIconMimeType(QString mimeType);
    QString                      iconMimeType();
    void                         setIconWidth(int width);
    int                          iconWidth();
    void                         setIconHeight(int iconHeight);
    int                          iconHeight();
    void                         setIconDepth(int iconDepth);
    int                          iconDepth();
    void                         setBdAddr(QString bdAddr);
    QString                      bdAddr();
    void                         setStartBTConnection(bool startBTConnection);
    bool                         startBTConnection();
    void                         setClientRules(QTmClientRules clientRules);
    QTmClientRules               clientRules();
    bool                         isNull();

private:
    QString                      m_clientId;
    QString                      m_clientName;
    QString                      m_manufacturerName;
    QString                      m_modelName;
    QString                      m_modelNumber;
    QString                      m_iconMimeType;
    int                          m_iconWidth;
    int                          m_iconHeight;
    int                          m_iconDepth;
    QString                      m_bdAddr;
    bool                         m_startBTConnection;
    QTmClientRules               m_clientRules;
};
#endif // QTMCLIENTPROFILE_H
