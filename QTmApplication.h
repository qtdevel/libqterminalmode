/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#ifndef QTMAPPLICATION_H
#define QTMAPPLICATION_H

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QUrl>

#include "QTmGlobal.h"
#include "QTmIcon.h"
#include "QTmAudioInfo.h"
#include "QTmDisplayInfo.h"
#include "QTmRemotingInfo.h"
#include "QTmAppInfo.h"

class QTmApplication : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString appId                              READ appId)
    Q_PROPERTY(QString name                               READ name)
    Q_PROPERTY(QStringList variants                       READ variants)
    Q_PROPERTY(QString providerName                       READ providerName)
    Q_PROPERTY(QUrl providerUrl                           READ providerUrl)
    Q_PROPERTY(QString description                        READ description)
    Q_PROPERTY(QList<QTmIcon *> iconList                  READ iconList)
    Q_PROPERTY(QList<int> allowedProfileIDs               READ allowedProfileIDs)
    Q_PROPERTY(QTmRemotingInfo *remotingInfo              READ remotingInfo)
    Q_PROPERTY(QUrl appCertificateURL                     READ appCertificateURL)
    Q_PROPERTY(QTmAppInfo *appInfo                        READ appInfo)
    Q_PROPERTY(QTmDisplayInfo *displayInfo                READ displayInfo)
    Q_PROPERTY(QTmAudioInfo *audioInfo                    READ audioInfo)
    Q_PROPERTY(QTmGlobal::TmResourceStatus resourceStatus READ resourceStatus)
    Q_PROPERTY(QTmGlobal::TmStatus status                 READ status)
    Q_PROPERTY(bool hasSignature                          READ signature)
    Q_PROPERTY(bool isValidated                           READ validated)
    Q_PROPERTY(int remoteServerId                         READ remoteServerId)

public:
   ~QTmApplication();

    QString                      appId();
    QString                      name();
    QStringList                  variants();
    QString                      providerName();
    QUrl                         providerUrl();
    QString                      description();
    QList<QTmIcon *>             iconList();
    QList<int>                   allowedProfileIDs();
    QTmRemotingInfo             *remotingInfo();
    QUrl                         appCertificateURL();
    QTmAppInfo                  *appInfo();
    QTmDisplayInfo              *displayInfo();
    QTmAudioInfo                *audioInfo();
    QTmGlobal::TmResourceStatus  resourceStatus();
    QTmGlobal::TmStatus          status();
    bool                         signature();
    bool                         validated();
    QUrl                         url();
    int                          remoteServerId();

private:
    QTmApplication(int remoteServerId, QString appId, bool signature, bool validated, QObject *parent = 0);

    void                         setName(QString name);
    void                         setVariants(QStringList variants);
    void                         setProviderName(QString providerName);
    void                         setProviderUrl(QUrl providerUrl);
    void                         setDescription(QString description);
    void                         setIconList(QList<QTmIcon *> iconList);
    void                         setAllowedProfileIDs(QList<int> allowedProfileIDs);
    void                         setRemotingInfo(QTmRemotingInfo *remotingInfo);
    void                         setAppCertificateURL(QUrl appCertificatURL);
    void                         setAppInfo(QTmAppInfo *appInfo);
    void                         setDisplayInfo(QTmDisplayInfo *displayInfo);
    void                         setAudioInfo(QTmAudioInfo *audioInfo);
    void                         setResourceStatus(QTmGlobal::TmResourceStatus resourceStatus);
    void                         setStatus(QTmGlobal::TmStatus status);
    void                         setUrl(QUrl url);

private:
    QString                      m_appId;
    QString                      m_name;
    QStringList                  m_variants;
    QString                      m_providerName;
    QUrl                         m_providerUrl;
    QString                      m_description;
    QList<QTmIcon *>             m_iconList;
    QList<int>                   m_allowedProfileIDs;
    QTmRemotingInfo             *m_remotingInfo;
    QUrl                         m_appCertificateURL;
    QTmAppInfo                  *m_appInfo;
    QTmDisplayInfo              *m_displayInfo;
    QTmAudioInfo                *m_audioInfo;
    QTmGlobal::TmResourceStatus  m_resourceStatus;
    QTmGlobal::TmStatus          m_status;
    bool                         m_signature;
    bool                         m_validated;
    QUrl                         m_url;
    int                          m_remoteServerId;
    friend class                 QTmRemoteServer;
};

#endif // QTMAPPLICATION_H
