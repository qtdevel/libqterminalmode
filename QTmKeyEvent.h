/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#ifndef QTMKEYEVENT_H
#define QTMKEYEVENT_H

#include <QObject>

class QTmKeyEvent : public QObject {
    Q_OBJECT

    Q_PROPERTY(int  key     READ key)
    Q_PROPERTY(bool pressed READ pressed)

public:
    QTmKeyEvent(int key, bool pressed);

    int  key();
    bool pressed();

private:
    void setKey(int key);
    void setPressed(int pressed);

private:
    int  m_key;
    bool m_pressed;
    friend class QTmRfbClient;
};

#endif // QTMKEYEVENT_H
