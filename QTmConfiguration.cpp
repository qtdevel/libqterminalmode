/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QTmConfiguration.h"
#include "QTmGlobal.h"

QTmConfiguration::QTmConfiguration() {
    m_label = "Unknown";
}

QTmConfiguration::QTmConfiguration(QString label) {
    m_label = label;
}

void QTmConfiguration::setConfiguration(QTmConfig key, unsigned int value) {
#ifdef QT_DEBUG_TM_CONFIGURATION
    switch(key) {
    case ConfigRfbVersionMajor:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "RFB Major Version" << value;
        break;
    case ConfigRfbVersionMinor:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "RFB Minor Version" << value;
        break;
    case ConfigTmVersionMajor:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "TM Major Version" << value;
        break;
    case ConfigTmVersionMinor:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "TM Minor Version" << value;
        break;
    case ConfigIncrementalUpdate:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Inc. Update" << value;
        break;
    case ConfigRunLengthEncoding:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Support Run-Length Encoding" << value;
        break;
    case ConfigContextInfoEncoding:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Support Context Information Pseudo Encoding" << value;
        break;
    case ConfigTerminalModeEncoding:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Support Terminal Mode Pseudo Encoding" << value;
        break;
    case ConfigDesktopSizeEncoding:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Support Desktop Size Pseudo Encoding" << value;
        break;
    case ConfigKeyboardLanguage:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Keyboard Language" << value;
        break;
    case ConfigKeyboardCountry:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Keyboard Country" << value;
        break;
    case ConfigUiLanguage:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "UI Language" << value;
        break;
    case ConfigUiCountry:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "UI Country" << value;
        break;
    case ConfigEventKnobKey:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Knob Key" << value;
        break;
    case ConfigEventDeviceKey:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Device Key" << value;
        break;
    case ConfigEventMediaKey:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Media Key" << value;
        break;
    case ConfigEventItuKeypad:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Itu Keypad" << value;
        break;
    case ConfigVirtualKeyboard:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Virtual Keyboard" << value;
        break;
    case ConfigKeyEventListing:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Key Event listing" << value;
        break;
    case ConfigKeyEventMapping:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Event mapping" << value;
        break;
    case ConfigEventFunctionKeys:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Function keys" << value;
        break;
    case ConfigEventPointer:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Pointer event" << value;
        break;
    case ConfigEventTouch:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Touch event" << value;
        break;
    case ConfigButtonMask:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Button mask" << value;
        break;
    case ConfigTouchNumber:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Touch number" << value;
        break;
    case ConfigPressureMask:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Pressure mask" << value;
        break;
    case ConfigColorARGB888:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "ARGB888 Support" << value;
        break;
    case ConfigColorRGB888:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "RGB888 Support" << value;
        break;
    case ConfigColorRGB565:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "RGB565 Support" << value;
        break;
    case ConfigColorRGB555:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "RGB555 Support" << value;
        break;
    case ConfigColorRGB444:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "RGB444 Support" << value;
        break;
    case ConfigColorRGB343:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "RGB343 Support" << value;
        break;
    case ConfigColorGray16:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Gray16 Support" << value;
        break;
    case ConfigColorGray8:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Gray8 Support" << value;
        break;
    case ConfigFbOrientation:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Framebuffer orientation" << value;
        break;
    case ConfigFbRotation:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Framebuffer rotation" << value;
        break;
    case ConfigFbUpScaling:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Framebuffer up-scaling" << value;
        break;
    case ConfigFbDownScaling:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Framebuffer down-scaling" << value;
        break;
    case ConfigFbReplacement:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Framebuffer replacement with empty framebuffer" << value;
        break;
    case ConfigFbColorFormat:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Remote Framebuffer color format" << value;
        break;
    case ConfigFbWidth:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Remote Framebuffer width in pixel" << value;
        break;
    case ConfigFbHeight:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Remote Framebuffer height in pixel" << value;
        break;
    case ConfigFbUpdates:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Framebuffer updates enabled" << value;
        break;
    case ConfigPixelWidth:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Relative pixel width" << value;
        break;
    case ConfigPixelHeight:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Relative pixel height" << value;
        break;
    case ConfigRequestedFbWidth:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Requested framebuffer width in pixel" << value;
        break;
    case ConfigRequestedFbHeight:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Requested framebuffer height in pixel" << value;
        break;
    case ConfigDisplayPixelWidth:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Display width in pixel" << value;
        break;
    case ConfigDisplayPixelHeight:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Display height in pixel" << value;
        break;
    case ConfigDisplaySizeWidth:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Physical display width" << value;
        break;
    case ConfigDisplaySizeHeight:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Physical display height" << value;
        break;
    case ConfigDisplayDistance:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Physical display distance" << value;
        break;
    case ConfigDeviceKeyLock:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Key Lock" << value;
        break;
    case ConfigDeviceDeviceLock:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Device lock" << value;
        break;
    case ConfigDeviceScreenSaver:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Screen saver" << value;
        break;
    case ConfigDeviceNightMode:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Night mode" << value;
        break;
    case ConfigDeviceVoiceInput:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Voice input" << value;
        break;
    case ConfigDeviceMicInput:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Mic input" << value;
        break;
    case ConfigDeviceDistraction:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Driver distraction" << value;
        break;
    case ConfigDeviceRotation:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Framebuffer rotation" << value;
        break;
    case ConfigDeviceOrientation:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "Framebuffer orientation" << value;
        break;
    default:
        qDebug() << "QTmConfiguration::setConfiguration:" << m_label << "key" << key << "value" << value;
        break;
    }
#endif
    m_configMap.insert(key, value);
}

unsigned int QTmConfiguration::configuration(QTmConfig key, unsigned int defaultValue) {
    return m_configMap.value(key, defaultValue);
}
