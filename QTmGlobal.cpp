/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QTmGlobal.h"


/*! \class QTmGlobal
    \brief The QTmGlobal class provides global definitions for TerminalMode.
*/

/*!
  Class constructor. \a parent points to the parent object.
 */
QTmGlobal::QTmGlobal(QObject *parent) : QObject(parent) {
    Q_UNUSED(parent)
    ;
}

/*!
  \enum QTmGlobal::TmDeviceStatus
  Enumeration, covering the status a device element QTmGlobal::TmDeviceElement can be in.
  \value statusUnknown   Status is unknown
  \value statusDisabled  Status is disabled
  \value statusEnabled   Status is enabled
 */

/*!
  \enum QTmGlobal::TmRotationStatus
  Enumeration, convering the status of the framebuffer rotation (clock wise).
  \value rotationUnknown  Framebuffer rotation is unknown
  \value rotationAbs0     Framebuffer rotation is 0 degree
  \value rotationAbs90    Framebuffer rotation is 90 degree
  \value rotationAbs180   Framebuffer rotation is 180 degree
  \value rotationAbs270   Framebuffer rotation is 270 degree
 */

/*!
  \enum QTmGlobal::TmOrientationStatus
  Enumeration, convering the status of the framebuffer orientation.
  \value orientationUnknown    Unknown framebuffer orientation
  \value orientationLandscape  Landscape framebuffer orientation (width > height)
  \value orientationPortrait   Portrait framebuffer orientation (height < width)
 */

/*!
  \enum QTmGlobal::TmDeviceElement
  Enumeration, covering the different device elements in the device status message.
  \value keyLock      Key lock
  \value deviceLock   Device lock
  \value screenSaver  Screen saver / dimmed screen
  \value nightMode    Night mode
  \value voiceInput   Voice input
  \value micInput     Microphone input
  \value distraction  Driver distraction
 */

/*!
  \enum QTmGlobal::TmAppCategory
  Application category, used in UPnP Advertisements and VNC context information messages.
  \value AppUnknown                 Unknown application
  \value AppUi                      General UI framework
  \value AppUiHomeScreen            Homescreen
  \value AppUiMenu                  Menue screen
  \value AppUiNotification          Notification
  \value AppUiApplicationList       Application listing
  \value AppUiSettings              Settings

  \value AppPhone                   General phone call application
  \value AppPhoneContactList        Contact list
  \value AppPhoneCallLog            Call log

  \value AppMedia                   General media
  \value AppMediaMusic              Music
  \value AppMediaVideo              Video
  \value AppMediaGaming             Gaming
  \value AppMediaImage              Image

  \value AppMessaging               General messaging
  \value AppMessagingSms            SMS
  \value AppMessagingMms            MMS
  \value AppMessagingEmail          Email

  \value AppNavigation              General Navigation

  \value AppBrowser                 General Browser
  \value AppApplicationStore        Application Store

  \value AppProductivity            General Productivity
  \value AppProductivityViewer      Viewer
  \value AppProductivityEditor      Editor

  \value AppInformation             General Information
  \value AppInfoNews                News
  \value AppInfoWeather             Weather
  \value AppInfoStocks              Stocks
  \value AppInfoTravel              Travel
  \value AppInfoSports              Sports
  \value AppInfoClock               Clock

  \value AppSocialNetworking        General Social Networking

  \value AppPim                     General PIM
  \value AppPimCalendar             Calendar
  \value AppPimNotes                Notes

  \value AppUiLess                  General UI less applications
  \value AppUiLessAudioServer       Audio server
  \value AppUiLessAudioClient       Audio client
  \value AppUiLessVoiceCommand      Voice command engine

  \value AppSystem                  General system
  \value AppSystemPinDeviceUnlock   PIN input for device unlock
  \value AppSystemPinBluetooth      Bluetooth PIN code input
  \value AppSystemPassword          Other password input
  \value AppSystemVoiceConfirmation Voice command confirmation
*/
/*!
  \enum QTmGlobal::TmVisualCategory
  Content category of the application. Used as a bit mask
  \value VisualText            Textual content
  \value VisualVideo           Video content
  \value VisualImage           Image content
  \value VisualVectorGraphics  Vector graphics content
  \value VisualThreeDGraphics  3D graphics content
  \value VisualUserInterface   User Interface content
  \value VisualCarMode         Content optimized for in-car use
  \value VisualMisc            Misc. content
*/
/*!
  \enum QTmGlobal::TmVisualRules
  Driver distraction rules, which are followed from the application. Used as a bit mask.
  \value VisualMinFontSize      Application enforces a minimum font size.
  \value VisualNoVideo          Application does not show video content.
  \value VisualNoAutoScrolling  Application does not show automatically scrolling text.
  \value VisualMaxFeedbackTime  Application ensures a maximum feedback time.
*/
/*!
  \enum QTmGlobal::TmAudioCategory
  Audio category, used as a bit mask.
  \value AudioPhone           Phone audio
  \value AudioMediaOut        Media audio (output)
  \value AudioMediaIn         Media audio (input)
  \value AudioVoiceCommandOut Voice Command (output)
  \value AudioVoiceCommandIn  Voice Command (input)
  \value AudioMisc            Misc Audio
 */
/*!
  \enum QTmGlobal::TmTrustLevel
  Trust level of the application or content category information
  \value TrustUnknown         Trust not known
  \value TrustUser            User has set the respective values
  \value TrustSelfRegistered  Application not known to the device manufacturer has set the respective values
  \value TrustRegistered      Application known to the device manufacturer application has set the respective values.
  \value TrustCertified       Respective values are coming from a valid application certificate
*/
/*!
  \enum QTmGlobal::TmStatus
  Status of an application driving the display at the Terminal Mode server.
  \value StatusUnknown    Application status is not known
  \value StatusForeground Application is running in foreground, i.e. driving the display
  \value StatusBackground Application is running in background, i.e. not driving the display
  \value StatusNotrunning Application is not running.
*/
/*!
  \enum QTmGlobal::TmResourceStatus
  Status of the UPnP audio resource
  \value ResourceFree Resource is free
  \value ResourceBusy Resource is busy, but can be overwritten
  \value ResourceNA   Resource is not available.
 */
/*!
  \enum QTmGlobal::TmDirection
  Direction of the UPnP audio link.
  \value DirectionIn   Input at the Terminal Mode server
  \value DirectionOut  Output at the Terminal Mode server
  \value DirectionBi   Input and Output at the Terminal Mode server
  \value DirectionNone Direction information not available or not applicable .
 */
/*!
    \enum QTmGlobal::TmColorFormat
    Color format used during the VNC session. The VNC client defines the color format the server must use.
    \value RGBNone No color format defined.
    \value RGB888 32 bit per pixel, 24 bit color depth, Red = 8 bit, Green = 8 bit, Blue = 8 bit.
    \value RGB565 16 bit per pixel, 16 bit color depth, Red = 5 bit, Green = 6 bit, Blue = 5 bit.
    \value RGB555 16 bit per pixel, 15 bit color depth, Red = 5 bit, Green = 5 bit, Blue = 5 bit.
    \value RGB444 16 bit per pixel, 12 bit color depth, Red = 4 bit, Green = 4 bit, Blue = 4 bit.
    \value RGB343 16 bit per pixel, 10 bit color depth, Red = 3 bit, Green = 4 bit, Blue = 3 bit.
    \value Gray16 16 bit per pixel gray value
    \value Gray8   8 bit per pixel gray value
*/
/*!
  \enum QTmGlobal::TmRemoteProtocol
  Remote protocol used for a given application, as advertised over UPnP.
  \value VNC Virtual Networking Computing (VNC) protocol. Base prototocol used for remtoting the display.
  \value RTP Real-Time Protocol (RTP), used for audio streaming. Different payload types available.
  \value BT_A2DP Bluetooth A2DP, used for streaming media audio over Bluetooth
  \value BT_HFP Bluetooth Handsfree Profile (BT HFP), used for streaming phone call audio over Bluetooth
  \value DAP Device Attestation Protocol (DAP), used for initial attestation of the server hardware and software.
  \value NONE No remoting protocol used.
  \value PROPRIETARY Proprietary protocol used.
*/
/*!
  \enum QTmGlobal::TmKeyEvent
  Key event defintion for specified Terminal Mode key events.
  \value XK_Knob_2D_0_shift_right         Knob 0 event: Shfit - right
  \value XK_Knob_2D_0_shift_left          Knob 0 event: Shfit - left
  \value XK_Knob_2D_0_shift_up            Knob 0 event: Shfit - up
  \value XK_Knob_2D_0_shift_up_right      Knob 0 event: Shfit - up & right
  \value XK_Knob_2D_0_shift_up_left       Knob 0 event: Shfit - up & left
  \value XK_Knob_2D_0_shift_down          Knob 0 event: Shfit - down
  \value XK_Knob_2D_0_shift_down_right    Knob 0 event: Shfit - down & right
  \value XK_Knob_2D_0_shift_down_left     Knob 0 event: Shfit - down & left
  \value XK_Knob_2D_0_shift_push          Knob 0 event: Push
  \value XK_Knob_2D_0_shift_pull          Knob 0 event: Pull
  \value XK_Knob_2D_0_rotate_x            Knob 0 event: Rotate - x clockwise
  \value XK_Knob_2D_0_rotate_X            Knob 0 event: Rotate - x anti-clockwise
  \value XK_Knob_2D_0_rotate_y            Knob 0 event: Rotate - y clockwise
  \value XK_Knob_2D_0_rotate_Y            Knob 0 event: Rotate - y anti-clockwise
  \value XK_Knob_2D_0_rotate_z            Knob 0 event: Rotate - z clockwise
  \value XK_Knob_2D_0_rotate_Z            Knob 0 event: Rotate - z anti-clockwise
  \value XK_Knob_2D_1_shift_right         Knob 1 event: Shfit - right
  \value XK_Knob_2D_1_shift_left          Knob 1 event: Shfit - left
  \value XK_Knob_2D_1_shift_up            Knob 1 event: Shfit - up
  \value XK_Knob_2D_1_shift_up_right      Knob 1 event: Shfit - up & right
  \value XK_Knob_2D_1_shift_up_left       Knob 1 event: Shfit - up & left
  \value XK_Knob_2D_1_shift_down          Knob 1 event: Shfit - down
  \value XK_Knob_2D_1_shift_down_right    Knob 1 event: Shfit - down & right
  \value XK_Knob_2D_1_shift_down_left     Knob 1 event: Shfit - down & left
  \value XK_Knob_2D_1_shift_push          Knob 1 event: Push
  \value XK_Knob_2D_1_shift_pull          Knob 1 event: Pull
  \value XK_Knob_2D_1_rotate_x            Knob 1 event: Rotate - x clockwise
  \value XK_Knob_2D_1_rotate_X            Knob 1 event: Rotate - x anti-clockwise
  \value XK_Knob_2D_1_rotate_y            Knob 1 event: Rotate - y clockwise
  \value XK_Knob_2D_1_rotate_Y            Knob 1 event: Rotate - y anti-clockwise
  \value XK_Knob_2D_1_rotate_z            Knob 1 event: Rotate - z clockwise
  \value XK_Knob_2D_1_rotate_Z            Knob 1 event: Rotate - z anti-clockwise

  \value XK_Knob_2D_2_shift_right         Knob 2 event: Shfit - right
  \value XK_Knob_2D_2_shift_left          Knob 2 event: Shfit - left
  \value XK_Knob_2D_2_shift_up            Knob 2 event: Shfit - up
  \value XK_Knob_2D_2_shift_up_right      Knob 2 event: Shfit - up & right
  \value XK_Knob_2D_2_shift_up_left       Knob 2 event: Shfit - up & left
  \value XK_Knob_2D_2_shift_down          Knob 2 event: Shfit - down
  \value XK_Knob_2D_2_shift_down_right    Knob 2 event: Shfit - down & right
  \value XK_Knob_2D_2_shift_down_left     Knob 2 event: Shfit - down & left
  \value XK_Knob_2D_2_shift_push          Knob 2 event: Push
  \value XK_Knob_2D_2_shift_pull          Knob 2 event: Pull
  \value XK_Knob_2D_2_rotate_x            Knob 2 event: Rotate - x clockwise
  \value XK_Knob_2D_2_rotate_X            Knob 2 event: Rotate - x anti-clockwise
  \value XK_Knob_2D_2_rotate_y            Knob 2 event: Rotate - y clockwise
  \value XK_Knob_2D_2_rotate_Y            Knob 2 event: Rotate - y anti-clockwise
  \value XK_Knob_2D_2_rotate_z            Knob 2 event: Rotate - z clockwise
  \value XK_Knob_2D_2_rotate_Z            Knob 2 event: Rotate - z anti-clockwise

  \value XK_Knob_2D_3_shift_right         Knob 3 event: Shfit - right
  \value XK_Knob_2D_3_shift_left          Knob 3 event: Shfit - left
  \value XK_Knob_2D_3_shift_up            Knob 3 event: Shfit - up
  \value XK_Knob_2D_3_shift_up_right      Knob 3 event: Shfit - up & right
  \value XK_Knob_2D_3_shift_up_left       Knob 3 event: Shfit - up & left
  \value XK_Knob_2D_3_shift_down          Knob 3 event: Shfit - down
  \value XK_Knob_2D_3_shift_down_right    Knob 3 event: Shfit - down & right
  \value XK_Knob_2D_3_shift_down_left     Knob 3 event: Shfit - down & left
  \value XK_Knob_2D_3_shift_push          Knob 3 event: Push
  \value XK_Knob_2D_3_shift_pull          Knob 3 event: Pull
  \value XK_Knob_2D_3_rotate_x            Knob 3 event: Rotate - x clockwise
  \value XK_Knob_2D_3_rotate_X            Knob 3 event: Rotate - x anti-clockwise
  \value XK_Knob_2D_3_rotate_y            Knob 3 event: Rotate - y clockwise
  \value XK_Knob_2D_3_rotate_Y            Knob 3 event: Rotate - y anti-clockwise
  \value XK_Knob_2D_3_rotate_z            Knob 3 event: Rotate - z clockwise
  \value XK_Knob_2D_3_rotate_Z            Knob 3 event: Rotate - z anti-clockwise

  \value XK_ITU_Key_0                     ITU Key for 0
  \value XK_ITU_Key_1                     ITU Key for 1
  \value XK_ITU_Key_2                     ITU Key for 2
  \value XK_ITU_Key_3                     ITU Key for 3
  \value XK_ITU_Key_4                     ITU Key for 4
  \value XK_ITU_Key_5                     ITU Key for 5
  \value XK_ITU_Key_6                     ITU Key for 6
  \value XK_ITU_Key_7                     ITU Key for 7
  \value XK_ITU_Key_8                     ITU Key for 8
  \value XK_ITU_Key_9                     ITU Key for 9
  \value XK_ITU_Key_Asterix               ITU Key for *
  \value XK_ITU_Key_Pound                 ITU Key for #

  \value XK_Device_Phone_call             Phone call start
  \value XK_Device_Phone_end              Phone call end
  \value XK_Device_Soft_left              Left soft button
  \value XK_Device_Soft_middle            Middle soft button
  \value XK_Device_Soft_right             Right soft button
  \value XK_Device_Application            Shortcut to Application listing
  \value XK_Device_Ok                     Ok (Confirmation)
  \value XK_Device_Delete                 Delete (Backspace)
  \value XK_Device_Zoom_in                Zoom in
  \value XK_Device_Zoom_out               Zoom out
  \value XK_Device_Clear                  Clear current position
  \value XK_Device_Forward                Go one step forward
  \value XK_Device_Backward               Go one step backward
  \value XK_Device_Home                   Shortcut to home screen
  \value XK_Device_Search                 Shortcut to search function
  \value XK_Device_Menu                   Shortcut to (application) menu screen

  \value XK_Function_Key_0                Function key -  0
  \value XK_Function_Key_1                Function key -  1
  \value XK_Function_Key_2                Function key -  2
  \value XK_Function_Key_3                Function key -  3
  \value XK_Function_Key_4                Function key -  4
  \value XK_Function_Key_5                Function key -  5
  \value XK_Function_Key_6                Function key -  6
  \value XK_Function_Key_7                Function key -  7
  \value XK_Function_Key_8                Function key -  8
  \value XK_Function_Key_9                Function key -  9
  \value XK_Function_Key_10               Function key - 10
  \value XK_Function_Key_11               Function key - 11
  \value XK_Function_Key_12               Function key - 12
  \value XK_Function_Key_13               Function key - 13
  \value XK_Function_Key_14               Function key - 14
  \value XK_Function_Key_15               Function key - 15
  \value XK_Function_Key_16               Function key - 16
  \value XK_Function_Key_17               Function key - 17
  \value XK_Function_Key_18               Function key - 18
  \value XK_Function_Key_19               Function key - 19
  \value XK_Function_Key_20               Function key - 20
  \value XK_Function_Key_21               Function key - 21
  \value XK_Function_Key_22               Function key - 22
  \value XK_Function_Key_23               Function key - 23
  \value XK_Function_Key_24               Function key - 24
  \value XK_Function_Key_25               Function key - 25
  \value XK_Function_Key_26               Function key - 26
  \value XK_Function_Key_27               Function key - 27
  \value XK_Function_Key_28               Function key - 28
  \value XK_Function_Key_29               Function key - 29
  \value XK_Function_Key_30               Function key - 30
  \value XK_Function_Key_31               Function key - 31

  \value XK_Function_Key_255              Function key - 255

  \value XK_Multimedia_Play               Multimedia key - Play media stream
  \value XK_Multimedia_Pause              Multimedia key - Pause media stream
  \value XK_Multimedia_Stop               Multimedia key - Stop media stream
  \value XK_Multimedia_Forward            Multimedia key - Forward media stream
  \value XK_Multimedia_Rewind             Multimedia key - Rewind media stream
  \value XK_Multimedia_Next               Multimedia key - Next media track
  \value XK_Multimedia_Previous           Multimedia key - Previous media track
  \value XK_Multimedia_Mute               Multimedia key - Mute audio
  \value XK_Multimedia_Unmute             Multimedia key - Unmute audio
  \value XK_Multimedia_Photo              Multimedia key - Take a photo
 */
