/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QTmConfiguration.h"
#include "QTmRfbClient.h"

/*!
  \class QTmRfbClient

  \brief This class implements the RFB client protocol, including the Terminal Mode extensions, defined in the version 1.0.

  Default values
  \list
  \o QTmConfiguration::ConfigContextInfoEncoding = true
  \o QTmConfiguration::ConfigTerminalModeEncoding = true
  \o QTmConfiguration::ConfigFbOrientation = true
  \o QTmConfiguration::ConfigFbRotation = false
  \o QTmConfiguration::ConfigFbReplacement = false
  \o QTmConfiguration::ConfigDisplaySizeWidth, = 0
  \o QTmConfiguration::ConfigDisplaySizeHeight = 0
  \o QTmConfiguration::ConfigDisplayDistance = 0
  \o QTmConfiguration::ConfigVirtualKeyboard = true
  \o QTmConfiguration::ConfigKeyEventListing = true
  \o QTmConfiguration::ConfigKeyEventMapping = true
  \o QTmConfiguration::ConfigIncrementalUpdate = true
  \o QTmConfiguration::ConfigRunLengthEncoding = true
  \o QTmConfiguration::ConfigFbUpScaling = true
  \o QTmConfiguration::ConfigFbDownScaling = true
  \o QTmConfiguration::ConfigDesktopSizeEncoding = true
  \o QTmConfiguration::ConfigDisplayPixelWidth = 0
  \o QTmConfiguration::ConfigDisplayPixelHeight = 0
  \endlist
 */

const int MSG_FRAMEBUFFER_UPDATE          =   0;     // RFB FramebufferUpdate message identifier
const int MSG_SET_COLOUR_MAP_ENTRIES      =   1;     // RFB SetColourMapEntries message identifier
const int MSG_BELL                        =   2;     // RFB Bell message identifier
const int MSG_SERVER_CUT_TEXT             =   3;     // RFB ServerCutText message identifier

const int MSG_TERMINAL_MODE               = 128;     // RFB TerminalMode extension message identifier

const int MSG_SET_PIXEL_FORMAT            =   0;     // RFB SetPixelFormat message identifier
const int MSG_SET_ENCODINGS               =   2;     // RFB SetEncodings message identifier
const int MSG_FRAMEBUFFER_UPDATE_REQUEST  =   3;     // RFB FramebufferUpdateRequest message identifier
const int MSG_KEY_EVENT                   =   4;     // RFB KeyEvent message identifier
const int MSG_POINTER_EVENT               =   5;     // RFB PointerEvent message identifier
const int MSG_CLIENT_CUT_TEXT             =   6;     // RFB ClientCutText message identifier

//VNC Extensions message subtypes
const int TM_MSG_SERVER_DISPLAY_CONFIG    =   1;    // RFB ServerDisplayConfiguration message
const int TM_MSG_CLIENT_DISPLAY_CONFIG    =   2;    // RFB ClientDisplayConfiguration message
const int TM_MSG_SERVER_EVENT_CONFIG      =   3;    // RFB ServerEventConfiguration message
const int TM_MSG_CLIENT_EVENT_CONFIG      =   4;    // RFB ClientEventConfiguration message
const int TM_MSG_EVENT_MAPPING            =   5;    // RFB EventMapping message
const int TM_MSG_EVENT_MAPPING_REQ        =   6;    // RFB EventMappingRequest message
const int TM_MSG_KEY_EVENT_LISTING        =   7;    // RFB KeyEventListing message
const int TM_MSG_KEY_EVENT_LISTING_REQ    =   8;    // RFB KeyEventListingRequest message
const int TM_MSG_KEYBOARD_TRIGGER         =   9;    // RFB KeyboardTrigger message
const int TM_MSG_KEYBOARD_TRIGGER_REQ     =  10;    // RFB KeyboardTriggerRequest message
const int TM_MSG_DEVICE_STATUS            =  11;    // RFB DeviceStatus message
const int TM_MSG_DEVICE_STATUS_REQ        =  12;    // RFB DeviceStatusRequest message
const int TM_MSG_CONTENT_ATTESTATION_RESP =  13;    // RFB DeviceAttestationResponse message
const int TM_MSG_CONTENT_ATTESTATION_REQ  =  14;    // RFB DeviceAttestationRequest message
// Value 15 reserved
const int TM_MSG_FRAMEBUFFER_BLOCKING     =  16;    // RFB FramebufferBlockingNotification message
// Value 17 reserved
const int TM_MSG_AUDIO_BLOCKING           =  18;    // RFB AudioBlockingNotification message
// Value 19 reserved
const int TM_MSG_TOUCH_EVENT              =  20;    // RFB TouchEvent message
const int TM_MSG_FRAMEBUFFER_ALT_TEXT     =  21;    // RFB FramebufferAlternativeText message
const int TM_MSG_FRAMEBUFFER_ALT_TEXT_REQ =  22;    // RFB FramebufferAlternativeTextRequest message

const int EXT_MSG_TERMINAL_MODE           = 128;
const int EXT_ENC_RAW                     =   0;    // Encoding identifier for Raw encoding (i.e. no encoding applied)
const int EXT_ENC_DESKTOP_SIZE            =-223;
const int EXT_ENC_TERMINAL_MODE           =-523;
const int EXT_ENC_TM_CONTEXT_INFO         =-524;
const int EXT_ENC_RUN_LENGTH_ENCODING     =-525;    // Encoding identifier for Run Length Encoding (scan-line based)

#ifdef QT_DEBUG_TM_PERFORMANCE_COUNTER
#include <sys/timeb.h>
static struct timeb PerformanceStartTime;
struct timeb        PerformanceEndTime;
double              PerformanceDiffTime, PerformanceFpsCount;
int                 PerformanceSampleWindow = 100;
int                 PERFORMANCE_counter = 0;
int                 PERFORMANCE_number_of_rectangles = 0;
int	            PERFORMANCE_number_of_pixel = 0;
int                 PERFORMANCE_number_of_runs;
#endif


/*!
  \enum QTmRfbClient::QTmSessionStatus
  This enumberation lists the possible status values the VNC client can be in.
  \value RFB_SESSION_NONE RFB session is in default state.
  \value RFB_SESSION_CONNECTED_ACTIVE RFB session is initialized and active, i.e. framebuffer updates are ongoing.
  \value RFB_SESSION_CONNECTED_PASSIVE RFB session is initialized but passive, i.e. no framebuffer updates are ongoing.
  \value RFB_SESSION_SERVER_VERSION VNC client is receiving VNC server RFB protocol version
  \value RFB_SESSION_SECURITY_TYPES VNC client is receiving the supported security types
  \value RFB_SESSION_SECURITY_RESPONSE VNC client is receiving the security response
  \value RFB_SESSION_SERVER_INIT VNC client is receiving the RFB Server Init message
  \value ERROR_RFB_SESSION_DISCONNECTED Error, VNC session got disconnected.
  \value ERROR_WRONG_SERVER_VERSION Error, wrong RFB server version received from VNC server
  \value ERROR_WRONG_SECURITY_TYPES Error, wrong security type received from VNC server
  \value ERROR_WRONG_FRAMEBUFFER_ENCODING Error, unknown framebuffer encoding received from VNC server
  \value ERROR_WRONG_MSG_TYPE Error, wrong RFB message type received from VNC server
  \value ERROR_WRONG_FSM_STATE Error, wrong FSM state.
  \value ERROR_CANNOT_ALLOCATE_SURFACE Error, client cannot allocate framebuffer surface.
 */

// ********************************************************************
// *****            Class Constructor and Destructor              *****
// ********************************************************************
/*!
  Class Constructor.
  Set the default values. \a parent gives the objects parent.
 */
QTmRfbClient::QTmRfbClient(QObject *parent)
    : QObject(parent)
    , m_connected(false)
    , m_eventMap(0)
    , m_surface(0)
    , m_url(0)
    , m_socket(0)
    , m_rfbSessionStatus(RFB_SESSION_NONE)
    , m_keyEventCounter(0)
    , m_framebuffer(0)
{
#ifdef QT_DEBUG_TM_RFBCLIENT
    m_rfbConfigClient = new QTmConfiguration("(Client)");
    m_rfbConfigServer = new QTmConfiguration("(Server)");
#else
    m_rfbConfigClient = new QTmConfiguration();
    m_rfbConfigServer = new QTmConfiguration();
#endif
    m_eventMap        = new QHash<unsigned int, unsigned int>();
    m_rfbConfigClient->setConfiguration(QTmConfiguration::ConfigFbUpdates,     false);
    m_rfbConfigClient->setConfiguration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::RGB888);
    qDebug () << "QTmRfbClient::QTmRfbClient:" << "Instance constructed at" << this;
}
/*!
  Class destructure.
  Disconnect the session prior deletion.
 */
QTmRfbClient::~QTmRfbClient() {
    qDebug () << "QTmRfbClient::~QTmRfbClient:" << "Instance destroyed at" << this;
    if (m_connected)
        sessionStop();
}


// ********************************************************************
// *****                  VNC Sessiong Handling                   *****
// ********************************************************************
/*!
  \property QTmRfbClient::sessionActive
  \brief Holds the information, whether the VNC session is active or not.
 */
/*!
  \property QTmRfbClient::sessionUrl
  \brief Holds the URL to the VNC server. Is set to QUrl(), if client is not connected.
 */
/*!
  \fn QTmRfbClient::sessionConnected(QString serverName)
  Signal emitted from a RFB client to inform that the VNC client is connected to the server and
  the VNC session is initialized. The name of the VNC server is given in \a serverName
  This signal is emitted, when the client has received the RFB Server Init message.
 */
/*!
  \fn QTmRfbClient::sessionDisconnected()
  Signal emitted from a RFB client to inform that the VNC session is lost.
  This signal is emitted from the protocolFsm() on reception of an invalid message or whenever
  the TCP connection is broken while transmitting or receiving data.
 */
/*!
  Start a VNC session and connect the VNC client to a VNC server at the URL \a url.
  The TCP socket is connected and the readReady() signal is connected to the protocolFsm().
  Returns true, if the VNC session has been started. False is returned otherwise.
  Note: The VNC session is not connected (i.e. initialized), until the sessionConnected() signal is received.
 */
bool QTmRfbClient::sessionStart(QUrl url) {
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::sessionStart:" << "Start VNC Session with" << url;
#endif
    m_mutex.lock();
    if (m_connected) {
        m_mutex.unlock();
        return false;
    }
    m_socket = new QTcpSocket(this);
    m_socket->connectToHost(url.host(), url.port());
    if (!m_socket->waitForConnected()) {
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "QTmRfbClient::sessionStart:" << m_socket->errorString();
#endif
        m_mutex.unlock();
        sessionStop();
        return false;
    }
    m_connected = true;
    m_url = url;
    m_rfbSessionStatus = RFB_SESSION_SERVER_VERSION;
    connect(m_socket, SIGNAL(readyRead()),
            this,     SLOT  (protocolFsm()),
            Qt::AutoConnection);
    m_mutex.unlock();
    return true;
}
/*!
  Stop the current VNC session.
  The surface, holding the framebuffer update is de-allocated. The TCP socket is disconnected.
 */
void QTmRfbClient::sessionStop() {
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::sessionStop:" << "Stop VNC Session with" << m_url;
#endif
    m_mutex.lock();
    if (!m_connected) {
        m_mutex.unlock();
        return;
    }
    m_connected = false;
    m_url       = QUrl();
    m_rfbSessionStatus = RFB_SESSION_NONE;
    //deallocate surface
    if (m_surface)
        m_surface->deallocateSurface();
    //disconnect socket
    disconnect(m_socket, SIGNAL(readyRead()), this, SLOT(protocolFsm()));
    m_socket->disconnectFromHost();
    m_socket->close();
    delete m_socket;
    m_socket = 0;
    m_mutex.unlock();
    return;
}
/*!
  Return true if the session is connected. False is retured otherwise.
 */
bool QTmRfbClient::sessionActive() {
    return m_connected;
}
/*!
  Returns the URL to the VNC server, the client is connected to.
 */
QUrl QTmRfbClient::sessionUrl() {
    return m_url;
}
/*!
  Returns the current RFB Session Status.
 */
QTmRfbClient::QTmSessionStatus QTmRfbClient::sessionStatus() {
    return m_rfbSessionStatus;
}
/*!
  Returns the pointer to the currently valid key event map.
 */
QHash<unsigned int, unsigned int> *QTmRfbClient::eventMap() {
    return m_eventMap;
}
/*!
  The the surface, the VNC client must use for the framebuffer updates.
  \a surface provides the pointer to the instance to use. The instance is already allocated.
 */
void QTmRfbClient::setSurface(QTmSurface *surface) {
    m_surface = surface;
}


// ********************************************************************
// *****                    VNC Configuration                     *****
// ********************************************************************
/*!
  Change the VNC client configuration.
  The \a key is set to \a value.
 */
void QTmRfbClient::setClientConfig(QTmConfiguration::QTmConfig key, unsigned int value) {
    m_rfbConfigClient->setConfiguration(key, value);
}
/*!
  Returns the VNC client configuration of \a key.
  In case the key has not been defined, the \a defaultValue is returned instead.
 */
unsigned int QTmRfbClient::clientConfig(QTmConfiguration::QTmConfig key, unsigned int defaultValue) {
    return m_rfbConfigClient->configuration(key, defaultValue);
}
/*!
  Returns the VNC server configuration of \a key.
  In case the key has not been defined, the \a defaultValue is returned instead.
 */
unsigned int QTmRfbClient::serverConfig(QTmConfiguration::QTmConfig key, unsigned int defaultValue) {
    return m_rfbConfigServer->configuration(key, defaultValue);
}


/**
 * RFB Protocol Handshake
 */
void QTmRfbClient::protocolHandshake() {
    unsigned char cmdBuffer[12];
    receiveData(12, (char *)cmdBuffer);
    int major = ((int) cmdBuffer[ 6]-48) + ((int) cmdBuffer[5]-48)*10 + ((int) cmdBuffer[4]-48)*100;
    int minor = ((int) cmdBuffer[10]-48) + ((int) cmdBuffer[9]-48)*10 + ((int) cmdBuffer[8]-48)*100;
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::protocolHandshake:" << "Receive Server RFB Protocol Version" << major << "." << minor;
#endif
    if (major != 3 && (minor != 3 || minor != 7 || minor != 8)) {
        m_rfbSessionStatus = ERROR_WRONG_SERVER_VERSION;
        return;
    }
    m_rfbConfigClient->setConfiguration(QTmConfiguration::ConfigRfbVersionMajor, 3);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigRfbVersionMajor, 3);
    m_rfbConfigClient->setConfiguration(QTmConfiguration::ConfigRfbVersionMinor, minor);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigRfbVersionMinor, minor);
    m_rfbSessionStatus = RFB_SESSION_SECURITY_TYPES;
    switch (minor) {
    case 3:
        transmitData(12, (char*)"RFB 003.003\n");
        break;
    case 7:
        transmitData(12, (char*)"RFB 003.007\n");
        break;
    case 8:
        transmitData(12, (char*)"RFB 003.008\n");
        break;
    default:
        m_rfbSessionStatus = ERROR_WRONG_SERVER_VERSION;
        return;
    }
}


/**
 * RFB Security Handshake
 */
void QTmRfbClient::securityHandshake() {
    unsigned char cmdBuffer[4];
    if (m_rfbConfigServer->configuration(QTmConfiguration::ConfigRfbVersionMinor) == 3) { //RFB Server decides the security type
        receiveData(1, (char*)cmdBuffer);
        if (cmdBuffer[0] == 0x01) {
            m_rfbSessionStatus = RFB_SESSION_SERVER_INIT;
            //Send RFB Client Init message
            clientInit();
        }
        else
            m_rfbSessionStatus = ERROR_WRONG_SECURITY_TYPES;
        return;
    }
    else { //Go through list to find if type 0x01 (None) is supported.
        receiveData(1, (char*)cmdBuffer);
        //Check number of security types
        int numberOfSecurityTypes = (int) cmdBuffer[0];
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "(QTmRfbClient::securityHandshake):" << "Number of security types" << numberOfSecurityTypes;
#endif
        if (numberOfSecurityTypes == 0) {
            qWarning() << "(QTmRfbClient::securityHandshake):" << "Security handshake failed.";
            //Read Security Failure Reason
            receiveData(4, (char*)cmdBuffer);
            int length = (cmdBuffer[0] << 24) | (cmdBuffer[1] << 16) | (cmdBuffer[2] << 8) | cmdBuffer[3];
#ifdef QT_DEBUG_TM_RFBCLIENT
            qDebug() << "(QTmRfbClient::securityHandshake):" << "Length of reason string" << length;
#endif
            unsigned char *failureReason = (unsigned char *) malloc(length+1);
            if (failureReason != 0) {
                receiveData(length, (char *)failureReason);
                failureReason[length] = '\0';
                qWarning() << "(QTmRfbClient::securityHandshake):" << "Failure reason:" << QString((const char *) failureReason);
            }
            delete failureReason;
            m_rfbSessionStatus = ERROR_ZERO_SECURITY_TYPES;
        }
        else {
            //Request Security Type 0x01 (None)
            //No need to evaluate the available types
            for (int i=numberOfSecurityTypes; i>0; i--) {
                receiveData(1, (char*)cmdBuffer);
            }
#ifdef QT_DEBUG_TM_RFBCLIENT
            qDebug() << "(QTmRfbClient::securityHandshake):" << "Security Type 0x01 (None) selected";
#endif
            cmdBuffer[0]= 0x01;
            transmitData(1, (char*)cmdBuffer);
            m_rfbSessionStatus = RFB_SESSION_SECURITY_RESPONSE;
        }
    }
}


void QTmRfbClient::securityResponse() {
    unsigned char cmdBuffer[4];
    receiveData(4, (char*)cmdBuffer);
    if (cmdBuffer[0] == 0x00 && cmdBuffer[1] == 0x00 && cmdBuffer[2] == 0x00 && cmdBuffer[3] == 0x00) {
        m_rfbSessionStatus = RFB_SESSION_SERVER_INIT;
        //Send RFB Client Init message
        clientInit();
    }
    else {
        qWarning() << "(QTmRfbClient::securityResponse):" << "Security handshake failed.";
        if (m_rfbConfigServer->configuration(QTmConfiguration::ConfigRfbVersionMinor) != 8)
            return;
        //Read Security Failure Reason
        receiveData(4, (char*)cmdBuffer);
        int length = (cmdBuffer[0] << 24) | (cmdBuffer[1] << 16) | (cmdBuffer[2] << 8) | cmdBuffer[3];
        unsigned char *failureReason = (unsigned char *) malloc(length+1);
        if (failureReason != 0) {
            receiveData(length, (char *)failureReason);
            failureReason[length] = '\0';
            qWarning() << "(QTmRfbClient::securityResponse):" << "Failure reason:" << failureReason;
        }
        delete failureReason;
        m_rfbSessionStatus = ERROR_WRONG_SECURITY_TYPES;
    }
}


/**
 * RFB Client Server Initialization
 */
void QTmRfbClient::clientInit() {
    unsigned char cmdBuffer[1];
    cmdBuffer[0] = 0x00; //Sharing is set to false
    transmitData(1, (char*)cmdBuffer);
}

void QTmRfbClient::serverInit() {
    unsigned char cmdBuffer[24];
    //RFB Server Init Message
    receiveData(24, (char *)cmdBuffer);
    //Famebuffer Size
    unsigned int width  = (cmdBuffer[0] << 8) | cmdBuffer[1];
    unsigned int height = (cmdBuffer[2] << 8) | cmdBuffer[3];
    //Color Format
    int bpp         = cmdBuffer[4];
    int depth       = cmdBuffer[5]; //Note: Ingnore ByteOrder at cmdBuffer[6]
    int true_color  = cmdBuffer[7];
    int red_max     = (cmdBuffer[8]  << 8) | cmdBuffer[9];
    int green_max   = (cmdBuffer[10] << 8) | cmdBuffer[11];
    int blue_max    = (cmdBuffer[12] << 8) | cmdBuffer[13];
    int red_shift   = cmdBuffer[14];
    int green_shift = cmdBuffer[15];
    int blue_shift  = cmdBuffer[16];
    if (true_color == 0)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::RGBNone);
    else if (bpp  == 32   && depth       == 24   &&
        red_max   == 0xFF && green_max   == 0xFF && blue_max   == 0xFF &&
        red_shift == 0x10 && green_shift == 0x08 && blue_shift == 0x00)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::RGB888);
    else if (bpp  == 16   && depth       == 16   &&
        red_max   == 0x1F && green_max   == 0x3F && blue_max   == 0x1F &&
        red_shift == 0x0B && green_shift == 0x05 && blue_shift == 0x00)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::RGB565);
    else if (bpp  == 16   && depth       == 15   &&
        red_max   == 0x1F && green_max   == 0x1F && blue_max   == 0x1F &&
        red_shift == 0x0A && green_shift == 0x05 && blue_shift == 0x00)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::RGB555);
    else if (bpp  == 16   && depth       == 12   &&
        red_max   == 0x0F && green_max   == 0x0F && blue_max   == 0x0F &&
        red_shift == 0x08 && green_shift == 0x04 && blue_shift == 0x00)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::RGB444);
    else if (bpp  == 16   && depth       == 10   &&
        red_max   == 0x07 && green_max   == 0x0F && blue_max   == 0x07 &&
        red_shift == 0x07 && green_shift == 0x03 && blue_shift == 0x00)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::RGB343);
    else if (bpp  == 16   && depth       == 16   &&
        red_max   == 0xFF && green_max   == 0x00 && blue_max   == 0x00 &&
        red_shift == 0x00 && green_shift == 0x00 && blue_shift == 0x00)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::Gray16);
    else if (bpp  == 8    && depth       == 8    &&
        red_max   == 0x0F && green_max   == 0x00 && blue_max   == 0x00 &&
        red_shift == 0x00 && green_shift == 0x00 && blue_shift == 0x00)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::Gray8);
    else
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::RGBNone);
    //Client follows Server Color format, if no color format has been defined otherwise
    if (m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::RGBNone) == (int) QTmGlobal::RGBNone)
        m_rfbConfigClient->setConfiguration(QTmConfiguration::ConfigFbColorFormat, m_rfbConfigServer->configuration(QTmConfiguration::ConfigFbColorFormat));
    //VNC Server name
    unsigned int length = (cmdBuffer[20] << 24) | (cmdBuffer[21] << 16) | (cmdBuffer[22] << 8) | cmdBuffer[23];
    unsigned char *serverName = (unsigned char *) malloc(length+1);
    receiveData(length, (char *)serverName);
    serverName[length] = '\0';
    //Create new image
    desktopSizePseudoEncoding(QSize(width, height));
    //VNC Client is now fully connected
    m_rfbSessionStatus = QTmRfbClient::RFB_SESSION_CONNECTED_PASSIVE;
    //Send setPixelFormat()
    setPixelFormat();
    //RFB SetEncodings messages
    if (m_rfbConfigClient->configuration(QTmConfiguration::ConfigRunLengthEncoding, true))
        setEncodings(EXT_ENC_RUN_LENGTH_ENCODING);
    setEncodings(EXT_ENC_RAW);
    if (m_rfbConfigClient->configuration(QTmConfiguration::ConfigDesktopSizeEncoding, true))
        setEncodings(EXT_ENC_DESKTOP_SIZE);
    if (m_rfbConfigClient->configuration(QTmConfiguration::ConfigContextInfoEncoding, true))
        setEncodings(EXT_ENC_TM_CONTEXT_INFO);
    if (m_rfbConfigClient->configuration(QTmConfiguration::ConfigTerminalModeEncoding, true))
        setEncodings(EXT_ENC_TERMINAL_MODE);
    else
        triggerFramebufferUpdates();
    qDebug() << "QTmRfbClient:" << "VNC Connected";
    emit sessionConnected(QString((char *)serverName));
#ifdef QT_DEBUG_TM_PERFORMANCE_COUNTER
    ftime(&PerformanceStartTime);
#endif
}


/**
 * Send RFB SetEncoding message to the RFB server.
 */
void QTmRfbClient::setEncodings(unsigned int encoding) {
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::setEncodings:" << "Send Set Encoding message" << (int) encoding;
#endif
    unsigned char cmdBuffer[8];
    cmdBuffer[0] = MSG_SET_ENCODINGS;
    cmdBuffer[1] = 0x00;
    cmdBuffer[2] = 0x00;
    cmdBuffer[3] = 0x01;
    cmdBuffer[4] = encoding >> 24;
    cmdBuffer[5] = encoding >> 16;
    cmdBuffer[6] = encoding >>  8;
    cmdBuffer[7] = encoding;
    transmitData(8, (char*)cmdBuffer);
}


/**
 * Send RFB SetPixelFormat message to the RFB server.
 */
void QTmRfbClient::setPixelFormat() {
    unsigned char cmdBuffer[20];
    bzero(cmdBuffer, 20);
    cmdBuffer[0]  = MSG_SET_PIXEL_FORMAT;
    cmdBuffer[6]  = 0x01; //Byte order flag
    cmdBuffer[7]  = 0x01; //True color flag
    QTmGlobal::TmColorFormat color = (QTmGlobal::TmColorFormat) m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::RGB888);
    switch (color) {
    case QTmGlobal::RGB565:
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::setPixelFormat:" << "Set Color Format" << "RGB565";
#endif
        cmdBuffer[4]  = 16;   //BPP
        cmdBuffer[5]  = 16;   //Color Depth
        cmdBuffer[9]  = 0x1F; //Red Max (LSB)
        cmdBuffer[11] = 0x3F; //Green Max (LSB)
        cmdBuffer[13] = 0x1F; //Blue Max (LSB)
        cmdBuffer[14] = 0x0B; //Red Shift
        cmdBuffer[15] = 0x05; //Green Shift
        cmdBuffer[16] = 0x00; //Blue Shift
        break;
    case QTmGlobal::RGB555:
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::setPixelFormat:" << "Set Color Format" << "RGB555";
#endif
        cmdBuffer[4]  = 16;
        cmdBuffer[5]  = 15;
        cmdBuffer[9]  = 0x1F;
        cmdBuffer[11] = 0x1F;
        cmdBuffer[13] = 0x1F;
        cmdBuffer[14] = 0x0A;
        cmdBuffer[15] = 0x05;
        cmdBuffer[16] = 0x00;
        break;
    case QTmGlobal::RGB444:
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::setPixelFormat:" << "Set Color Format" << "RGB444";
#endif
        cmdBuffer[4]  = 16;
        cmdBuffer[5]  = 12;
        cmdBuffer[9]  = 0x0F;
        cmdBuffer[11] = 0x0F;
        cmdBuffer[13] = 0x0F;
        cmdBuffer[14] = 0x08;
        cmdBuffer[15] = 0x04;
        cmdBuffer[16] = 0x00;
        break;
    case QTmGlobal::RGB343:
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::setPixelFormat:" << "Set Color Format" << "RGB343";
#endif
        cmdBuffer[4]  = 16;
        cmdBuffer[5]  = 10;
        cmdBuffer[9]  = 0x07;
        cmdBuffer[11] = 0x0F;
        cmdBuffer[13] = 0x07;
        cmdBuffer[14] = 0x07;
        cmdBuffer[15] = 0x03;
        cmdBuffer[16] = 0x00;
        break;
    case QTmGlobal::RGB888:
    default:
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::setPixelFormat:" << "Set Color Format" << "ARGB888";
#endif
        cmdBuffer[4]  = 32;
        cmdBuffer[5]  = 24;
        cmdBuffer[9]  = 0xFF;
        cmdBuffer[11] = 0xFF;
        cmdBuffer[13] = 0xFF;
        cmdBuffer[14] = 0x10;
        cmdBuffer[15] = 0x08;
        cmdBuffer[16] = 0x00;
        break;
    }
    transmitData(20, (char*)cmdBuffer);
}


/*!
  Send an RFB Key Event message, as given in \a event.
  If the event is key-press event, the key event counter is set to zero.
  The \a event is not deleted.
 */
void QTmRfbClient::keyEvent(QTmKeyEvent *event) {
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::keyEvent:" << "Send key event" << event->key() << event->pressed();
#endif
    unsigned char cmdBuffer[8];
    cmdBuffer[0] = MSG_KEY_EVENT;
    cmdBuffer[1] = event->pressed();
    cmdBuffer[2] = 0x00;
    cmdBuffer[3] = 0x00;
    cmdBuffer[4] = event->key() >> 24;
    cmdBuffer[5] = event->key() >> 16;
    cmdBuffer[6] = event->key() >>  8;
    cmdBuffer[7] = event->key();
    //Increase key event counter on press events only
    if (event->pressed())
        m_keyEventCounter++;
    transmitData(8, (char*)cmdBuffer);
}
/*!
  Send an RFB Pointer Event message, as given in \a event.
  The \a event is not deleted.
 */
void QTmRfbClient::pointerEvent(QTmPointerEvent *event) {
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::pointerEvent:" << "Send pointer event" << event->point();
#endif
    unsigned char cmdBuffer[6];
    cmdBuffer[0] = MSG_POINTER_EVENT;
    cmdBuffer[1] = event->pressed();
    cmdBuffer[2] = event->point().x() >> 8;
    cmdBuffer[3] = event->point().x();
    cmdBuffer[4] = event->point().y() >> 8;
    cmdBuffer[5] = event->point().y();
    transmitData(6, (char *) cmdBuffer);
}
/*!
  Send an RFB Touch Event message, as given in \a touchEvent. This message is a Terminal Mode VNC extension message.
  This message sends all simultaneous individual events within a single message.
  These individual events are removed from the QList, while sending. \a touchEvent itself is not deleted though.
 */
void QTmRfbClient::touchEvent(QList<QTmTouchEvent *> *touchEvent) {
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::touchEvent:" << "Send touch event" << touchEvent->size();
#endif
    int n = touchEvent->size() & 0xFF;
    int j = 5;
    QByteArray buffer(5+n*6, 0);
    buffer[0] = EXT_MSG_TERMINAL_MODE;
    buffer[1] = TM_MSG_TOUCH_EVENT;
    buffer[2] = (1+n*6) >> 8;
    buffer[3] = (1+n*6);
    buffer[4] = n;
    foreach(QTmTouchEvent *event, *touchEvent) {
        buffer[j++] = event->point().x() >> 8;
        buffer[j++] = event->point().x();
        buffer[j++] = event->point().y() >> 8;
        buffer[j++] = event->point().y();
        buffer[j++] = event->eventId();
        buffer[j++] = event->pressure();
    }
    transmitData(5+n*6, (char*)buffer.constData());
}

/*!
  Send an RFB ClientCutText message. The message to be sent is given in \a text.
  The text is translated into Latin1. Other character sets are not supported.
 */
void QTmRfbClient::clientCutText(QString text) {
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::clientCutText:" << "Send Client Touch Text" << text;
#endif
    unsigned long int length = text.size();
    unsigned char *cmdBuffer = (unsigned char *) malloc(8+length);
    cmdBuffer[0] = MSG_CLIENT_CUT_TEXT;
    cmdBuffer[1] = 0x00;
    cmdBuffer[2] = 0x00;
    cmdBuffer[3] = 0x00;
    cmdBuffer[4] = length >> 24;
    cmdBuffer[5] = length >> 16;
    cmdBuffer[6] = length >>  8;
    cmdBuffer[7] = length;
    memcpy(cmdBuffer+8, text.toLatin1(), length);
    transmitData(8+length, (char *) cmdBuffer);
}
void QTmRfbClient::serverCutText() {
    unsigned char cmdBuffer[8];
    receiveData(7, (char*)cmdBuffer+1);
    int length = (cmdBuffer[4] << 24) | (cmdBuffer[5] << 16) | (cmdBuffer[6] << 8) | (cmdBuffer[7]);
    char *serverText = (char *) malloc(length + 1);
    receiveData(length, serverText);
    serverText[length] = '\0';
    QString copyText(serverText);
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(copyText, QClipboard::Clipboard);
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::serverCutText:" << "Receive Client Touch Text" << serverText;
#endif
    free(serverText);
}


/**
 * Receive RFB SetColourMapEntries message from the RFB server.
 */
void QTmRfbClient::setColourMapEntries() {
    unsigned char cmdBuffer[6];
    receiveData(5, (char*)cmdBuffer+1);
    int n = (cmdBuffer[4] << 8) | (cmdBuffer[5]);
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient:" << "Received " << n << "colours. NOTE: Colour Maps are not supported. Ignore colours";
#endif
    for (int i=n; i>0; i++)
        receiveData(6, (char*)cmdBuffer);
}


/**
 * Receive RFB Bell message from the RFB server and call system beep function.
 */
void QTmRfbClient::bell() {
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient:" << "BELL";
#endif
    QApplication::beep();
}


/*!
  Send an RFB Framebuffer Update Request message to the RFB server.
  The client will request a non-incremental update for the entire framebuffer region.
  The message is only send, when framebuffer updates have previously been disabled.
 */
void QTmRfbClient::triggerFramebufferUpdates() {
    if (m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbUpdates, false))
        framebufferUpdateRequest(false);
}

void QTmRfbClient::framebufferUpdateRequest(bool incremental) {
    if (!m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbUpdates, false))
        return;
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::framebufferUpdateRequest:" << "Send Framebuffer Update Request message"
             << QRect(0, 0,
                      m_rfbConfigClient->configuration(QTmConfiguration::ConfigRequestedFbWidth, 0),
                      m_rfbConfigClient->configuration(QTmConfiguration::ConfigRequestedFbHeight, 0))
             << incremental;
#endif
    unsigned char cmdBuffer[10];
    cmdBuffer[0]  = MSG_FRAMEBUFFER_UPDATE_REQUEST;
    cmdBuffer[1]  = (incremental) ? 1 : 0;
    cmdBuffer[2]  = 0;
    cmdBuffer[3]  = 0;
    cmdBuffer[4]  = 0;
    cmdBuffer[5]  = 0;
    cmdBuffer[6]  = m_rfbConfigClient->configuration(QTmConfiguration::ConfigRequestedFbWidth, 0) >> 8;
    cmdBuffer[7]  = m_rfbConfigClient->configuration(QTmConfiguration::ConfigRequestedFbWidth, 0);
    cmdBuffer[8]  = m_rfbConfigClient->configuration(QTmConfiguration::ConfigRequestedFbHeight, 0) >> 8;
    cmdBuffer[9]  = m_rfbConfigClient->configuration(QTmConfiguration::ConfigRequestedFbHeight, 0);
    transmitData(10, (char*)cmdBuffer);
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::framebufferUpdateRequest:" << "... done";
#endif
}

void QTmRfbClient::framebufferUpdate() {
    unsigned char cmdBuffer[12];
    unsigned int i;
    unsigned int nRectangle;
#ifdef QT_DEBUG_TM_PERFORMANCE_COUNTER
    bool PERFORMANCE_sample = false;
#endif
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "QTmRfbClient::framebufferUpdate:" << "Receive Framebuffer Update message";
#endif
    receiveData(3, (char *)cmdBuffer+1);
    nRectangle = (cmdBuffer[2] << 8) | cmdBuffer[3];
    for (i=nRectangle; i>0; i--) {
        receiveData(12, (char*)cmdBuffer);
        unsigned int x      = (cmdBuffer[0] << 8)  | cmdBuffer[1];
        unsigned int y      = (cmdBuffer[2] << 8)  | cmdBuffer[3];
        unsigned int width  = (cmdBuffer[4] << 8)  | cmdBuffer[5];
        unsigned int height = (cmdBuffer[6] << 8)  | cmdBuffer[7];
        int          enc    = (cmdBuffer[8] << 24) | (cmdBuffer[9] << 16) | (cmdBuffer[10] << 8) | cmdBuffer[11];
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "QTmRfbClient::framebufferUpdate:" << "Rectangle" << i << QRect(x, y, width, height);
#endif
        switch (enc) {
        case EXT_ENC_RAW:
            if (i == 1)
                framebufferUpdateRequest(m_rfbConfigClient->configuration(QTmConfiguration::ConfigIncrementalUpdate, true));
            if (width == 0 || height == 0)
                break;
#ifdef QT_DEBUG_TM_PERFORMANCE_COUNTER
            PERFORMANCE_number_of_pixel += height * width;
            PERFORMANCE_number_of_rectangles++;
            PERFORMANCE_sample = true;
#endif
            rawEncoding(x, y, width, height);
            //Update the graphical user interface
            if (!m_surface)
                return;
            m_surface->updateSurface(QRect(x, y, width, height));
            break;
        case EXT_ENC_RUN_LENGTH_ENCODING:
            if (i == 1)
                framebufferUpdateRequest(m_rfbConfigClient->configuration(QTmConfiguration::ConfigIncrementalUpdate, true));
            if (width == 0 || height == 0)
                break;
#ifdef QT_DEBUG_TM_PERFORMANCE_COUNTER
            PERFORMANCE_number_of_rectangles++;
            PERFORMANCE_number_of_pixel += height * width;
            PERFORMANCE_number_of_runs  += rleEncoding(x, y, width, height);
            PERFORMANCE_sample = true;
#else
            rleEncoding(x, y, width, height);
#endif
            //Update the graphical user interface
            if (!m_surface)
                return;
            m_surface->updateSurface(QRect(x, y, width, height));
            break;
        case EXT_ENC_DESKTOP_SIZE:
            desktopSizePseudoEncoding(QSize ((cmdBuffer[4] << 8) | cmdBuffer[5], (cmdBuffer[6] << 8) | cmdBuffer[7]));
            if (i == 1)
                framebufferUpdateRequest(false);
            break;
        case EXT_ENC_TM_CONTEXT_INFO:
            if (i == 1)
                framebufferUpdateRequest(m_rfbConfigClient->configuration(QTmConfiguration::ConfigIncrementalUpdate, true));
            contextInformationPseudoEncoding(cmdBuffer);
            break;
        default:
            m_rfbSessionStatus = ERROR_WRONG_FRAMEBUFFER_ENCODING;
            qDebug() << "ERROR" << "Unknown Framebuffer Encoding";
            break;
        }
    }
#ifdef QT_DEBUG_TM_PERFORMANCE_COUNTER
    if (!PERFORMANCE_sample)
        return;
    PERFORMANCE_counter++;
    if ((PERFORMANCE_counter % PerformanceSampleWindow) == 0)
    {
        ftime(&PerformanceEndTime);
        PerformanceDiffTime = (1000.0*PerformanceEndTime.time   + PerformanceEndTime.millitm -
                               1000.0*PerformanceStartTime.time - PerformanceStartTime.millitm)/
                PerformanceSampleWindow;
        PerformanceFpsCount = 1000.0/PerformanceDiffTime;
        qDebug("***** \nPerformance: After %i(th) FrameBufferUpdates", PERFORMANCE_counter);
        qDebug("***** - Achieved framerate/s   %.2f", PerformanceFpsCount);
        qDebug("***** - Number of Pixels       %i",   PERFORMANCE_number_of_pixel);
        qDebug("***** - Average rectangle size %.2f", (double) PERFORMANCE_number_of_pixel / (double) PERFORMANCE_number_of_rectangles);
        qDebug("***** - Average Run Length     %.2f", (double) PERFORMANCE_number_of_pixel / (double) PERFORMANCE_number_of_runs);
        PERFORMANCE_number_of_pixel = 0;
        PERFORMANCE_number_of_rectangles = 0;
        PERFORMANCE_number_of_runs = 0;
        ftime(&PerformanceStartTime);
    }
#endif
}


// ********************************************************************
// *****              Display Configuration message               *****
// ********************************************************************
/*!
  \fn QTmRfbClient::displayConfigurationReceived()
  Signal emitted from a RFB client to inform about an server display configuration message received.
  The receiving component should use this information to decide on the pixel format to request.
 */
/*!
  Receive an RFB Server Display Configuration message. This message is a Terminal Mode VNC extension message.
  The server display configuration message provides information about the display properties at the VNC server.
  This includes the following elements:
  \list
    \o Termina Mode Server version (major and minor)
    \o Supported framebuffer configurations (orientation, rotaton, scaling, replacement
    \o Relative pixel width
    \o Supported color formats
  \endlist
  The received information is stored in the server configuration isnstace. After emitting the
  displayConfigurationReceived() signal, the client sends its own Client Display Configuration message automatically.
  Returns the number of bytes read.
 */
int QTmRfbClient::serverDisplayConfig() {
    unsigned char cmdBuffer[12];
    int read = receiveData(12, (char*)cmdBuffer);
    if (read < 12) {
        qWarning() << "QRfbClientTm:" << "Error receiving Server Display Configuration";
        return read;
    }
    //Terminal Mode version
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigTmVersionMajor, ((int) cmdBuffer[0])-48);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigTmVersionMinor, ((int) cmdBuffer[1])-48);
    //Framebuffer Configuration
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbOrientation,  (cmdBuffer[3]  >> 0) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbRotation,     (cmdBuffer[3]  >> 1) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbUpScaling,    (cmdBuffer[3]  >> 2) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbDownScaling,  (cmdBuffer[3]  >> 3) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbReplacement,  (cmdBuffer[3]  >> 4) & 0x01);
    //Relative Pixel Size
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigPixelWidth,     (cmdBuffer[4]  << 8) | cmdBuffer[5]);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigPixelHeight,    (cmdBuffer[6]  << 8) | cmdBuffer[7]);
    //Pixel Format support
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigColorARGB888,   (cmdBuffer[11] >> 0) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigColorRGB888,    (cmdBuffer[10] >> 0) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigColorRGB565,    (cmdBuffer[9]  >> 0) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigColorRGB555,    (cmdBuffer[9]  >> 1) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigColorRGB444,    (cmdBuffer[9]  >> 1) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigColorRGB343,    (cmdBuffer[9]  >> 1) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigColorGray16,    (cmdBuffer[8]  >> 0) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigColorGray8,     (cmdBuffer[8]  >> 1) & 0x01);
    //Emit signal on message reception
    emit displayConfigurationReceived();
    //Send response message
     clientDisplayConfig();
    return 12;
}
/*!
  Sends an RFB Client Display Configuration message. This message is a Terminal Mode VNC extension message.
  The client display configuration message provides information about the display properties at the VNC Client.
  This includes the following elements:
  \list
    \o Termina Mode Server version (major and minor)
    \o Supported framebuffer configurations (orientation, rotaton, scaling, replacement
    \o Screen resolution
    \o Phyiscal screen dimensions and distance from the driver
  \endlist
  The transmitted information is taken from the client configuration isnstace.
 */
void QTmRfbClient::clientDisplayConfig() {
    unsigned char cmdBuffer[18];
    cmdBuffer[0]  = EXT_MSG_TERMINAL_MODE;
    cmdBuffer[1]  = TM_MSG_CLIENT_DISPLAY_CONFIG;
    cmdBuffer[2]  = 0x00;
    cmdBuffer[3]  = 0x0E;
    cmdBuffer[4]  = 0x01; //Terminal Mode Major version is 1
    cmdBuffer[5]  = 0x00; //Terminal Mode Minor version is 0
    //Framebuffer Configuration
    cmdBuffer[6]  = 0x00;
    cmdBuffer[7]  = ((m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbOrientation,  true) & 0x01) << 0) |
                    ((m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbRotation,    false) & 0x01) << 1) |
                    ((m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbUpScaling,    true) & 0x01) << 2) |
                    ((m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbDownScaling,  true) & 0x01) << 3) |
                    ((m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbReplacement, false) & 0x01) << 4);
    //Client display sizes
    cmdBuffer[8]  = m_rfbConfigClient->configuration(QTmConfiguration::ConfigDisplayPixelWidth,  0) >> 8;
    cmdBuffer[9]  = m_rfbConfigClient->configuration(QTmConfiguration::ConfigDisplayPixelWidth,  0);
    cmdBuffer[10] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigDisplayPixelHeight, 0) >> 8;
    cmdBuffer[11] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigDisplayPixelHeight, 0);
    cmdBuffer[12] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigDisplaySizeWidth,   0) >> 8;
    cmdBuffer[13] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigDisplaySizeWidth,   0);
    cmdBuffer[14] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigDisplaySizeHeight,  0) >> 8;
    cmdBuffer[15] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigDisplaySizeHeight,  0);
    cmdBuffer[16] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigDisplayDistance,    0) >> 8;
    cmdBuffer[17] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigDisplayDistance,    0);
    transmitData(18, (char*)cmdBuffer);
}

QSize QTmRfbClient::setServerFbSize(QSize size) {
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "QTmRfbClient::setServerFbSize:" << "Native FB size" << size;
#endif
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbWidth,  size.width());
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigFbHeight, size.height());
    bool scaling = m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbDownScaling, true);
    if (!scaling) {
        int clientWidth  = m_rfbConfigClient->configuration(QTmConfiguration::ConfigDisplayPixelWidth, 0);
        int clientHeight = m_rfbConfigClient->configuration(QTmConfiguration::ConfigDisplayPixelHeight, 0);
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "QTmRfbClient::setServerFbSize:" << "FB Scaling not supported";
        qDebug() << "QTmRfbClient::setServerFbSize:" << "Client FB size" << QSize(clientWidth, clientHeight);
#endif
        if (size.width() > clientWidth)   size.setWidth(clientWidth);
        if (size.height() > clientHeight) size.setHeight(clientHeight);
    }
#ifdef QT_DEBUG_TM_RFBCLIENT
    else {
        qDebug() << "QTmRfbClient::setServerFbSize:" << "FB Scaling supported";
    }
    qDebug() << "QTmRfbClient::setServerFbSize:" << "Request FB size" << size;
#endif
    m_rfbConfigClient->setConfiguration(QTmConfiguration::ConfigRequestedFbWidth,  size.width());
    m_rfbConfigClient->setConfiguration(QTmConfiguration::ConfigRequestedFbHeight, size.height());
    return size;
}

QSize QTmRfbClient::setClientFbSize(QSize size, bool scaling) {
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "QTmRfbClient::setClientFbSize:" << "Native FB size" << size << scaling;
#endif
    m_rfbConfigClient->setConfiguration(QTmConfiguration::ConfigFbUpScaling,   scaling);
    m_rfbConfigClient->setConfiguration(QTmConfiguration::ConfigFbDownScaling, scaling);
    m_rfbConfigClient->setConfiguration(QTmConfiguration::ConfigDisplayPixelWidth,  size.width());
    m_rfbConfigClient->setConfiguration(QTmConfiguration::ConfigDisplayPixelHeight, size.height());

    if (!scaling) {
        int serverWidth  = m_rfbConfigServer->configuration(QTmConfiguration::ConfigFbWidth, 0);
        int serverHeight = m_rfbConfigServer->configuration(QTmConfiguration::ConfigFbHeight, 0);
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "QTmRfbClient::setClientFbSize:" << "FB Scaling not supported";
        qDebug() << "QTmRfbClient::setClientFbSize:" << "Server FB size" << QSize(serverWidth, serverHeight);
#endif
        if (size.width() > serverWidth)   size.setWidth(serverWidth);
        if (size.height() > serverHeight) size.setHeight(serverHeight);
    }
#ifdef QT_DEBUG_TM_RFBCLIENT
    else {
        qDebug() << "QTmRfbClient::setClientFbSize:" << "FB Scaling supported";
    }
    qDebug() << "QTmRfbClient::setClientFbSize:" << "Request FB size" << size;
#endif
    m_rfbConfigClient->setConfiguration(QTmConfiguration::ConfigRequestedFbWidth,  size.width());
    m_rfbConfigClient->setConfiguration(QTmConfiguration::ConfigRequestedFbHeight, size.height());
    return size;
}


// ********************************************************************
// *****               Event Configuration message                *****
// ********************************************************************
/*!
  \fn QTmRfbClient::eventConfigurationReceived()
  Signal emitted from a RFB client to inform about an server event configuration message received.
  The receiving component should use this information to adapt its own eventing mechanisms, to support as
  many server events as possible.
 */
/*!
  Receive an RFB Server Event Configuration message. This message is a Terminal Mode VNC extension message.
  The server event configuration message provides information about the event mechanisms at the VNC server.
  This includes the following elements:
  \list
    \o Keyboard Language and Country
    \o User Interface Language and Country
    \o Supported Device, Multimedia and Function keys
    \o Supported key and pointer event mechanisms
  \endlist
  The received information is stored in the server configuration isnstace. After emitting the
  eventConfigurationReceived() signal, the client sends its own Client Event Configuration message automatically.
  In case framebuffer updates are still disabled, the client will send a framebuffer update request message.
  Returns the number of bytes read.
 */
int QTmRfbClient::serverEventConfig() {
    unsigned char cmdBuffer[28];
    int read = receiveData(28, (char*)cmdBuffer);
    if (read < 28) {
        qWarning() << "QRfbClientTm:" << "Error receiving Server Event Configuration";
        return read;
    }
    //Read the Server Language settings
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigKeyboardLanguage,   (cmdBuffer[0] << 8) | cmdBuffer[1]);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigKeyboardCountry,    (cmdBuffer[2] << 8) | cmdBuffer[3]);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigUiLanguage,         (cmdBuffer[4] << 8) | cmdBuffer[5]);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigUiCountry,          (cmdBuffer[6] << 8) | cmdBuffer[7]);
    //Read the Server Event settings
    int eventKnob    = (cmdBuffer[8]  << 24) | (cmdBuffer[9]  << 16) | (cmdBuffer[10] << 8) | cmdBuffer[11];
    int eventDevice  = (cmdBuffer[12] << 24) | (cmdBuffer[13] << 16) | (cmdBuffer[14] << 8) | cmdBuffer[15];
    int eventMedia   = (cmdBuffer[16] << 24) | (cmdBuffer[17] << 16) | (cmdBuffer[18] << 8) | cmdBuffer[19];
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigEventKnobKey,      eventKnob);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigEventDeviceKey,    eventDevice);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigEventMediaKey,     eventMedia);
    //Key event related
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigEventItuKeypad,   (cmdBuffer[23] >> 0) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigVirtualKeyboard,  (cmdBuffer[23] >> 1) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigKeyEventListing,  (cmdBuffer[23] >> 2) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigKeyEventMapping,  (cmdBuffer[23] >> 3) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigEventFunctionKeys, cmdBuffer[22]);
    //Pointer event related
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigEventPointer,     (cmdBuffer[27] >> 0) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigEventTouch,       (cmdBuffer[27] >> 1) & 0x01);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigButtonMask,        cmdBuffer[26]);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigTouchNumber,       cmdBuffer[25]);
    m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigPressureMask,      cmdBuffer[24]);
    //Emit signal on message reception
    emit eventConfigurationReceived();
    //Send response message
    clientEventConfig();
    //Send framebuffer updates, if not already enabled
    if (m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbUpdates, false)) {
        triggerFramebufferUpdates();
    }
    return 28;
}
/*!
  Send an RFB Client Event Configuration message. This message is a Terminal Mode VNC extension message.
  The client event configuration message provides information about the event mechanisms at the VNC server.
  This includes the following elements:
  \list
    \o Keyboard Language and Country
    \o User Interface Language and Country
    \o Supported Device, Multimedia and Function keys
    \o Supported key and pointer event mechanisms
  \endlist
  The transmitted information is taken from the client configuration isnstace.
 */
void QTmRfbClient::clientEventConfig() {
    unsigned char cmdBuffer[32];
    cmdBuffer[0]  = EXT_MSG_TERMINAL_MODE;
    cmdBuffer[1]  = TM_MSG_CLIENT_EVENT_CONFIG;
    cmdBuffer[2]  = 0x00;
    cmdBuffer[3]  = 0x1C;
    //Language Setting
    cmdBuffer[4]  = m_rfbConfigClient->configuration(QTmConfiguration::ConfigKeyboardLanguage, 0) >> 8;
    cmdBuffer[5]  = m_rfbConfigClient->configuration(QTmConfiguration::ConfigKeyboardLanguage, 0);
    cmdBuffer[6]  = m_rfbConfigClient->configuration(QTmConfiguration::ConfigKeyboardCountry,  0) >> 8;
    cmdBuffer[7]  = m_rfbConfigClient->configuration(QTmConfiguration::ConfigKeyboardCountry,  0);
    cmdBuffer[8]  = m_rfbConfigClient->configuration(QTmConfiguration::ConfigUiLanguage,       0) >> 8;
    cmdBuffer[9]  = m_rfbConfigClient->configuration(QTmConfiguration::ConfigUiLanguage,       0);
    cmdBuffer[10] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigUiCountry,        0) >> 8;
    cmdBuffer[11] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigUiCountry,        0);
    // Multifunction knob configuration
    cmdBuffer[12] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventKnobKey, 0) >> 24;
    cmdBuffer[13] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventKnobKey, 0) >> 16;
    cmdBuffer[14] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventKnobKey, 0) >>  8;
    cmdBuffer[15] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventKnobKey, 0);
    // Mobile device key configuration
    cmdBuffer[16] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventDeviceKey, 0) >> 24;
    cmdBuffer[17] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventDeviceKey, 0) >> 16;
    cmdBuffer[18] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventDeviceKey, 0) >>  8;
    cmdBuffer[19] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventDeviceKey, 0);
    // Multimedia key configuration
    cmdBuffer[20] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventMediaKey, 0) >> 24;
    cmdBuffer[21] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventMediaKey, 0) >> 16;
    cmdBuffer[22] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventMediaKey, 0) >>  8;
    cmdBuffer[23] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventMediaKey, 0);
    // Key event configuration
    cmdBuffer[24] = 0x00;
    cmdBuffer[25] = 0x00;
    cmdBuffer[26] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventFunctionKeys,    0);
    cmdBuffer[27] = ((m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventItuKeypad,     0) & 0x01) << 0) |
                    ((m_rfbConfigClient->configuration(QTmConfiguration::ConfigVirtualKeyboard, true) & 0x01) << 1) |
                    ((m_rfbConfigClient->configuration(QTmConfiguration::ConfigKeyEventListing, true) & 0x01) << 2) |
                    ((m_rfbConfigClient->configuration(QTmConfiguration::ConfigKeyEventMapping, true) & 0x01) << 3);
    // Pointer event configuration
    cmdBuffer[28] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigPressureMask, 0);
    cmdBuffer[29] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigTouchNumber,  0);
    cmdBuffer[30] = m_rfbConfigClient->configuration(QTmConfiguration::ConfigButtonMask,   0);
    cmdBuffer[31] = ((m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventPointer, 0) & 0x01) << 0) |
                    ((m_rfbConfigClient->configuration(QTmConfiguration::ConfigEventTouch,   0) & 0x01) << 1);
    transmitData(32, (char*)cmdBuffer);
    m_keyEventCounter = 0;
}


// ********************************************************************
// *****                  Event Mapping message                   *****
// ********************************************************************
/*!
  \fn QTmRfbClient::eventMappingReceived(unsigned int clientKey, unsigned int serverKey)
  Signal emitted from a RFB client to inform about an event mapping message received.
  \a serverKey defines the key symbol value at the server, to which the client symboal value \a clientKey
  is mapped.
 */
/*!
  Receive an RFB Event Mapping message. This message is a Terminal Mode VNC extension message.
  Read the new key mapping and emit a eventMappingReceived() signal.
  Returns the number of bytes read.
 */
int QTmRfbClient::eventMapping() {
    unsigned char cmdBuffer[8];
    int read = receiveData(8, (char *) cmdBuffer);
    if (read < 8) {
        qWarning() << "QRfbClientTm:" << "Error receiving Event Mapping message";
        return read;
    }
    unsigned int clientKey = (cmdBuffer[0]<<24) | (cmdBuffer[1]<<16) | (cmdBuffer[2]<<8) | cmdBuffer[3];
    unsigned int serverKey = (cmdBuffer[4]<<24) | (cmdBuffer[5]<<16) | (cmdBuffer[6]<<8) | cmdBuffer[7];
    m_eventMap->insert(clientKey, serverKey);
    emit eventMappingReceived(clientKey, serverKey);
    return 8;
}
/*!
  Send an RFB Event Mapping Request message. This message is a Terminal Mode VNC extension message.
  \a serverKey defines the key symbol value at the server, to which the client symboal value \a clientKey
  should be mapped.
 */
void QTmRfbClient::eventMappingRequest(unsigned int clientKey, unsigned int serverKey) {
    unsigned char cmdBuffer[12];
    cmdBuffer[0]  = EXT_MSG_TERMINAL_MODE;
    cmdBuffer[1]  = TM_MSG_EVENT_MAPPING_REQ;
    cmdBuffer[2]  = 0x00;
    cmdBuffer[3]  = 0x08;
    cmdBuffer[4]  = clientKey >> 24;
    cmdBuffer[5]  = clientKey >> 16;
    cmdBuffer[6]  = clientKey >>  8;
    cmdBuffer[7]  = clientKey;
    cmdBuffer[8]  = serverKey >> 24;
    cmdBuffer[9]  = serverKey >> 16;
    cmdBuffer[10] = serverKey >>  8;
    cmdBuffer[11] = serverKey;
    transmitData(12, (char*)cmdBuffer);
}


// ********************************************************************
// *****                Key Event Listing message                 *****
// ********************************************************************
/*!
  \fn QTmRfbClient::keyEventListReceived(QList<unsigned int> *list, unsigned int counter)
  Signal emitted from a RFB client to inform about a key event listing message received.
  This message is meant to inform the client, that a new key event list is available.
  The client can use the new list to change the available virtual key (if applicable).
  \a list provides a pointer to a QList<unsigned int>, containing the supported key symbol values.
  \a counter provides the current key event counter. This can be used to check, whether the list
  is relevant for the current input, or not.
 */
/*!
  Receive an RFB Key Event Listing message. This message is a Terminal Mode VNC extension message.
  Read the supported key events and store them in a QList.
  After receiving all events, a keyEventListReceived() signal is emitted.
  Returns the number of bytes read.
 */
int QTmRfbClient::keyEventListing() {
    unsigned char cmdBuffer[4];
    int read = receiveData(4, (char*)cmdBuffer);
    if (read < 4) {
        qWarning() << "QRfbClientTm:" << "Error receiving Key Event Listing message";
        return read;
    }
    //Get the key event listing configuration. Determine the number of key events in the list.
    unsigned char      key_event_config  =  cmdBuffer[0];
    unsigned short int key_event_number  =  cmdBuffer[1];
    unsigned short int key_event_counter = (cmdBuffer[2] << 8) | cmdBuffer[3];
    Q_UNUSED(key_event_config);
    Q_UNUSED(key_event_counter);
    //Read key event list
    char *buffer = (char *) malloc(key_event_number<<2);
    read = receiveData(key_event_number<<2, buffer);
    if (read < (key_event_number<<2)) {
        qWarning() << "QRfbClientTm:" << "Error receiving Key Event List";
        free(buffer);
        return 4 + read;
    }
    //Create QList of events
    QList<unsigned int> *list = new QList<unsigned int>();
    for (int i=0; i<key_event_number; i++) {
        unsigned int event = (buffer[i<<2] << 24) | (buffer[(i<<2)+1] << 16) | (buffer[(i<<2)+2] << 8) | buffer[(i<<2)+3];
        list->append(event);
    }
    free(buffer);
    emit keyEventListReceived(list, m_keyEventCounter);
    return 4 + read;
}
/*!
  Send an RFB Key Event Listing Request message. This message is a Terminal Mode VNC extension message.
  This message indicates to the VNC server, whether to enable (\a enable = true) or disable (\a enable = false) Key Event Listing.
 */
void QTmRfbClient::keyEventListingRequest(bool enable) {
    if (!m_rfbConfigServer->configuration(QTmConfiguration::ConfigKeyEventListing, true))
        return;
    unsigned char cmdBuffer[8];
    bzero(cmdBuffer, 8);
    cmdBuffer[0] = EXT_MSG_TERMINAL_MODE;
    cmdBuffer[1] = TM_MSG_KEY_EVENT_LISTING_REQ;
    cmdBuffer[3] = 0x04;
    cmdBuffer[7] = enable;
    transmitData(8, (char*)cmdBuffer);
}


// ********************************************************************
// *****             Virtual Keyboard Trigger message             *****
// ********************************************************************
/*!
  \fn QTmRfbClient::virtualKeyboardTriggerReceived(QPoint point, bool remove)
  Signal emitted from a RFB client to inform about a virtual keyboard trigger message received.
  This message is meant to inform the client, that either a virtual keyboard should be shown on
  the remote display (\a remove = false), or a virtual keyboard should be removed (\a remove = true).
  \a point provides the location of the cursor.
 */
/*!
  Receive an RFB Virtual Keyboard Trigger message. This message is a Terminal Mode VNC extension message.
  If the message contains a valid cursor position, a virtualKeyboardTriggerReceived() signal is emitted.
  Returns the number of bytes read.
 */
int QTmRfbClient::virtualKeyboardTrigger() {
    unsigned char cmdBuffer[16];
    int read = receiveData(16, (char *) cmdBuffer);
    if (read < 16) {
        qWarning() << "QRfbClientTm:" << "Error receiving Virtual Keyboard Trigger message";
        return read;
    }
    //Determine a valid cursor position
    if (cmdBuffer[3] & 0x01) {
        int xCursor = (cmdBuffer[4]  << 8) | cmdBuffer[5];
        int yCursor = (cmdBuffer[6]  << 8) | cmdBuffer[7];
        emit virtualKeyboardTriggerReceived(QPoint(xCursor, yCursor), (cmdBuffer[3] & 0x08) ? true : false);
    }
    // Note: Text input size & location and following key event listing indicator are ignored.
    return 16;
}
/*!
  Send an RFB Virtual Keyboard Trigger Request message. This message is a Terminal Mode VNC extension message.
  The message tells the VNC server that the client will handle virtual keyboard trigger messages (\a enable = true)
  or not (\a enable = false).
 */
void QTmRfbClient::virtualKeyboardTriggerRequest(bool enable) {
    unsigned char cmdBuffer[8];
    bzero(cmdBuffer, 8);
    cmdBuffer[0]  = EXT_MSG_TERMINAL_MODE;
    cmdBuffer[1]  = TM_MSG_KEYBOARD_TRIGGER_REQ;
    cmdBuffer[3]  = 0x04;
    cmdBuffer[7]  = (enable) ? 0x01 : 0x00;
    transmitData(8, (char*)cmdBuffer);
}


// ********************************************************************
// *****                   Device Status message                  *****
// ********************************************************************
/*!
  \fn QTmRfbClient::deviceStatusReceived(unsigned int deviceStatus)
  Signal emitted from a RFB client to inform about a device status message received.
  This message is meant to inform the client, that the server has changed its device status.
  The configuration vector \a deviceStatus contains the new status.
 */
/*!
  Receive an RFB Device Status message. This message is a Terminal Mode VNC extension message.
  Read the device status and set the Server configuration.
  A deviceStatusReceived() signal is emitted.
  Returns the number of bytes read.
 */
int QTmRfbClient::deviceStatus() {
    unsigned char cmdBuffer[4];
    int read = receiveData(4, (char *) cmdBuffer);
    if (read < 4) {
        qWarning() << "QRfbClientTm:" << "Error receiving Device Status message";
        return read;
    }
    if (( cmdBuffer[3]       & 0x03) != 1)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigDeviceKeyLock,     (cmdBuffer[3]       & 0x03));
    if (((cmdBuffer[3] >> 2) & 0x03) != 1)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigDeviceDeviceLock, ((cmdBuffer[3] >> 2) & 0x03));
    if (((cmdBuffer[3] >> 4) & 0x03) != 1)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigDeviceScreenSaver,((cmdBuffer[3] >> 4) & 0x03));
    if (((cmdBuffer[3] >> 6) & 0x03) != 1)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigDeviceNightMode,  ((cmdBuffer[3] >> 6) & 0x03));
    if (( cmdBuffer[2]       & 0x03) != 1)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigDeviceVoiceInput,  (cmdBuffer[2]       & 0x03));
    if (((cmdBuffer[2] >> 2) & 0x03) != 1)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigDeviceMicInput,   ((cmdBuffer[2] >> 2) & 0x03));
    if (( cmdBuffer[1]       & 0x03) != 1)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigDeviceDistraction, (cmdBuffer[1]       & 0x03));
    if (( cmdBuffer[0] & 0x03) != 1 && (cmdBuffer[0] & 0x03) != 2 && (cmdBuffer[0] & 0x03) != 3)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigDeviceRotation,    (cmdBuffer[0]       & 0x07));
    if (((cmdBuffer[0] >> 3) & 0x03) != 1)
        m_rfbConfigServer->setConfiguration(QTmConfiguration::ConfigDeviceOrientation,((cmdBuffer[0] >> 3) & 0x03));
    //emit device status
    unsigned int deviceStatus = (cmdBuffer[0]<<24) | (cmdBuffer[1]<<16) | (cmdBuffer[2]<<8) | cmdBuffer[3];
    emit deviceStatusReceived(deviceStatus);
    return 4;
}
/*!
  Send an RFB Device Status Request message. This message is a Terminal Mode VNC extension message.
  The device status requested is taken from the server configuration class instance.
 */
void QTmRfbClient::deviceStatusRequest() {
    unsigned char cmdBuffer[8];
    bzero(cmdBuffer, 8);
    cmdBuffer[0] = EXT_MSG_TERMINAL_MODE;
    cmdBuffer[1] = TM_MSG_DEVICE_STATUS_REQ;
    cmdBuffer[3] = 0x04;
    cmdBuffer[7] = (m_rfbConfigServer->configuration(QTmConfiguration::ConfigDeviceKeyLock)     << 0) |
                   (m_rfbConfigServer->configuration(QTmConfiguration::ConfigDeviceDeviceLock)  << 2) |
                   (m_rfbConfigServer->configuration(QTmConfiguration::ConfigDeviceScreenSaver) << 4) |
                   (m_rfbConfigServer->configuration(QTmConfiguration::ConfigDeviceNightMode)   << 6);
    cmdBuffer[6] = (m_rfbConfigServer->configuration(QTmConfiguration::ConfigDeviceVoiceInput)  << 0) |
                   (m_rfbConfigServer->configuration(QTmConfiguration::ConfigDeviceMicInput)    << 2);
    cmdBuffer[5] = (m_rfbConfigServer->configuration(QTmConfiguration::ConfigDeviceDistraction) << 0);
    cmdBuffer[4] = (m_rfbConfigServer->configuration(QTmConfiguration::ConfigDeviceRotation)    << 0) |
                   (m_rfbConfigServer->configuration(QTmConfiguration::ConfigDeviceOrientation) << 3);
    transmitData(8, (char*)cmdBuffer);
}


// ********************************************************************
// *****                Content Attestation message               *****
// ********************************************************************
/*!
  Receive an RFB Content Attestation message. This message is a Terminal Mode VNC extension message.
  Returns the number of bytes read.

  TODO: Implementation (currently empty body)
 */
int QTmRfbClient::contentAttestation() {
    return 0;
}
/*!
  Send an RFB Content Attestation Request message. This message is a Terminal Mode VNC extension message.
  The VNC server must attest the next framebuffer update message, according to the provided configuration.
  \a nounce, provides a random nounce the VNC server must use in its response.
  \a attestation, describes the elements of the framebuffer update message to be attested.
  \a signature TBD
  \a key TBD
  \a session_key TBD

  TODO: Implementation (currently empty body)
 */
void QTmRfbClient::contentAttestationRequest(char */*nounce*/, char /*attestation*/, char /*signature*/, char /*key*/, char */*session_key*/) {
    return;
}


// ********************************************************************
// *****               Blocking Notification message              *****
// ********************************************************************
/*!
  Send an RFB Framebuffer Blocing Notification message. This message is a Terminal Mode VNC extension message.
  The message tells the VNC server that the client has blocked the rectangular framebuffer area, as given in
  \a fbArea. This area corresponds to the application  with the application identifier \a appId.
  The reason for blocking is given in \a reason.
 */
void QTmRfbClient::framebufferBlockingNotification(QRect fbArea, unsigned int appId, unsigned int reason) {
    unsigned char cmdBuffer[18];
    cmdBuffer[0]  = EXT_MSG_TERMINAL_MODE;
    cmdBuffer[1]  = TM_MSG_FRAMEBUFFER_BLOCKING;
    cmdBuffer[2]  = 0x00;
    cmdBuffer[3]  = 0x0E;
    cmdBuffer[4]  = fbArea.x() >> 8;
    cmdBuffer[5]  = fbArea.x();
    cmdBuffer[6]  = fbArea.y() >> 8;
    cmdBuffer[7]  = fbArea.y();
    cmdBuffer[8]  = fbArea.width() >> 8;
    cmdBuffer[9]  = fbArea.width();
    cmdBuffer[10] = fbArea.height() >> 8;
    cmdBuffer[11] = fbArea.height();
    cmdBuffer[12] = appId >> 24;
    cmdBuffer[13] = appId >> 16;
    cmdBuffer[14] = appId >>  8;
    cmdBuffer[15] = appId;
    cmdBuffer[16] = reason >> 8;
    cmdBuffer[17] = reason;
    transmitData(18, (char*)cmdBuffer);
}
/*!
  Send an RFB Audio Blocing Notification message. This message is a Terminal Mode VNC extension message.
  The message tells the VNC server that the client has blocked an incoming audio stream.
  The stream belongs to the application with the application identifier \a appId.
  The reason for blocking is given in \a reason.
 */
void QTmRfbClient::audioBlockingNotification(unsigned int appId, unsigned int reason) {
    unsigned char cmdBuffer[10];
    cmdBuffer[0]  = EXT_MSG_TERMINAL_MODE;
    cmdBuffer[1]  = TM_MSG_AUDIO_BLOCKING;
    cmdBuffer[2]  = 0x00;
    cmdBuffer[3]  = 0x06;
    cmdBuffer[4] = appId >> 24;
    cmdBuffer[5] = appId >> 16;
    cmdBuffer[6] = appId >>  8;
    cmdBuffer[7] = appId;
    cmdBuffer[8] = reason >> 8;
    cmdBuffer[9] = reason;
    transmitData(10, (char*)cmdBuffer);
}


// ********************************************************************
// *****           Framebuffer Alternative Text message           *****
//*********************************************************************
/*!
  \fn QTmRfbClient::framebufferAlternativeTextReceived(unsigned int appId, QString text)
  Signal emitted from a RFB client to inform about a new framebuffer alternative text message received.
  The application \a appId uniquely identfies the application providing the alternative text \a text.
 */
/*!
  Receive an RFB Framebuffer Alternative Text message.
  The signal framebufferAlternativeTextReceived() is emitted after the message has been fully received.
  Returns the number of bytes read.
 */
int QTmRfbClient::framebufferAlternativeText() {
    unsigned char cmdBuffer[6];
    int read = receiveData(6, (char *) cmdBuffer);
    if (read < 6) {
        qWarning() << "QRfbClientTm:" << "Error receiving Framebuffer Alternative Text message";
        return read;
    }
    unsigned int appId  = (cmdBuffer[0]<<24) | (cmdBuffer[1]<<16) | (cmdBuffer[2]<<8) | cmdBuffer[3];
    unsigned int length = (cmdBuffer[4]<<16) |  cmdBuffer[5];
    char *text = (char *) malloc(length+1);
    text[length] = '\0';
    read = receiveData(length, text);
    if (read < (int) length) {
        qWarning() << "QRfbClientTm:" << "Error receiving Framebuffer Alternative Text message";
        free(text);
        return read + 6;
    }
    emit framebufferAlternativeTextReceived(appId, QString(text));
    free(text);
    return 6 + length;
}
/*!
  Send an RFB Framebuffer Alternative Text Request message. The message is a Terminal Mode VNC extension message.
  The message requests framebuffer alternative text messages from the VNC server.
  The maximum length of the text in those messages in given in \a max_length.
 */
void QTmRfbClient::framebufferAlternativeTextRequest(unsigned short int max_length) {
    unsigned char cmdBuffer[6];
    cmdBuffer[0]  = EXT_MSG_TERMINAL_MODE;
    cmdBuffer[1]  = TM_MSG_FRAMEBUFFER_ALT_TEXT_REQ;
    cmdBuffer[2]  = 0x00;
    cmdBuffer[3]  = 0x02;
    cmdBuffer[4]  = max_length >> 8;
    cmdBuffer[5]  = max_length;
    transmitData(6, (char*)cmdBuffer);
}


/**
 * Protocol FSM
 */
void QTmRfbClient::protocolFsm() {
    unsigned char cmdBuffer[4];
    int length, read;
    switch (m_rfbSessionStatus) {
    case RFB_SESSION_CONNECTED_ACTIVE:
    case RFB_SESSION_CONNECTED_PASSIVE:
        receiveData(1, (char *)cmdBuffer);
/*        if (m_socket->read((char *)cmdBuffer, 1) != 1) {
            m_rfbSessionStatus = ERROR_RFB_SESSION_DISCONNECTED;
            emit sessionDisconnected();
            break;
        }*/
        switch (cmdBuffer[0]) {
        case MSG_FRAMEBUFFER_UPDATE:
#ifdef QT_DEBUG_TM_RFBCLIENT
            qDebug() << "QTmRfbClient::protocolFsm:" << "MSG_FRAMEBUFFER_UPDATE";
#endif
            framebufferUpdate();
            break;
        case MSG_SET_COLOUR_MAP_ENTRIES:
#ifdef QT_DEBUG_TM_RFBCLIENT
            qDebug() << "QTmRfbClient::protocolFsm:" << "MSG_SET_COLOUR_MAP_ENTRIES";
#endif
            setColourMapEntries();
            break;
        case MSG_BELL:
#ifdef QT_DEBUG_TM_RFBCLIENT
            qDebug() << "QTmRfbClient::protocolFsm:" << "MSG_BELL";
#endif
            bell();
            break;
        case MSG_SERVER_CUT_TEXT:
#ifdef QT_DEBUG_TM_RFBCLIENT
            qDebug() << "QTmRfbClient::protocolFsm:" << "MSG_SERVER_CUT_TEXT";
#endif
            serverCutText();
            break;
        case MSG_TERMINAL_MODE:
            receiveData(3, (char*)cmdBuffer+1);
            length = (cmdBuffer[2]<<8) | cmdBuffer[3];
            read   = 0;
            switch (cmdBuffer[1]) {
            case TM_MSG_SERVER_DISPLAY_CONFIG:
#ifdef QT_DEBUG_TM_RFBCLIENT
                qDebug() << "QTmRfbClient::protocolFsm:" << "TM_MSG_SERVER_DISPLAY_CONFIG";
#endif
                read = serverDisplayConfig();
                break;
            case TM_MSG_SERVER_EVENT_CONFIG:
#ifdef QT_DEBUG_TM_RFBCLIENT
                qDebug() << "QTmRfbClient::protocolFsm:" << "TM_MSG_SERVER_EVENT_CONFIG";
#endif
                read = serverEventConfig();
                break;
            case TM_MSG_EVENT_MAPPING:
#ifdef QT_DEBUG_TM_RFBCLIENT
                qDebug() << "QTmRfbClient::protocolFsm:" << "TM_MSG_EVENT_MAPPING";
#endif
                read = eventMapping();
                break;
            case TM_MSG_KEY_EVENT_LISTING:
#ifdef QT_DEBUG_TM_RFBCLIENT
                qDebug() << "QTmRfbClient::protocolFsm:" << "TM_MSG_KEY_EVENT_LISTING";
#endif
                read = keyEventListing();
                break;
            case TM_MSG_KEYBOARD_TRIGGER:
#ifdef QT_DEBUG_TM_RFBCLIENT
                qDebug() << "QTmRfbClient::protocolFsm:" << "TM_MSG_KEYBOARD_TRIGGER";
#endif
                read = virtualKeyboardTrigger();
                break;
            case TM_MSG_DEVICE_STATUS:
#ifdef QT_DEBUG_TM_RFBCLIENT
                qDebug() << "QTmRfbClient::protocolFsm:" << "TM_MSG_DEVICE_STATUS";
#endif
                read = deviceStatus();
                break;
            case TM_MSG_CONTENT_ATTESTATION_RESP:
#ifdef QT_DEBUG_TM_RFBCLIENT
                qDebug() << "QTmRfbClient::protocolFsm:" << "TM_MSG_CONTENT_ATTESTATION_RESP";
#endif
                read = contentAttestation();
                break;
            case TM_MSG_FRAMEBUFFER_ALT_TEXT:
#ifdef QT_DEBUG_TM_RFBCLIENT
                qDebug() << "QTmRfbClient::protocolFsm:" << "TM_MSG_FRAMEBUFFER_ALT_TEXT";
#endif
                read = framebufferAlternativeText();
                break;
            default:
                read = 0;
                qWarning() << "QRfbClientTm::rfb_protocol_ext_handle_msg: Unknown Type:" << cmdBuffer[1];
                break;
            }
            //Payload remaining from the message will be read and ignored.
            if (read < length) {
                char *dummyBuffer = (char *) malloc(length-read);
                receiveData(length-read, (char*)dummyBuffer);
                delete dummyBuffer;
            }
            break;
        default:
            qWarning() << "QTmRfbClient:" << "Error - Wrong FSM State " << cmdBuffer[0] << ". Exiting.";
            m_rfbSessionStatus = QTmRfbClient::ERROR_WRONG_MSG_TYPE;
            emit sessionDisconnected();
            break;
        }
        break;
    case RFB_SESSION_SERVER_VERSION:
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "QTmRfbClient::protocolFsm:" << "RFB_SESSION_SERVER_VERSION";
#endif
        protocolHandshake();
        break;
    case RFB_SESSION_SECURITY_TYPES:
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "QTmRfbClient::protocolFsm:" << "RFB_SESSION_SECURITY_TYPES";
#endif
        securityHandshake();
        break;
    case RFB_SESSION_SECURITY_RESPONSE:
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "QTmRfbClient::protocolFsm:" << "RFB_SESSION_SECURITY_RESPONSE";
#endif
        securityResponse();
        break;
    case RFB_SESSION_SERVER_INIT:
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "QTmRfbClient::protocolFsm:" << "RFB_SESSION_SERVER_INIT";
#endif
        serverInit();
        break;
    case RFB_SESSION_NONE:
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "QTmRfbClient::protocolFsm:" << "RFB_SESSION_NONE";
#endif
        break;
    default:
        qWarning() << "QTmRfbClient::protocolFsm:" << "Error - Wrong FSM State " << cmdBuffer[0] << ". Exiting.";
        m_rfbSessionStatus = QTmRfbClient::ERROR_WRONG_FSM_STATE;
        sessionStop();
        emit sessionDisconnected();
        break;
    }
    if (m_socket->bytesAvailable() > 0) {
#ifdef QT_DEBUG_TM_RFBCLIENT
        qDebug() << "QTmRfbClient::protocolFsm:" << "More data available" << m_socket->bytesAvailable();
#endif
        protocolFsm();
    }
}


/**
 * <b>Implementation Details:</b>
 *
 * Go through the framebuffer update area, line-by-line.
 * Read each line individually and copy the received bytes into the client framebuffer.
 */
void
QTmRfbClient::rawEncoding(int x, int y, int width, int height) {
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::rawEncoding:" << "Receive RAW Encoding" << QRect(x, y, width, height);
#endif
    QTmGlobal::TmColorFormat colorFormat;
    colorFormat   = (QTmGlobal::TmColorFormat) m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::RGBNone);
    int fb_width  = m_rfbConfigServer->configuration(QTmConfiguration::ConfigFbWidth,  0);
    switch (colorFormat) {
      case QTmGlobal::RGB888: {
        unsigned int *fb_down = (unsigned int *) m_framebuffer + y * fb_width + x;
        for (;height>0; height--) {
            receiveData(width<<2, (char *) fb_down);
            fb_down += fb_width;
        }
      } break;
      case QTmGlobal::RGB565:
      case QTmGlobal::RGB555:
      case QTmGlobal::RGB444: {
        unsigned short int *fb_down = (unsigned short int *) m_framebuffer + y * fb_width + x;
        for (;height>0; height--) {
            receiveData(width<<1, (char *) fb_down);
            fb_down += fb_width;
        }
      } break;
      case QTmGlobal::RGB343: {
        unsigned short int *fb_down = (unsigned short int *) m_framebuffer + y * fb_width + x;
        for (;height>0; height--) {
            receiveData(width<<1, (char *) fb_down);
            for (int i=width; i>0; i--)
            {
                /**
                 * For RGB343 do local color transformation to RGB444, as QImage does not support RGB343
                 * Add 0x0101 to the color value to increase luminance.
                 */
                unsigned short int color = *fb_down;
                *fb_down++ = ((color & 0x0380) << 2) | ((color & 0x007F) << 1) | 0x0101;
            }
            fb_down += (fb_width - width);
        }
      } break;
      default:
        qWarning() << "QRfbClientFbProcessing:" << "Unknown Color Format";
        return;
    }
}


/**
 * <b>Implementation Details:</b>
 *
 * Go through the framebuffer update area, line-by-line.
 * Read each line individually. The first two bytes of each line, give the number of run-length encodings for this line.
 * The number of bytes read for each run-length, depend on the color format.
 * For every run-length encoding, do the following steps:
 * - decode the run-length @c number
 * - decode the color value @c color
 * - copy the color value @c color, @c number times into the client framebuffer.
 */
#ifdef QT_DEBUG_TM_PERFORMANCE_COUNTER
int QTmRfbClient::rleEncoding(int x, int y, int width, int height) {
#else
void QTmRfbClient::rleEncoding(int x, int y, int width, int height) {
#endif
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::rawEncoding:" << "Receive RLE Encoding" << QRect(x, y, width, height);
#endif

#ifdef QT_DEBUG_TM_PERFORMANCE_COUNTER
    int number_of_runs = 0;
#endif
    unsigned int number;
    unsigned int no_runs;
    unsigned int position;
    QTmGlobal::TmColorFormat colorFormat;
    colorFormat   = (QTmGlobal::TmColorFormat) m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbColorFormat, QTmGlobal::RGBNone);
    int fb_width  = m_rfbConfigServer->configuration(QTmConfiguration::ConfigFbWidth,  0);
    switch(colorFormat) {
      case QTmGlobal::RGB888: {
        unsigned int  *fb_down = (unsigned int *) m_framebuffer + y * fb_width + x;
        unsigned int   color;
        unsigned char *cmdBuffer = (unsigned char *) malloc(width << 2);
        for (;height>0; height--) {
            receiveData(2, (char *) cmdBuffer);
            position = 0;
            no_runs  = (cmdBuffer[0] << 8) | cmdBuffer[1];
            receiveData(no_runs << 2, (char *) cmdBuffer);
#ifdef QT_DEBUG_TM_PERFORMANCE_COUNTER
            number_of_runs += no_runs;
#endif
            for (; no_runs>0; no_runs--) {
                color  = cmdBuffer[position++];
                color |= cmdBuffer[position++] <<  8;
                color |= cmdBuffer[position++] << 16;
                number = cmdBuffer[position++] + 1;
                for (;number>0; number--)
                    *fb_down++ = color;
            }
            fb_down += (fb_width - width);
        }
        free(cmdBuffer);
      } break;
      case QTmGlobal::RGB565:
      case QTmGlobal::RGB555: {
        unsigned short int *fb_down = (unsigned short int *) m_framebuffer + y * fb_width + x;
        unsigned short int  color;
        unsigned char      *cmdBuffer = (unsigned char *) malloc(width*3);
        for (;height>0; height--) {
            receiveData(2, (char *) cmdBuffer);
            position = 0;
            no_runs  = (cmdBuffer[0] << 8) | cmdBuffer[1];
            receiveData(no_runs*3, (char *) cmdBuffer);
#ifdef QT_DEBUG_TM_PERFORMANCE_COUNTER
            number_of_runs += no_runs;
#endif
            for (; no_runs>0; no_runs--) {
                switch (colorFormat) {
                  case QTmGlobal::RGB565:
                    color  = cmdBuffer[position++];
                    color |= cmdBuffer[position++] << 8;
                    number = cmdBuffer[position++] + 1;
                    break;
                  case QTmGlobal::RGB555:
                    color   =  cmdBuffer[position++];
                    color  |= (cmdBuffer[position]   & 0x07) <<  8;
                    number  = (cmdBuffer[position++] >> 7);
                    number |= (cmdBuffer[position++] << 1);
                    break;
                  default:
                    number = 0;
                    color  = 0;
                    break;
                }
                for (;number>0; number--)
                    *fb_down++ = color;
            }
            fb_down += (fb_width - width);
        }
        free(cmdBuffer);
      } break;
      case QTmGlobal::RGB444:
      case QTmGlobal::RGB343: {
        unsigned short int *fb_down = (unsigned short int *) m_framebuffer + y * fb_width + x;
        unsigned short int  color;
        unsigned char      *cmdBuffer = (unsigned char *) malloc(width*2);
        for (;height>0; height--) {
            receiveData(2, (char *) cmdBuffer);
            position = 0;
            no_runs  = (cmdBuffer[0] << 8) | cmdBuffer[1];
            receiveData(no_runs*2, (char *) cmdBuffer);
#ifdef QT_DEBUG_TM_PERFORMANCE_COUNTER
            number_of_runs += no_runs;
#endif
            for (; no_runs>0; no_runs--) {
                switch (colorFormat) {
                  case QTmGlobal::RGB444:
                    color  =   cmdBuffer[position++];
                    color |=  (cmdBuffer[position]   & 0x0F) << 8;
                    number = ((cmdBuffer[position++] & 0xF0) >> 4) + 1;
                    break;
                  case QTmGlobal::RGB343:
                    color  =   cmdBuffer[position++];
                    color |=  (cmdBuffer[position]   & 0x03) << 8;
                    number = ((cmdBuffer[position++] & 0xFC) >> 2) + 1;
                    /**
                     * For RGB343 do local color transformation to RGB444, as QImage does not support RGB343
                     * Add 0x0101 to the color value to increase luminance.
                     */
                    color  = ((color & 0x0380) << 2) | ((color & 0x007F) << 1) | 0x0101;
                    break;
                  default:
                    number = 0;
                    color  = 0;
                    break;
                }
                for (;number>0; number--)
                    *fb_down++ = color;
            }
            fb_down += (fb_width - width);
        }
        free(cmdBuffer);
      } break;
      default:
        break;
    }
#ifdef QT_DEBUG_TM_PERFORMANCE_COUNTER
    return number_of_runs;
#endif
}


// ********************************************************************
// *****           Desktop Size Pseudo Encoding message           *****
//*********************************************************************
/*!
  \fn QTmRfbClient::framebufferResized(QSize size)
  Signal emitted from a RFB client to inform about a Desktop Size Pseudo Encoding message received.
  The new framebuffer size is given in \a size.
 */
/*!
  Receive a RFB Desktop Size Pseudo Encoding message.
  A new framebuffer surface must be allocated, with the new framebuffer resolution.
  In case the new allocation fails, a sessionDisconnected() signal is emitted.
  Otherwise a framebufferResized() message is emitted.
 */
void QTmRfbClient::desktopSizePseudoEncoding(QSize framebufferSize) {
#ifdef QT_DEBUG_TM_RFBCLIENT
    qDebug() << "QTmRfbClient::desktopSizePseudoEncoding:" << "Desktop Size signal received" << framebufferSize;
#endif
    if (!m_surface){
        qWarning() << "QTmRfbClient::desktopSizePseudoEncoding:" << "Surface not set before!";
        return;
    }
    framebufferSize = setServerFbSize(framebufferSize);
    QTmGlobal::TmColorFormat colorFormat = (QTmGlobal::TmColorFormat) m_rfbConfigClient->configuration(QTmConfiguration::ConfigFbColorFormat);
    m_framebuffer = m_surface->allocateSurface(framebufferSize, colorFormat);
    if(!m_framebuffer) {
        qWarning() << "QTmRfbClient::desktopSizePseudoEncoding:" << "Allocate surface failed!";
        m_rfbSessionStatus = QTmRfbClient::ERROR_CANNOT_ALLOCATE_SURFACE;
        emit sessionDisconnected();
        return;
    }
    emit framebufferResized(framebufferSize);
}


// ********************************************************************
// *****       Context Information Pseudo Encoding message        *****
//*********************************************************************
/*!
  \fn QTmRfbClient::contextInformationReceived(QRect area, int appId, QTmAppInfo *appCategory, QTmDisplayInfo *conCategory)
  Signal emitted from a RFB client to inform about a Context Information Pseudo Encoding message received.
  \a area gives the rectangular area, for which the context information is given.
  \a appId gives the application id, of the application, providing the content in question.
  \a appCategory gives the application information.
  \a conCategory gives the content information.
 */
/*!
  Receive a RFB Context Information Pseudo Encoding message.
  After reception, the signal contextInformationReceived() is emitted.
 */
void QTmRfbClient::contextInformationPseudoEncoding(unsigned char *buffer) {
    QRect area   = QRect((buffer[0]  << 8) | buffer[1], (buffer[2]  << 8) | buffer[3],
                         (buffer[4]  << 8) | buffer[5], (buffer[6]  << 8) | buffer[7]);
    unsigned char cmdBuffer[20];
    receiveData(20, (char*)cmdBuffer);

    int appId    = (cmdBuffer[0] << 24) | (cmdBuffer[1] << 16) | (cmdBuffer[2] << 8) | cmdBuffer[3];

    QTmAppInfo     *appCategory = new QTmAppInfo();
    QTmDisplayInfo *conCategory = new QTmDisplayInfo();
    appCategory->setCategory ((int) (cmdBuffer[8] << 24) | (cmdBuffer[9] << 16) | (cmdBuffer[10] << 8) | cmdBuffer[11]);
    conCategory->setCategory ((cmdBuffer[12] << 24) | (cmdBuffer[13] << 16) | (cmdBuffer[14] << 8) | cmdBuffer[15]);
    conCategory->setRules    ((cmdBuffer[16] << 24) | (cmdBuffer[17] << 16) | (cmdBuffer[18] << 8) | cmdBuffer[19]);

    switch ((cmdBuffer[4] <<  8) |  cmdBuffer[5]) {
    case (int) QTmGlobal::TrustUser:           appCategory->setTrust(QTmGlobal::TrustUser);           break;
    case (int) QTmGlobal::TrustSelfRegistered: appCategory->setTrust(QTmGlobal::TrustSelfRegistered); break;
    case (int) QTmGlobal::TrustRegistered:     appCategory->setTrust(QTmGlobal::TrustRegistered);     break;
    case (int) QTmGlobal::TrustCertified:      appCategory->setTrust(QTmGlobal::TrustCertified);      break;
    default: appCategory->setTrust(QTmGlobal::TrustUnknown);
    }
    switch ((cmdBuffer[6] <<  8) |  cmdBuffer[7]) {
    case (int) QTmGlobal::TrustUser:           conCategory->setTrust(QTmGlobal::TrustUser);           break;
    case (int) QTmGlobal::TrustSelfRegistered: conCategory->setTrust(QTmGlobal::TrustSelfRegistered); break;
    case (int) QTmGlobal::TrustRegistered:     conCategory->setTrust(QTmGlobal::TrustRegistered);     break;
    case (int) QTmGlobal::TrustCertified:      conCategory->setTrust(QTmGlobal::TrustCertified);      break;
    default: appCategory->setTrust(QTmGlobal::TrustUnknown);
    }
    emit contextInformationReceived(area, appId, appCategory, conCategory);
}


// ********************************************************************
// *****             Send and Receive Data from Socket            *****
// ********************************************************************
/*!
  Wait until the number of bytes to be read bytes are available from the TCP socket.
  If bytes are not available (either completely or in part) return 0.
  Timeout counter is set to 5 seconds.
  If bytes are available read \a length bytes and store them in \a buffer
  Returns the number of read bytes.
 */
int QTmRfbClient::receiveData(int length, char *buffer) {
    while (m_socket->bytesAvailable() < length) {
        if (m_socket && !m_socket->waitForReadyRead(5 * 1000)) {
            qWarning() << "error" << m_socket->error() << m_socket->errorString();
            emit sessionDisconnected();
            return 0;
        }
    }
    int read = m_socket->read((char *)buffer, length);
    return read;
}
/*!
  Write \a length number of bytes from the \a buffer to the TCP socket.
  Return the number of successful written bytes.
 */
int QTmRfbClient::transmitData(int length, char *buffer) {
    if (!m_socket)
        return 0;
    QMutexLocker locker(&m_mutex);
    int written = m_socket->write(buffer, length);
    if (written != length)
        emit sessionDisconnected();
    return written;
}

