/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#ifndef QTMREMOTINGINFO_H
#define QTMREMOTINGINFO_H

#include <QObject>
#include "QTmGlobal.h"

class QTmRemotingInfo : public QObject {
    Q_OBJECT
    Q_PROPERTY(QTmGlobal::TmRemoteProtocol protocolId READ protocolId)
    Q_PROPERTY(QString                     format     READ format)
    Q_PROPERTY(QTmGlobal::TmDirection      direction  READ direction)

public:
    QTmGlobal::TmRemoteProtocol  protocolId();
    QString                      format();
    QTmGlobal::TmDirection       direction();

private:
    QTmRemotingInfo();
    void                         setProtocolId(QTmGlobal::TmRemoteProtocol protocolId);
    void                         setFormat(QString format);
    void                         setDirection(QTmGlobal::TmDirection direction);

private:
    QTmGlobal::TmRemoteProtocol  m_protocolId;
    QString                      m_format;
    QTmGlobal::TmDirection       m_direction;
    friend class                 QTmRemoteServer;
};

#endif // QTMREMOTINGINFO_H
