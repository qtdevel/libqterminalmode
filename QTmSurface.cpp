/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QDebug"
#include "QTmSurface.h"

int QTmSurface::translateKey(int key, Qt::KeyboardModifiers modifier)
{
    qDebug() << "QTmSurface:" << key << modifier;
    if( !(modifier & Qt::ShiftModifier) && (key >= Qt::Key_A) && (key <= Qt::Key_Z) )
        return (key + 0x20); //return small letter a..z


    if (key == Qt::Key_Return || key == Qt::Key_Enter)
        return 0xFF0D; //XK_Enter

    if (key == Qt::Key_Backspace)
        return 0xFF08; //XK_Backspace

    if (key == Qt::Key_Delete)
        return 0xFF0B; //XK_Delete

    if (key == Qt::Key_Home)
        return QTmGlobal::XK_Device_Home;

    if (key == Qt::Key_Up)
        return 0xFF52; //XK_Up

    if (key == Qt::Key_Down)
        return 0xFF54; //XK_Down

    if (key == Qt::Key_Left)
        return 0xFF51; //XK_Left

    if (key == Qt::Key_Right)
        return 0xFF53; //XK_Right

    return key;
}

QTransform QTmSurface::getTransformation(QSize viewSize, QSize surfaceSize) {
    QTransform transform;
    qreal dx, dy;
    qreal sx = (qreal) viewSize.width() / surfaceSize.width();
    qreal sy = (qreal) viewSize.height() / surfaceSize.height();
    if(sx < sy) {
        sy = sx;
    } else {
        sx = sy;
    }
    dx = (viewSize.width() - surfaceSize.width() * sx) / 2;
    dy = (viewSize.height() - surfaceSize.height() * sy) / 2;
    transform.translate(dx, dy);
    transform.scale(sx, sy);
    return transform;
}
