/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QTmClientProfile.h"

/*!
  \class QTmClientProfile

  \brief The QTmClientProfile class provides access to the encapsulated client profile data, utilized by the TmClientProfile:1 UPnP service.

  The Client Profile data contains preferences which can be set by the Terminal Mode Client such as client's identifying information, icon preferences,
  Bluetooth settings and driver distraction rules.

  This class represents the A_ARG_TYPE_ClientProfile variable of TmClientProfile:1 UPnP service.
*/

/*!
  Class constructor. Arguments \a clientId (Terminal Mode Client Identifier) and \a parent.
 */
QTmClientProfile::QTmClientProfile(QString clientId, QObject *parent) :
        m_clientId(clientId),
        m_startBTConnection(false) {
    Q_UNUSED(parent)
}

/*!
  Class destructor.
 */
QTmClientProfile::~QTmClientProfile()
{

}

/*!
  Get the ClientId of Terminal Mode client.
  The function will return a QString.
 */
QString QTmClientProfile::clientId()
{
    return m_clientId;
}

/*!
  Set the Client Name of Terminal Mode client
  to \a clientName.
 */
void QTmClientProfile::setClientName(QString clientName)
{
    m_clientName = clientName;
}

/*!
  Get the Client Name of Terminal Mode client.
  The function will return a QString.
 */
QString QTmClientProfile::clientName()
{
    return m_clientName;
}

/*!
  Set the Manufacturer Name of Terminal Mode client
  to \a manufacturerName.
 */
void QTmClientProfile::setManufacturerName(QString manufacturerName)
{
    m_manufacturerName = manufacturerName;
}

/*!
  Get the Manufacturer Name of Terminal Mode client.
  The function will return a QString.
 */
QString QTmClientProfile::manufacturerName()
{
    return m_manufacturerName;
}

/*!
  Set the Model Name of Terminal Mode client
  to \a modelName.
 */
void QTmClientProfile::setModelName(QString modelName)
{
    m_modelName = modelName;
}

/*!
  Get the Model Name of Terminal Mode client.
  The function will return a QString.
 */
QString QTmClientProfile::modelName()
{
    return m_modelName;
}

/*!
  Set the Model Number of Terminal Mode client
  to \a modelNumber.
 */
void QTmClientProfile::setModelNumber(QString modelNumber)
{
    m_modelNumber = modelNumber;
}

/*!
  Get the Model Number of Terminal Mode client.
  The function will return a QString.
 */
QString QTmClientProfile::modelNumber()
{
    return m_modelNumber;
}

/*!
  Set the preferred Icon Mimetype of Terminal Mode client
  to \a mimeType.
 */
void QTmClientProfile::setIconMimeType(QString mimeType)
{
    m_iconMimeType = mimeType;
}

/*!
  Get the preferred Icon Mimetype of Terminal Mode client.
  The function will return a QString.
 */
QString QTmClientProfile::iconMimeType()
{
    return m_iconMimeType;
}

/*!
  Set the preferred Icon Width of Terminal Mode client
  to \a iconWidth.
 */
void QTmClientProfile::setIconWidth(int iconWidth)
{
    m_iconWidth = iconWidth;
}

/*!
  Get the preferred Icon Width of Terminal Mode client.
  The function will return an int.
 */
int QTmClientProfile::iconWidth()
{
    return m_iconWidth;
}

/*!
  Set the preferred Icon Height of Terminal Mode client
  to \a iconHeight.
 */
void QTmClientProfile::setIconHeight(int iconHeight)
{
    m_iconHeight = iconHeight;
}

/*!
  Get the preferred Icon Height of Terminal Mode client.
  The function will return an int.
 */
int QTmClientProfile::iconHeight()
{
    return m_iconHeight;
}

/*!
  Set the preferred Icon Depth of Terminal Mode client
  to \a iconDepth.
 */
void QTmClientProfile::setIconDepth(int iconDepth)
{
    m_iconDepth = iconDepth;
}

/*!
  Get the preferred Icon Depth of Terminal Mode client.
  The function will return an int.
 */
int QTmClientProfile::iconDepth()
{
    return m_iconDepth;
}

/*!
  Set the Bluetooth Device Address of Terminal Mode client
  to \a bdAddr.
 */
void QTmClientProfile::setBdAddr(QString bdAddr)
{
    m_bdAddr = bdAddr;
}

/*!
  Get the Bluetooth Device Address of Terminal Mode client.
  The function will return a QString.
 */
QString QTmClientProfile::bdAddr()
{
    return m_bdAddr;
}

/*!
  Set whether Terminal Mode client will initiate Bluetooth connections
  to \a startBTConnection (true or false).
 */
void QTmClientProfile::setStartBTConnection(bool startBTConnection)
{
    m_startBTConnection = startBTConnection;
}

/*!
  Get whether Terminal Mode client will initiate Bluetooth connections.
  The function will return a boolean value.
 */
bool QTmClientProfile::startBTConnection()
{
    return m_startBTConnection;
}

/*!
  Set the Driver Distraction Rules specified by the Terminal Mode client
  to those specified in \a clientRules.
 */
void QTmClientProfile::setClientRules(QTmClientRules clientRules)
{
    m_clientRules = clientRules;
}

/*!
  Get the Driver Distraction Rules specified by the Terminal Mode client.
  The function will return a QTmClientRules object.
 */
QTmClientRules QTmClientProfile::clientRules()
{
    return m_clientRules;
}

/*!
  Check whether this TmClientProfile object is empty or not.
  The function will return a boolean.
 */
bool QTmClientProfile::isNull()
{
    if(!clientId().compare("null", Qt::CaseInsensitive))
        return true;
    else
        return false;
}
