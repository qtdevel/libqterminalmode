/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#ifndef QTMICON_H
#define QTMICON_H

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QUrl>

class QTmIcon : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString mimeType READ mimeType)
    Q_PROPERTY(int     width    READ width)
    Q_PROPERTY(int     height   READ height)
    Q_PROPERTY(int     depth    READ depth)
    Q_PROPERTY(QUrl    url      READ url)

public:
    QString  mimeType();
    int      width();
    int      height();
    int      depth();
    QUrl     url();

private:
    QTmIcon();
    void     setMimeType(QString mimeType);
    void     setWidth(int width);
    void     setHeight(int height);
    void     setDepth(int depth);
    void     setUrl(QUrl url);

private:
    QString           m_mimeType;
    int               m_width;
    int               m_height;
    int               m_depth;
    QUrl              m_url;
    friend class      QTmUpnpControlPoint;
    friend class      QTmRemoteServer;
};

#endif // QTMICON_H
