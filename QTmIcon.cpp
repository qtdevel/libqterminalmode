/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QTmIcon.h"

/*!
  \class QTmIcon
  \brief Holds information representing an icon.

  This class gives access to information describing an icon, which is accessible from a given URL.
  The icon is described via the following parameters:
  \list
  \o Icon format (Mimetype) e.g. image/png
  \o Width and height of the icon
  \o Color depth
  \o URL to the icon
  \endlist

  The class does not have a public constructor, but is rather instantiated from the QTmRemoteServer or
  the QTmRfbServer class instance.
 */
/*!
  \property QTmIcon::mimeType
  \brief Holds the icon's mimetype.
  The icon's mimetype describes how the icon is encoded. Typical icon formats are image/png or image/bmp.
  For Terminal Mode use, the icon should have transparent color, as supported in image/png.
 */
/*!
  \property QTmIcon::width
  \brief Holds the icon's width in pixel.
  The icon's width is given in number of pixels.
 */
/*!
  \property QTmIcon::height
  \brief Holds the icon's height in pixel.
  The icon's height is given in number of pixels.
 */
/*!
  \property QTmIcon::depth
  \brief Holds the icon's color depth.
  The icon's depth is given in bits per pixel. The typical color depth is either 16 (RGB 565) or 24 (RGB 888).
 */
/*!
  \property QTmIcon::url
  \brief Holds the icon's URL.
  The icon's URL points to the icon's image data, and can be received from there, using e.g. regular HTTP Get request.
 */

/*!
  Class Constructor.
 */
QTmIcon::QTmIcon() :
        m_width(0),
        m_height(0),
        m_depth(0) {
    m_mimeType = QString();
    m_url      = QUrl();
}

QString QTmIcon::mimeType() {
    return m_mimeType;
}
void QTmIcon::setMimeType(QString mimeType) {
    m_mimeType = mimeType;
}

int QTmIcon::width() {
    return m_width;
}
void QTmIcon::setWidth(int width) {
    m_width = width;
}

int QTmIcon::height() {
    return m_height;
}
void QTmIcon::setHeight(int height) {
    m_height = height;
}

int QTmIcon::depth() {
    return m_depth;
}
void QTmIcon::setDepth(int depth) {
    m_depth = depth;
}

QUrl QTmIcon::url() {
    return m_url;
}
void QTmIcon::setUrl(QUrl url) {
    m_url = url;
}
