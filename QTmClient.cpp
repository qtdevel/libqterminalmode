/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QTmGlobal.h"
#include "QTmClient.h"

#include "QTmUpnpControlPoint.h"
#include "QTmRemoteServer.h"
#include "QTmPointerEvent.h"

/*!
  \class QTmClient

  \brief The QTmClient class provides a Terminal Mode client, compliant to the 1.0 version of the Terminal Mode specification.

  \section1 General Architecture
  The Terminal Mode client class QTmClient covers the following functions
  \list
    \o A Network observer, checking for the availability of IP based interfaces and whether they will be disconnected.
    \o UPnP Control Point(s), listening on IP interfaces for Terminal Mode enabled server devices and managing remote applications.
    \o VNC Client(s), remoting the Terminal Mode server's user interface to the Terminal Mode client.
  \endlist
  The QTmClient allows for multiple Control Points, one for each available IP interface.
  In addition, the QTmClient support multiple VNC clients, either running over different IP connections,
  distinguished by either the IP address, the port number or both.

  The QTmClient uses unique UPnP Control Point and VNC Client identifier, to distinguish between those. The identifier
  are provided with the instantiation of those, as described below.

  The QTmClient class is instantiated, using the following example code:
  \code
    #include <QTmClient.h>
    #include <QTmGlobal.h>

    QTmClient *m_TerminalMode;

    void foo() {
        m_TerminalMode = new QTmClient(this);
    }
  \endcode

  \section1 Network Observer
  The network observer allows the QTmClient to detect any change in the IPv4 interfaces. The network observer provides
  the following functions:
  \list
    \o void networkStartObserver(), starting the network observer.
    \o void networkStopObserver(), stopping the network observer.
  \endlist
  The network observer must be started, before any interface configuration change can be detected.

  It will emit the following signals, on any change:
  \list
    \o networkDeviceDetected(QString interfaceName, QHostAddress hostAddress), providing the \a interfaceName and \a hostAddress of the new IPv4 interface
    \o networkDeviceLost(QHostAddress hostAddress), providing the \a hostAddress of the lost IPv4 interface
  \endlist
  The network observer is therefore instantiated, using the following example code:
  \code
    void foo() {
        connect(m_TerminalMode, SIGNAL(networkDeviceDetected(QString, QHostAddress)),
                this,           SLOT  (deviceDetected(QString, QHostAddress)));
        connect(m_TerminalMode, SIGNAL(networkDeviceLost(QHostAddress)),
                this,           SLOT  (deviceLost(QHostAddress)));
        m_TerminalMode->networkStartObserver();
    }

    void deviceDetected(QHostAddress hostAddress) {
        [...]
    }

    void deviceLost(QHostAddress hostAddress) {
        [...]
    }
  \endcode

  \section1 UPnP Control Point
  The UPnP control points allows the QTmClient to detect, launch and terminate any application, which got advertised
  from the Terminal Mode server, connected to the given interface. In addition, the control point will allow for
  accessing the server's device description.

  A control point does provide the following functions:
  \list
    \o int upnpStartControlPoint(), starting a control point and return a unique identifier
    \o void upnpConnectControlPoint(QString interfaceName, QHostAddress interface, int id), connecting the control point, identified with \a id,
       to the host address \a interface with name \a interfaceName.
    \o void upnpDisconnectControlPoint(int id), disconnecting the control point, identified with \a id.
    \o void upnpStopControlPoint(int id), stopping the control point, identified with \a id.
  \endlist
  A disconnected control point, can be connected again at any time. A stopped control point must be started first,
  before it can be connected again.

  A control point will emit the following signals:
  \list
    \o upnpNewApplication(QTmApplication *application), advertising a new application, as described
       in \a application.
    \o upnpApplicationStatusChanged(QString appID, QTmGlobal::TmStatus status), signalling a change in the application status.
    \o upnpApplicationStarted(QTmApplication *application), signalling that the application
       \a application. The IP address and port number for the VNC connection are available via QTmApplication methods.
  \endlist
  The UPnP control point is started and controled, using the following example code:
  \code
    int m_upnpId;

    void foo() {
        connect(m_TerminalMode, SIGNAL(upnpApplicationAdded(QTmApplication *)),
                this,           SLOT  (upnpApplicationAdded(QTmApplication *)));
        connect(m_TerminalMode, SIGNAL(upnpApplicationStatusChanged(QString, QTmGlobal::TmStatus)),
                this,           SLOT  (upnpApplicationStatusChanged(QString, QTmGlobal::TmStatus)));
        connect(m_TerminalMode, SIGNAL(upnpApplicationStarted(QTmApplication *)),
                this,           SLOT  (upnpApplicationStarted(QTmApplication *)));
    }

    void deviceDetected(QString interfaceName, QHostAddress hostAddress) {
        m_upnpId = m_TerminalMode->upnpStartControlPoint();
        m_TerminalMode->upnpConnectControlPoint(interfaceName, hostAddress.toString(), m_upnpId.id);
    }

    void deviceLost(QHostAddress hostAddress) {
        m_TerminalMode->upnpDisconnectControlPoint(m_upnpId);
        m_TerminalMode->upnpStopControlPoint(m_upnpId);
    }

    void upnpApplicationAdded(QTmApplication *application) {
        [...]
    }

    void upnpApplicationStarted(QTmApplication *application) {
        [...]
    }

    void upnpApplicationStatusChanged(QString appId, QTmGlobal::TmStatus status) {
        [...]
    }
  \endcode

  \section1 VNC Client
  The VNC client allows the QTmClient to connect to a VNC server, receiving the server's framebuffer data and providing
  key and pointer events back. The VNC client does support all Terminal Mode extensions as well as Desktop Size
  Pseudo Encoding.

  The VNC client provides the following functions:
  \list
    \o int vncStartClient(), starting a VNC client and returning a unique identifier.
    \o void vncConnectClient(QTmApplication, int id), connecting the VNC client, identified through
       \a id, to receive the user interface of the given \a application.
    \o void vncDisconnectClient(int id), disconnect the VNC client, identified through \a id, from the VNC server.
    \o void vncStopClient(int id), stop the VNC client, identified through \a id.
  \endlist
  A disconnected VNC client, can be connected again at any time. A stopped VNC client must be started first,
  before it can be connected again.

  In addition to the above give basic VNC operation functions, the client does support the following configuration
  functions as well.
  \list
  \o void vncSetColorFormat(QTmGlobal::TmColorFormat colorFormat, int id), setting the clients \a id color format.
  \o void vncSetRunLengthEncoding(bool enable, int id), setting the client \a id to support run length encoding.
  \o void vncSetIncrementalUpdate(bool enable, int id), setting the client \a id to support incremental framebuffer updates.
  \o void vncSetScalingSupport   (bool enable, int id), setting the client \a id to support framebuffer scaling.
  \o void vncSetPreferredSize    (QSize size,  int id), setting the clients \a id preferred framebuffer size.
  \endlist
  The VNC client does provide a widget, which contains the uptodate framebuffer copy. Access to the widget is provided
  via the function QWidget *vncClientWidget(int id). In addition the client allows to input dedicated keys via
  void vncClickVirtualKey(int keySym, int id). It must be noted that regular keypress and pointer events are captured
  as well, as long as the VNC widget is in focus.

  The VNC client will emit the following signals:
  \list
    \o void vncClientResized(QSize size, int id), signalling that the server's framebuffer has been changed.
    \o void vncClientConnected(int id), signalling that the VNC client has been connected.
    \o void vncClientDisconnected(int id), signalling that the VNC client has been disconnected.
    \o void vncClientContextInformation(QRect area, int appId, QTmAppInfo *appCategory, QTmDisplayInfo *conCategory, int id),
       signalling that the framebuffer content has changed to the new context information.
  \endlist

  The VNC client is started and controled, using the following example code:
  \code
    int      m_vncId;
    QWidget *m_vncWidget;

    void foo() {
        connect(m_TerminalMode, SIGNAL(vncClientConnected(int)),      this, SLOT(vncConnected(int)));
        connect(m_TerminalMode, SIGNAL(vncClientDisconnected(int)),   this, SLOT(vncDisconnected(int)));
        connect(m_TerminalMode, SIGNAL(vncClientResized(QSize, int)), this, SLOT(vncResized(QSize, int)));
    }

    void deviceLost(QHostAddress hostAddress) {
        m_TerminalMode->vncDisconnectClient(m_vncId);
    }

    void upnpApplicationAdded(QTmApplication *application) {
        m_vncWidget = m_TerminalMode->vncClientWidget(m_vncId);
        show(m_vncWidget);

        m_TerminalMode->vncSetColorFormat      (QTmGlobal::RGB565, m_vncId);
        m_TerminalMode->vncSetPreferredSize    (QSize(800, 480), m_vncId);
        m_TerminalMode->vncSetRunLengthEncoding(true, m_vncId);
        m_TerminalMode->vncSetIncrementalUpdate(true, m_vncId);
        m_TerminalMode->vncSetScalingSupport   (true, m_vncId);

        m_TerminalMode->vncConnectClient(application->url(), m_vncId);
    }

    void MainApplication::vncResized(QSize size, int id) {
        [...]
    }

    void MainApplication::vncConnected(int id) {
        m_vncWidget->setFocus();
    }

    void MainApplication::vncDisconnected(int id) {
        m_TerminalMode->vncStopClient(m_vncId);
    }
  \endcode
 */


/*!
  Class constructor.
  Initialized class variables and register all meta types defined in QTmGlobal.h.
  \a parent points to the objects parent.
 */
QTmClient::QTmClient(QObject *parent)
    : QObject(parent)
    , m_upnpControlPoint(0)
    , m_maxVncId(0)
    , m_networkObserver(0)
{
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Terminal Mode Class created";
#endif
    qRegisterMetaType<QTmGlobal::TmAppCategory>    ("QTmGlobal::TmAppCategory");
    qRegisterMetaType<QTmGlobal::TmVisualCategory> ("QTmGlobal::TmVisualCategory");
    qRegisterMetaType<QTmGlobal::TmVisualRules>    ("QTmGlobal::TmVisualRules");
    qRegisterMetaType<QTmGlobal::TmAudioCategory>  ("QTmGlobal::TmAudioCategory");
    qRegisterMetaType<QTmGlobal::TmTrustLevel>     ("QTmGlobal::TmTrustLevel");
    qRegisterMetaType<QTmGlobal::TmStatus>         ("QTmGlobal::TmStatus");
    qRegisterMetaType<QTmGlobal::TmResourceStatus> ("QTmGlobal::TmResourceStatus");
    qRegisterMetaType<QTmGlobal::TmDirection>      ("QTmGlobal::TmDirection");
    qRegisterMetaType<QTmGlobal::TmColorFormat>    ("QTmGlobal::TmColorFormat");

    qmlRegisterType<QTmDeclarativeItem>("QTerminalMode", 1, 0, "TerminalMode");

    m_rfbClientMap.insert(-1, 0);
}
/*!
  Class destructor.
*/
QTmClient::~QTmClient() {
}

// ************************************************************
// *****                 NETWORK OBSERVER                 *****
// ************************************************************
/*!
  \fn QTmClient::networkDeviceDetected(QString interfaceName, QHostAddress ipAddress)
  Signal emitted from the network observer to signal that a new network device is available.
  The network device's host address is given in \a ipAddress; \a interfaceName provides the name of the interface.
  Use QTmClient::upnpConnectControlPoint() to start receiving advertisements from the detected interface.
 */
/*!
  \fn QTmClient::networkDeviceLost(QHostAddress ipAddress)
  Signal emitted from the network observer to signal that a known network device is not available any more.
  The network device's host address is given in \a ipAddress.
  On a device lost, the running UPnP control point(s) and VNC clients should be stopped and started again, once
  the network becomes available again.
 */
/*!
  Instantiate and start the network observer instance of QTmNetworkObserver and connect to the relevant signals.
  The checking for available interfaces is started.
  The network observer will emit the signal QTmClient::networkDeviceDetected() signal, indicating any new interface found and
  it will emit the signal QTmClient::networkDeviceLost() indiciating the loss of a known one.

  The network observer can be configure, which interfaces to monitor.
  The parameters \a monitorIPv4 and \a monitorIPv6 indicate,
  whether the network observer will monitor IPv4 and IPv6 interfaces respectively.

  Use QTmClient::networkStopObserver() to stop the network observer.

  Note: If both, IPv4 and IPv6, interfaces are monitored, the network observer will signal the existence of IPv4 and
      IPv6 interfaces separately.
 */
void QTmClient::networkStartObserver(bool monitorIPv4, bool monitorIPv6) {
    m_networkObserver = new QTmNetworkObserver(this, monitorIPv4, monitorIPv6);
    connect(m_networkObserver, SIGNAL(addedAddress(QString, QNetworkAddressEntry)),
            this,              SLOT  (deviceDetected(QString, QNetworkAddressEntry)),
            Qt::DirectConnection);
    connect(m_networkObserver, SIGNAL(removedAddress(QNetworkAddressEntry)),
            this,              SLOT  (deviceLost(QNetworkAddressEntry)),
            Qt::DirectConnection);
    m_networkObserver->checkNetworks();
}
/*!
  Stop the network observer and disconnect all signals.
  The network observer instance is deleted.
  The network observer can be started any time again using QTmClient::networkStartObserver().
 */
void QTmClient::networkStopObserver() {
    disconnect(m_networkObserver, SIGNAL(addedAddress(QNetworkAddressEntry)),   this, SLOT(deviceDetected(QNetworkAddressEntry)));
    disconnect(m_networkObserver, SIGNAL(removedAddress(QNetworkAddressEntry)), this, SLOT(deviceLost(QNetworkAddressEntry)));
    if (m_networkObserver)
        delete m_networkObserver;
    m_networkObserver = 0;
}
/*!
  Return the type of the network bearer, identified by the \a interfaceName.
  */
QNetworkConfiguration::BearerType QTmClient::networkBearerType(QString interfaceName) {
    return m_networkObserver->bearerType(interfaceName);
}

// ************************************************************
// *****                UPNP CONTROL POINT                *****
// ************************************************************
/*!
  \fn QTmClient::remoteServerDeviceAdded(QTmRemoteServer *remoteServerDevice)
  Signal emitted from the UPnP Control Point to inform about a new \a remoteServerDevice, being found on the interface.
  The \a remoteServerDevice will provide all available information from the server's device XML.
 */
/*!
  \fn QTmClient::remoteServerDeviceRemoved(QTmRemoteServer *remoteServerDevice)
  Signal emitted from the UPnP Control Point to inform that \a remoteServerDevice has been removed from the interface.
 */
/*!
  Instantiate and start the UPnP Control Point instance of QTmUpnpControlPoint. Returns false if the Control Point,
  has been already started; returns true otherwise.
  The function will connect two signals:
  It will emit the following signals:
  \list
    \o QTmClient::remoteServerDeviceAdded(), if a remote server device is found
    \o QTmClient::remoteServerDeviceRemoved(), if a remote server device is removed
  \endlist
  Note: The control point will only search for new remote server, after the control point has been connected to
  an interface and host address.
 */
bool QTmClient::upnpStartControlPoint()
{
    if (m_upnpControlPoint) {
        qDebug() << "QTmClient:" << "UPnP Control Point already started";
        return false;
    }

#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Start UPnP Control Point";
#endif
    m_upnpControlPoint = new QTmUpnpControlPoint();

    //Init signal/slot mechanism
    connect(m_upnpControlPoint, SIGNAL(remoteServerDeviceAdded(QTmRemoteServer *)),
            this,               SIGNAL(remoteServerDeviceAdded(QTmRemoteServer *)),
            Qt::DirectConnection);
    connect(m_upnpControlPoint, SIGNAL(remoteServerDeviceRemoved(QTmRemoteServer *)),
            this,               SIGNAL(remoteServerDeviceRemoved(QTmRemoteServer *)),
            Qt::DirectConnection);
    return true;
}
/*!
  Connect the UPnP Control Point to the host address given in \a hostAddress and the interface \a interfaceName.
  The control point will now listen and search for new Terminal Mode Remote Server devices.
  The control point will not wait for the next server advertisement, but rather perform an proactive MSearch query.
  Returns with false, if either no control point is running, or the connection setup failed.
  Note: The UPnP Control Point can be connected to multiple different interfaces the same time.
 */
bool QTmClient::upnpConnectControlPoint(QString interfaceName, QHostAddress hostAddress) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Connect UPnP Control Point to" << hostAddress;
#endif
    if (!m_upnpControlPoint) {
        qDebug() << "QTmClient:" << "UPnP Control Point not started!";
        return false;
    }

    if (!m_upnpControlPoint->connectUpnp(interfaceName, hostAddress))
        return false;

    if (!m_upnpControlPoint->sendMSearchQuery(hostAddress))
        qWarning() << "QTmClient" << "Could not send M-SEARCH";

    return true;
}
/*!
  Disconnect the UPnP Control Point from the host address \a hostAddress.
  All remote Terminal Mode server devices for this address will be removed.
 */
void QTmClient::upnpDisconnectControlPoint(QHostAddress hostAddress) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Disconnect UPnP Control Point from" << hostAddress;
#endif
    if (m_upnpControlPoint)
        m_upnpControlPoint->disconnectUpnp(hostAddress);
    else
        qDebug() << "QTmClient:" << "UPnP Control Point not started!";
}
/*!
  Stop the UPnP Control Point. The control point instance is deleted.
  The signals previously connected are disconnected.
 */
void QTmClient::upnpStopControlPoint() {
    if (!m_upnpControlPoint) {
        qDebug() << "QTmClient:" << "UPnP Control Point not started!";
        return;
    }

#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Stop UPnP Control Point";
#endif

    //Deinit signal/slot mechanism
    disconnect(m_upnpControlPoint, SIGNAL(remoteServerDeviceAdded(QTmRemoteServer *)),
               this,               SIGNAL(remoteServerDeviceAdded(QTmRemoteServer *)));
    disconnect(m_upnpControlPoint, SIGNAL(remoteServerDeviceRemoved(QTmRemoteServer *)),
               this,               SIGNAL(remoteServerDeviceRemoved(QTmRemoteServer *)));

    //Close UPnP Control Point
    delete m_upnpControlPoint;
    m_upnpControlPoint = 0;
}
/*!
  Launch the application \a application on the remote server device, using a UPnP Launch Application SOAP action.
  The function will return true, when the control point has sucessfully sent the Launch Application SOAP action.
  The signal QTmClient::upnpApplicationStarted() is emitted, when the application's user interface is accessible.
  Do not connect to the remote user interface via QTmClient::vncConnectClient() at this point.
  Returns immediately, if the UPnP control point associated with the application not existing anymore.
  The associated control is accessbile via QTmApplication::upnpControlPointId().
 */
bool QTmClient::upnpLaunchApplication(QTmApplication *application) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Launch Application" << application->appId();
#endif

    QTmRemoteServer *server = remoteServerByApplication(application);
    return server ? server->launchApplication(application) : false;
}
/*!
  Terminate the application \a application on the remote server device, using a UPnP Terminate Application SOAP action.
  The function will return true, when the control point has sucessfully successfully sent the
  Terminate Application SOAP action.
  Returns immediately, if the UPnP control point associated with the application not existing anymore.
  This function will not disconnect the VNC connection. Use QTmClient::vncDisconnectClient() instead.
 */
bool QTmClient::upnpTerminateApplication(QTmApplication *application) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Terminate Application" << application->appId();
#endif

    QTmRemoteServer *server = remoteServerByApplication(application);
    return server ? server->terminateApplication(application) : false;
}
/*!
  Get Status of the application \a application on the remote server device, using a UPnP Get Application Status SOAP action.
  The function will return the status of type QTmGlobal::TmStatus.
  Get Application Status.
 */
QTmGlobal::TmStatus QTmClient::upnpGetApplicationStatus(QTmApplication *application) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Get Application Status of App with Id" << application->appId();
#endif

    QTmRemoteServer *server = remoteServerByApplication(application);
    return server ? server->getApplicationStatus(application) : QTmGlobal::StatusUnknown;
}

// ************************************************************
// *****                    VNC CLIENT                    *****
// ************************************************************
/*!
  \fn QTmClient::vncClientResized(QSize size, int id)
  Signal emitted from the VNC client \a id to inform about a new framebuffer size \a size.
  This signal is received, when the client receives the VNC ServerInit message and any subsequent
  Desktop Size Pseudo Encoding message.
 */
/*!
  \fn QTmClient::vncClientConnected(int id)
  Signal emitted from the VNC client \a id to inform that the VNC client has been successfully connected
  to the VNC server, and that the client is receiving framebuffer updates.
 */
/*!
  \fn QTmClient::vncClientDisconnected(int id)
  Signal emitted from the VNC client \a id to inform that the VNC client has been disconnected from the VNC server.
 */
/*!
  \fn QTmClient::vncClientKeyboardTrigger(QPoint point, bool remove, int id)
  Signal emitted from the VNC client \a id to inform that a keyboard trigger message has been received from the server.
  \a point gives the QPoint location of the cursor and \a remove tells whether the keyboard should be removed ('true')
  or shown ('false').
 */
/*!
  \fn QTmClient::vncClientKeyEventList(QList<unsigned int> *list, unsigned int counter, int id)
  Signal emitted from the VNC client \a id to informat that a key event listing message has been received from the server.
  The pointer to the list of supported key events is given in \a list.
  The parameter \a counter indicates the event counter.
 */
/*!
  \fn QTmClient::vncClientFbAlternativeText(unsigned int appId, QString text, int id)
  Signal emitted from the VNC client \a id to informa that a framebuffer alternative text message has been received from the server.
  The corresponding application is given in \a appId and the alternative text in \a text.
 */
/*!
  \fn QTmClient::vncClientContextInformation(QRect area, int appId, QTmAppInfo *appCategory, QTmDisplayInfo *conCategory, int id)
  Signal emitted from the VNC client \a id that new context information is available for the framebuffer rectangle
  \a area. The area belongs to the application \a appId. The application category information is given in \a appCategory,
  the content category information in \a conCategory.
 */
/*!
  Instantiate and start the VNC client instance. Returns a unique identifier,
  which must be used in any subsequent function call, accessing the instance.
  The same identifier is used in any emitted signal to identify the emitting VNC client.
 */
int QTmClient::vncStartClient() {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Start VNC Client";
#endif
    int id = m_maxVncId;
    m_maxVncId++;

    QTmRfbClient *rfbClient = new QTmRfbClient();
    rfbClient->setClientConfig(QTmConfiguration::ConfigKeyboardLanguage,    ('e' << 8) | 'n');
    rfbClient->setClientConfig(QTmConfiguration::ConfigKeyboardCountry,     ('u' << 8) | 's');
    rfbClient->setClientConfig(QTmConfiguration::ConfigUiLanguage,          ('e' << 8) | 'n');
    rfbClient->setClientConfig(QTmConfiguration::ConfigUiCountry,           ('u' << 8) | 's');
    //Key Event Configuration
    rfbClient->setClientConfig(QTmConfiguration::ConfigEventKnobKey,         0x00000000);
    rfbClient->setClientConfig(QTmConfiguration::ConfigEventDeviceKey,       0x00000023);
    rfbClient->setClientConfig(QTmConfiguration::ConfigEventMediaKey,        0x00000000);
    rfbClient->setClientConfig(QTmConfiguration::ConfigEventItuKeypad,       false);
    rfbClient->setClientConfig(QTmConfiguration::ConfigEventFunctionKeys,    0x00);
    //Pointer Event Configuration
    rfbClient->setClientConfig(QTmConfiguration::ConfigEventPointer,         0x00000101);
    rfbClient->setClientConfig(QTmConfiguration::ConfigEventTouch,           0);
    rfbClient->setClientConfig(QTmConfiguration::ConfigButtonMask,           0x00000101);
    rfbClient->setClientConfig(QTmConfiguration::ConfigEventPointer,         0x00000101);
    rfbClient->setClientConfig(QTmConfiguration::ConfigTouchNumber,          0x00);
    rfbClient->setClientConfig(QTmConfiguration::ConfigPressureMask,         0x00);
    //Enable framebuffer updates
    rfbClient->setClientConfig(QTmConfiguration::ConfigFbUpdates,            true);

    m_rfbClientMap.insert(id, rfbClient);

    connect(rfbClient, SIGNAL(   sessionConnected(QString)),
            this,      SLOT  (rfbSessionConnected(QString)),
            Qt::QueuedConnection);
    connect(rfbClient, SIGNAL(   sessionDisconnected()),
            this,      SLOT  (rfbSessionDisconnected()),
            Qt::QueuedConnection);
    connect(rfbClient, SIGNAL(   displayConfigurationReceived()),
            this,      SLOT  (rfbDisplayConfiguration()),
            Qt::BlockingQueuedConnection);
    connect(rfbClient, SIGNAL(   eventConfigurationReceived()),
            this,      SLOT  (rfbEventConfiguration()),
            Qt::BlockingQueuedConnection);
    connect(rfbClient, SIGNAL(   deviceStatusReceived(unsigned int)),
            this,      SLOT  (rfbDeviceStatus(unsigned int)),
            Qt::QueuedConnection);
    connect(rfbClient, SIGNAL(   framebufferResized(QSize)),
            this,      SLOT  (rfbDesktopSize(QSize)),
            Qt::BlockingQueuedConnection);
    connect(rfbClient, SIGNAL(   virtualKeyboardTriggerReceived(QPoint,bool)),
            this,      SLOT  (rfbVirtualKeyboardTrigger(QPoint,bool)),
            Qt::QueuedConnection);
    connect(rfbClient, SIGNAL(   keyEventListReceived(QList<uint>*,unsigned int)),
            this,      SLOT  (rfbKeyEventList(QList<uint>*, unsigned int)),
            Qt::QueuedConnection);
    connect(rfbClient, SIGNAL(   framebufferAlternativeTextReceived(uint,QString)),
            this,      SLOT  (rfbFramebufferAlternativeText(uint,QString)),
            Qt::QueuedConnection);
    connect(rfbClient, SIGNAL(   contextInformationReceived(QRect,int,QTmAppInfo*,QTmDisplayInfo*)),
            this,      SLOT  (rfbContextInformation(QRect,int,QTmAppInfo*,QTmDisplayInfo*)),
            Qt::BlockingQueuedConnection);
    return id;
}
/*!
  Connect the VNC client, identified by \a id, to the application's user interface given in \a application.
  The URL of the user interface is available on receiving the QTmApplication::upnpApplicationStarted()
  signal, via QTmApplication::url().

  The VNC client will emit the following signals:
  \list
    \o QTmClient::vncClientConnected()
    \o QTmClient::vncClientDisconnected()
    \o QTmClient::vncClientResized()
    \o QTmClient::vncClientKeyboardTrigger()
    \o QTmClient::vncClientKeyEventList()
    \o QTmClient::vncClientFbAlternativeText()
    \o QTmClient::vncClientContextInformation()
  \endlist

  The function connects all relevant signals and start the VNC session, i.e. doing the RFB version handshake, the RFB
  security handshake and the RFB server and client initialization.
  The VNC client will emit a vncClientConnected signals, once the RFB initialization phase is finished.
  Returns immediately, if the VNC client \a id is not existing anymore.
 */
void QTmClient::vncConnectClient(QTmApplication *application, int id) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Connect VNC Client" << id << "to" << application->url();
#endif
    QTmRfbClient *rfbClient = m_rfbClientMap.value(id, 0);
    if(!rfbClient) {
        qWarning() << "QTmClient::vncConnectClient() was called before the rfbClient was created via QTmClient::vncStartClient() ... no updates and event will work ... fix in application and/or API/lib ";
        return;
    }
    // Start session
    if (!rfbClient->sessionStart(application->url()))
        vncDisconnectClient(id);
    QThread *m_thread = new QThread();
    m_threadMap.insert(id, m_thread);
    m_thread->start();
    rfbClient->moveToThread(m_thread);
}
/*!
  Disconnect the VNC client, identified by \a id. All relevant signals are disconnected and the VNC session is stopped.
  A signal vncClientDisconnected is emitted.
  Returns immediately, if the VNC client \a id is not existing anymore.
 */
void QTmClient::vncDisconnectClient(int id) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Disconnect VNC Client" << id;
#endif
    QTmRfbClient *rfbClient = m_rfbClientMap.value(id, 0);
    if (!rfbClient)
        return;
    //Stop session
    rfbClient->sessionStop();
    emit vncClientDisconnected(id);
}
/*!
  Stop the VNC client, identified by \a id.
  The VNC client instance is deleted.
  Returns immediately, if the VNC client \a id is not existing anymore.
 */
void QTmClient::vncStopClient(int id) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Stop VNC Client" << id;
#endif
    // disconnect client from surface
    disconnectRfbClientFromSurface(id);
    // clean-up client
    QTmRfbClient *rfbClient = m_rfbClientMap.value(id, 0);
    if (rfbClient) {
        m_rfbClientMap.remove(id);
        disconnect(rfbClient, SIGNAL(   sessionConnected(QString)),
                   this,      SLOT  (rfbSessionConnected(QString)));
        disconnect(rfbClient, SIGNAL(   sessionDisconnected()),
                   this,      SLOT  (rfbSessionDisconnected()));
        disconnect(rfbClient, SIGNAL(   displayConfigurationReceived()),
                   this,      SLOT  (rfbDisplayConfiguration()));
        disconnect(rfbClient, SIGNAL(   eventConfigurationReceived()),
                   this,      SLOT  (rfbEventConfiguration()));
        disconnect(rfbClient, SIGNAL(   deviceStatusReceived(unsigned int)),
                   this,      SLOT  (rfbDeviceStatus(unsigned int)));
        disconnect(rfbClient, SIGNAL(   framebufferResized(QSize)),
                   this,      SLOT  (rfbDesktopSize(QSize)));
        disconnect(rfbClient, SIGNAL(   virtualKeyboardTriggerReceived(QPoint,bool)),
                   this,      SLOT  (rfbVirtualKeyboardTrigger(QPoint,bool)));
        disconnect(rfbClient, SIGNAL(   keyEventListReceived(QList<uint>*,unsigned int)),
                   this,      SLOT  (rfbKeyEventList(QList<uint>*, unsigned int)));
        disconnect(rfbClient, SIGNAL(   framebufferAlternativeTextReceived(uint,QString)),
                   this,      SLOT  (rfbFramebufferAlternativeText(uint,QString)));
        disconnect(rfbClient, SIGNAL(   contextInformationReceived(QRect,int,QTmAppInfo*,QTmDisplayInfo*)),
                   this,      SLOT  (rfbContextInformation(QRect,int,QTmAppInfo*,QTmDisplayInfo*)));
        delete rfbClient;
    }
    QThread *thread = m_threadMap.value(id, 0);
    if (thread) {
        thread->quit();
        m_threadMap.remove(id);
    }
    // TODO not sure if it need to be delete or the caller of vncClientWidget() should take care ...
    m_widgetMap.remove(id);
    m_declarativeMap.remove(id);
}

void QTmClient::rfbDeviceStatus(unsigned int deviceStatus)
{
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Device status received:" << QString("0x%1").arg(deviceStatus, 0, 16);
#else
    Q_UNUSED(deviceStatus)
#endif
}

/*!
  Return the VNC client widget, containing the QImage of the server's framebuffer. This image is automatically
  updated with new incoming framebuffer updates.
  Returns 0 (zero) if the \a id is not existing.
  The caller must take ownership of this widgets (setParent(...) / add to layout)
  or show it as top levelwindow via show()
 */
QWidget* QTmClient::vncClientWidget(int id) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "Get VNC client" << id;
#endif
    if(m_declarativeMap.value(id, 0) != 0) {
        qWarning() << "Tried to request a QTmClient::vncClientWidget for id:" << id
                   << " ... but there is already a QTmDeclarativeItem for this id! returning 0";
        return 0;
    }
    QTmWidget *tmWidget = m_widgetMap.value(id, 0);
    if(tmWidget == 0) {
        tmWidget = new QTmWidget();
        m_widgetMap.insert(id, tmWidget);
    }
    QTmRfbClient *rfbClient = m_rfbClientMap.value(id, 0);
    if(rfbClient != 0) {
        connectRfbClientToSurface(id);
        connect(tmWidget,  SIGNAL(tmKeyEvent(QTmKeyEvent *)),
                rfbClient, SLOT  (  keyEvent(QTmKeyEvent *)),
                Qt::BlockingQueuedConnection);
        connect(tmWidget,  SIGNAL(tmPointerEvent(QTmPointerEvent *)),
                rfbClient, SLOT  (  pointerEvent(QTmPointerEvent *)),
                Qt::BlockingQueuedConnection);
    }
    else {
        qWarning() << "QTmClient::vncClientWidget() was called before the rfbClient was created via QTmClient::vncStartClient() ... no updates and event will work ... fix in application and/or API/lib ";
    }
    return tmWidget;
}

void QTmClient::setClientDeclarativeItem(QTmDeclarativeItem* item, int id) {
    if(m_widgetMap.value(id, 0) != 0) {
        qWarning() << "Tried to QTmClient::setClientDeclarativeItem for id:" << id
                   << " ... but there is already a QTmWidget for this id! => returning immediately and ignoring QTmDeclrativeItem";
        return;
    }
    if(m_declarativeMap.value(id, 0) != 0) {
        qWarning() << "Tried to QTmClient::setClientDeclarativeItem for id:" << id
                   << " a SECOND time. This is not supported => returning immediately and ignoring QTmDeclrativeItem";
        return;
    }
    if(m_declarativeMap.value(id, 0) == item)
        return;
    m_declarativeMap.insert(id, item);
    QTmRfbClient *rfbClient = m_rfbClientMap.value(id, 0);
    if(rfbClient != 0) {
        connectRfbClientToSurface(id);
        connect(item,      SIGNAL(tmKeyEvent(QTmKeyEvent *)),
                rfbClient, SLOT  (  keyEvent(QTmKeyEvent *)),
                Qt::BlockingQueuedConnection);
        connect(item,      SIGNAL(tmPointerEvent(QTmPointerEvent *)),
                rfbClient, SLOT  (  pointerEvent(QTmPointerEvent *)),
                Qt::BlockingQueuedConnection);
    }
    else {
        qWarning() << "QTmClient::setClientDeclarativeItem() was called before the rfbClient was created via QTmClient::vncStartClient() ... no updates and event will work ... fix in application and/or API/lib ";
    }
}


/*!
  Set the color format of the VNC client \a id to \a colorFormat.
  The color format must be changed before the VNC client has been connected.
  Returns immediately, if the VNC client \a id is not existing anymore.
 */
void QTmClient::vncSetColorFormat(QTmGlobal::TmColorFormat colorFormat, int id) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "(VNC Client" << id << "): Client Color Format:" << colorFormat;
#endif
    QTmRfbClient *rfbClient = m_rfbClientMap.value(id, 0);
    if (!rfbClient) 
        return;
    rfbClient->setClientConfig(QTmConfiguration::ConfigFbColorFormat, colorFormat);
}
/*!
  Set the support for run length encoding for the VNC client \a id.
  If \a enable is true, the VNC client will send the respective SetEncoding message.
  The default value is \c 'false'.
  Returns immediately, if the VNC client \a id is not existing anymore.
  Note: This value can not be set to false again, once set to true.
 */
void QTmClient::vncSetRunLengthEncoding(bool enable, int id) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "(VNC Client" << id << "): Enable Run-Length Encoding:" << enable;
#endif
    QTmRfbClient *rfbClient = m_rfbClientMap.value(id, 0);
    if (!rfbClient) 
        return;
    rfbClient->setClientConfig(QTmConfiguration::ConfigRunLengthEncoding, enable ? 1 : 0);
}
/*!
  Set the support for incremental framebuffer updates for the VNC client \a id.
  If \a enable is true, the VNC client will request incremental framebuffer updates,
  otherwise it will request non-incremental updates.
  Returns immediately, if the VNC client \a id is not existing anymore.
  The default value is \c 'false'.
 */
void QTmClient::vncSetIncrementalUpdate(bool enable, int id) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "(VNC Client" << id << "): Enable Incremental Updates:" << enable;
#endif
    QTmRfbClient *rfbClient = m_rfbClientMap.value(id, 0);
    if (!rfbClient) 
        return;
    rfbClient->setClientConfig(QTmConfiguration::ConfigIncrementalUpdate, enable);
}
/*!
  Set the preferred framebuffer size to \a size for the VNC client \a id.
  If \a scaling is true, the VNC client will not scale the received framebuffer.
  The client will use this information in the ClientDisplayConfiguration message.
  The default scaling value is \c 'false'.
  Returns immediately, if the VNC client \a id is not existing anymore.
 */
void QTmClient::vncSetPreferredSize(QSize size, bool scaling, int id) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient::vncSetPreferredSize:" << "(VNC Client" << id << "): Preferred Framebuffer Size:" << size;
#endif
    QTmRfbClient *rfbClient = m_rfbClientMap.value(id, 0);
    if (!rfbClient) {
        qDebug() << "QTmClient::vncSetPreferredSize:" << "Cannot set framebuffer size for RFB Client!";
        return;
    }
    rfbClient->setClientFbSize(size, scaling);
    QWidget *widget = m_widgetMap.value(id, 0);
    if (!widget) {
        qDebug() << "QTmClient::vncSetPreferredSize:" << "Cannot set minimum size for Widget!";
        return;
    }
    widget->setMinimumSize(size);
    if (!scaling)
        widget->setMaximumSize(size);
    else
        widget->setMaximumSize(QSize(16777215,16777215));
    widget->update();
}
/*!
  Send a Key Press followed by a Key Release event for the key event \a keySym via the
  VNC client \a id to the VNC server.
  Returns immediately, if the VNC client \a id is not existing anymore.
 */
void QTmClient::vncClickVirtualKey(int keySym, int id) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient:" << "(VNC Client" << id << "): Click Virtual Key:" << QString("0x%1").arg(keySym, 0, 16);
#endif
    QTmRfbClient *rfbClient = m_rfbClientMap.value(id, 0);
    if (rfbClient != 0) {
        if (keySym >= Qt::Key_A && keySym <= Qt::Key_Z) {
            QTmKeyEvent eventDown(QTmSurface::translateKey(keySym, Qt::ShiftModifier), true);
            QTmKeyEvent eventUp  (QTmSurface::translateKey(keySym, Qt::ShiftModifier), false);
            rfbClient->keyEvent(&eventDown);
            rfbClient->keyEvent(&eventUp);
        }
        else {
            QTmKeyEvent eventDown(QTmSurface::translateKey(keySym, Qt::NoModifier), true);
            QTmKeyEvent eventUp  (QTmSurface::translateKey(keySym, Qt::NoModifier), false);
            rfbClient->keyEvent(&eventDown);
            rfbClient->keyEvent(&eventUp);
        }
    }
}
/*!
  Send a Framebuffer Blocking Notification message for the designated framebuffer area \a area
  as provided from the application \a appId. Reason for blocking is given in \a reason.
  Message is provided via the VNC client \a id.
 */
void QTmClient::vncBlockFramebuffer(QRect area, unsigned int appId, unsigned int reason, int id) {
#ifdef QT_DEBUG_TM
    qDebug() << "QTmClient::vncBlockFramebuffer:" << "(VNC Client" << id << ")" << "Block Framebuffer";
#endif
    QTmRfbClient *rfbClient = m_rfbClientMap.value(id, 0);
    if (rfbClient != 0)
        rfbClient->framebufferBlockingNotification(area, appId, reason);
}

// Private SLOTS

void QTmClient::deviceDetected(QString networkInterfaceName, QNetworkAddressEntry networkEntry) {
    qDebug() << "QTerminalmode:" << "Device detected" << networkEntry.ip();
    emit networkDeviceDetected(networkInterfaceName, networkEntry.ip());
}

void QTmClient::deviceLost(QNetworkAddressEntry networkEntry) {
    qDebug() << "QTerminalmode:" << "Device lost" << networkEntry.ip();
    emit networkDeviceLost(networkEntry.ip());
}

QTmRemoteServer* QTmClient::remoteServerByApplication(QTmApplication *application) {
    if (!m_upnpControlPoint)
        return 0;

    // Get the remote server device
    return m_upnpControlPoint->remoteServerById(application->remoteServerId());
}

void QTmClient::rfbDesktopSize(QSize framebufferSize)
{
    int id = m_rfbClientMap.key(qobject_cast<QTmRfbClient*>(sender()), -1);
    if(id != -1) {
        emit vncClientResized(framebufferSize, id);
    }
    else { qWarning() << "void QTmClient::rfbDesktopSize(): sender() is not known! error!"; }
}

void QTmClient::rfbSessionConnected(QString serverName) {
    qDebug() << "QTmClient::rfbSessionConnected:" << "Connected with VNC Server" << serverName;
    QTmRfbClient *rfbClient = qobject_cast<QTmRfbClient*>(sender());
    int id = m_rfbClientMap.key(rfbClient, -1);
    if(id != -1) {
        emit vncClientConnected(id);
    }
    else {
        qWarning() << "void QTmClient::rfbSessionConnected(): sender() is not known! error!";
    }
}

void QTmClient::rfbSessionDisconnected()
{
    int id = m_rfbClientMap.key(qobject_cast<QTmRfbClient*>(sender()), -1);
    if(id != -1) {
        emit vncClientDisconnected(id);
    }
    else { qWarning() << "void QTmClient::rfbSessionDisconnected(): sender() is not known! error!"; }
}


void QTmClient::rfbEventConfiguration() {
    int id = m_rfbClientMap.key(qobject_cast<QTmRfbClient*>(sender()), -1);
    if(id != -1)
        emit vncClientEventConfiguration(id);
    else
        qWarning() << "void QTmClient::rfbSessionDisconnected(): sender() is not known! error!";
}

void QTmClient::rfbDisplayConfiguration() {
    int id = m_rfbClientMap.key(qobject_cast<QTmRfbClient*>(sender()), -1);
    if(id != -1)
        emit vncClientDisplayConfiguration(id);
    else
        qWarning() << "void QTmClient::rfbSessionDisconnected(): sender() is not known! error!";
}

void QTmClient::rfbVirtualKeyboardTrigger(QPoint point, bool remove)
{
    int id = m_rfbClientMap.key(qobject_cast<QTmRfbClient*>(sender()), -1);
    if(id != -1) {
        emit vncClientKeyboardTrigger(point, remove, id);
    }
    else { qWarning() << "void QTmClient::rfbKeyboardTrigger(): sender() is not known! error!"; }
}

void QTmClient::rfbKeyEventList(QList<unsigned int> *list, unsigned int counter)
{
    int id = m_rfbClientMap.key(qobject_cast<QTmRfbClient*>(sender()), -1);
    if(id != -1) {
        emit vncClientKeyEventList(list, counter, id);
    }
    else { qWarning() << "void QTmClient::rfbKeyEventList(): sender() is not known! error!"; }
}

void QTmClient::rfbFramebufferAlternativeText(unsigned int appId, QString text)
{
    int id = m_rfbClientMap.key(qobject_cast<QTmRfbClient*>(sender()), -1);
    if(id != -1) {
        emit vncClientFbAlternativeText(appId, text, id);
    }
    else { qWarning() << "void QTmClient::rfbFbAlternativeText(): sender() is not known! error!"; }
}

void QTmClient::rfbContextInformation(QRect area, int appId, QTmAppInfo *appCategory, QTmDisplayInfo *conCategory)
{
    int id = m_rfbClientMap.key(qobject_cast<QTmRfbClient*>(sender()), -1);
    if(id != -1) {
        qDebug() << "QTmClient::rfbContextInformation:" << "update received";
        emit vncClientContextInformation(area, appId, appCategory, conCategory, id);
    }
    else { qWarning() << "void QTmClient::rfbContextInformation(): sender() is not known! error!"; }
}

bool QTmClient::connectRfbClientToSurface(int id)
{
    QTmRfbClient* rfbClient = m_rfbClientMap.value(id, 0);
    if(!rfbClient)
        return false;

    QTmWidget* tmWidget = m_widgetMap.value(id, 0);
    if(tmWidget != 0) {
        rfbClient->setSurface((QTmSurface*)tmWidget);
    }
    QTmDeclarativeItem* tmItem = m_declarativeMap.value(id, 0);
    if(tmItem != 0) {
        Q_ASSERT(tmWidget == 0);
        rfbClient->setSurface((QTmSurface*)tmItem);
    }
    return true;
}

void QTmClient::disconnectRfbClientFromSurface(int id)
{
    QTmRfbClient* rfbClient = m_rfbClientMap.value(id, 0);
    if (rfbClient) {
        rfbClient->setSurface(0);

        QTmWidget* tmWidget = m_widgetMap.value(id, 0);
        if(tmWidget != 0) {
            disconnect(tmWidget, SIGNAL(tmKeyEvent(QTmKeyEvent *)),         rfbClient, SLOT(keyEvent(QTmKeyEvent *)));
            disconnect(tmWidget, SIGNAL(tmPointerEvent(QTmPointerEvent *)), rfbClient, SLOT(pointerEvent(QTmPointerEvent *)));
        }
        QTmDeclarativeItem* tmItem = m_declarativeMap.value(id, 0);
        if(tmItem != 0) {
            disconnect(tmItem, SIGNAL(tmKeyEvent(QTmKeyEvent *)),         rfbClient, SLOT(keyEvent(QTmKeyEvent *)));
            disconnect(tmItem, SIGNAL(tmPointerEvent(QTmPointerEvent *)), rfbClient, SLOT(pointerEvent(QTmPointerEvent *)));
        }
    }
}
