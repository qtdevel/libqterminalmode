/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QTmTouchEvent.h"

QTmTouchEvent::QTmTouchEvent() :
        m_point(QPoint(0,0)),
        m_eventId(0),
        m_pressure(0) {
}

QPoint QTmTouchEvent::point() {
    return m_point;
}
void QTmTouchEvent::setPoint(QPoint point) {
    m_point = point;
}

int QTmTouchEvent::eventId() {
    return m_eventId;
}
void QTmTouchEvent::setEventId(int eventId) {
    m_eventId = eventId;
}

int QTmTouchEvent::pressure() {
    return m_pressure;
}
void QTmTouchEvent::setPressure(int pressure) {
    m_pressure = pressure;
}
