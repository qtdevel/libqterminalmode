/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QtNetwork>
#include <QtNetwork/QHostAddress>
#include <QtXml/QDomDocument>
#include <QStringList>
#include "QUpnpHttpServer.h"

//Implements the HTTP Server for UPnP Control Point
//Used for receiving UPnP event notifications using GENA
QUpnpHttpServer::QUpnpHttpServer(QObject *parent) : QTcpServer(parent) {}

QUpnpHttpServer::~QUpnpHttpServer()
{
    close();
}

void QUpnpHttpServer::addPropertyChangeListener(QString sid, QUpnpPropertyChangeListener *listener)
{
#ifdef QT_DEBUG_TM_UPNP_HTTP
    qDebug() << "QUpnpHttpServer:" << "Add property change listener for" << sid;
#endif
    m_propChangeListenerMutex.lock();
    m_propChangeListenerHash.insert(sid, listener);
    m_propChangeListenerMutex.unlock();
}

void QUpnpHttpServer::removePropertyChangeListener(QString sid)
{
#ifdef QT_DEBUG_TM_UPNP_HTTP
    qDebug() << "QUpnpHttpServer:" << "Remove property change listener for"  << sid;
#endif
    m_propChangeListenerMutex.lock();
    m_propChangeListenerHash.remove(sid);
    m_propChangeListenerMutex.unlock();
}

void QUpnpHttpServer::incomingConnection(int socket)
{
    // When a new client connects, the server constructs a QTcpSocket and all
    // communication with the client is done over this QTcpSocket. QTcpSocket
    // works asynchronously, this means that all the communication is done
    // in the two slots readClient() and discardClient().
    QTcpSocket* s = new QTcpSocket(this);
    connect(s,    SIGNAL(readyRead()),
            this, SLOT(readClient()),
            Qt::AutoConnection);
    connect(s,    SIGNAL(disconnected()),
            this, SLOT(discardClient()),
            Qt::AutoConnection);
    s->setSocketDescriptor(socket);
}

void QUpnpHttpServer::readClient()
{
    //Server checks if it is a UPnP eventing message (notify) and sends an acknowledgment back
    QTcpSocket* s1 = (QTcpSocket*)sender();
    QByteArray rBuffer("");

    //While there's data, read and store it in receiveBuffer
    while (s1->bytesAvailable())
    {
        rBuffer.append(s1->readAll());
    }

#ifdef QT_DEBUG_TM_UPNP_HTTP
    qDebug() << "QUpnpHttpServer:" << "Received UPnP Event";
#endif

    //Process Event Message
    int errCode = processEventMessage(rBuffer);
#ifdef QT_DEBUG_TM_UPNP_HTTP
    if (errCode != 200)
    qDebug() << "QUpnpHttpServer:" << "Processing UPnP Event failed, with error" << errCode;
#endif

    //Return Event Response
    QTextStream os(s1);
    os.setAutoDetectUnicode(true);

    switch(errCode)
    {
        case 200:   os << "HTTP/1.1 200 OK\r\n\r\n";
        break;

        case 400:   os << "HTTP/1.1 400 Bad Request\r\nContent-Type: text/html; charset=\"utf-8\"\r\n\r\n<h1>HTTP/1.1 400 Bad Request</h1>\r\n";
        break;

        default:    os << "HTTP/1.1 412 Precondition Failed\r\nContent-Type: text/html; charset=\"utf-8\"\r\n\r\n<h1>HTTP/1.1 412 Precondition Failed</h1>\r\n";
        break;
    }
}

void QUpnpHttpServer::discardClient()
{
    QTcpSocket* s1 = (QTcpSocket*)sender();
    s1->deleteLater();
}


int QUpnpHttpServer::processEventMessage(QByteArray& buf)
{
#ifdef QT_DEBUG_TM_UPNP_HTTP
    qDebug() << "QUpnpHttpServer:" << "Processing UPnP Event";
#endif

    QString messageStr(buf);

    //Check message type
    if(messageStr.startsWith("NOTIFY", Qt::CaseInsensitive) == false)
        return 400;

    //Parse header
    bool ntFound = false;
    bool ntsFound = false;
    QString sid;
    QStringList lines = messageStr.split('\n');
    foreach (QString line, lines) {
        line = line.trimmed();
        if (line.startsWith("NTS:", Qt::CaseInsensitive)) {
            ntsFound = true;
            if (!line.contains("upnp:propchange", Qt::CaseInsensitive))
                return 412;
        } else if (line.startsWith("NT:", Qt::CaseInsensitive)) {
            ntFound = true;
            if (!line.contains("upnp:event", Qt::CaseInsensitive))
                return 412;
        } else if (line.startsWith("SID:", Qt::CaseInsensitive)) {
            // Check content of SID header
            sid = line.remove("SID:", Qt::CaseInsensitive).trimmed();
        }
    }

    //Check for presence of NT and NTS headers and their values
    if (!ntFound || !ntsFound)
        return 400;

    //Check for presence of SID
    if (sid.isEmpty())
        return 400;

    //Check whether the SID is known
    m_propChangeListenerMutex.lock();
    QUpnpPropertyChangeListener *listener = m_propChangeListenerHash.value(sid, 0);
    m_propChangeListenerMutex.unlock();
    if (!listener)
        return 412;

    //Get the XML portion of message
    int index = messageStr.indexOf("<?xml");
    if (index == -1)
        return 400;
    QDomDocument xml;
    if (xml.setContent(messageStr.mid(index).simplified()) == false)
        return 400;

    //Parse XML to check which evented variable was received
    QDomNodeList propertyNodes = xml.documentElement().elementsByTagName("e:property");
    if (!propertyNodes.isEmpty()) {
        for (int i = 0; ; i++) {
            //Parse e:properties
            QDomNode propertyNode = propertyNodes.at(i);
            if (propertyNode.isNull())
                break;

            //Parse variable
            QDomNode variableNode = propertyNode.firstChild();
            if (variableNode.isNull() || variableNode.nodeName().isEmpty())
                continue;
            QDomNode valueNode = variableNode.firstChild();
            if (valueNode.isNull() || valueNode.nodeValue().isEmpty())
                continue;

            //Process property change event
#ifdef QT_DEBUG_TM_UPNP_HTTP
            qDebug() << "QUpnpHttpServer:" << "property change event "
                    << variableNode.nodeName() << "="
                    << valueNode.nodeValue();
#endif
            listener->processPropertyChangeEvent(variableNode.nodeName(),
                                                 valueNode.nodeValue());
        }
    }
    return 200;//OK
}
