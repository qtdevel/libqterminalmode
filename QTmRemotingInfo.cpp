/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QTmRemotingInfo.h"

/*!
  \class QTmRemotingInfo
  \brief Holds additional information about the remote protocol properties of the advertised remote application.

  This class provides information about the remote protocol used to interact with the respective application,
  which is running on the Terminal Mode server device. This information is used from the Terminal Mode client
  to decide, whether it can interact with the remote application, via the given protocol, or not.

  The information, which is provided consists of the following elements:
  \list
  \o Protocol identifier
  \o Protocol format
  \o Protocol direction
  \endlist

  The QTmRemotingInfo instance is used from the QTmRemoteServer,as part of every advertised application.
  The class does not have a public constructor, but is rather instantiated from the QTmRemoteServer class instance.
 */
/*!
  \property QTmRemotingInfo::protocolId
  \brief Holds the remote protocol used from the advertised application.

  The protocol Id identifies the protocol used to access the remote application. In case of an application,
  which user interface is replicated into the the Terminal Mode client's display, this is VNC.
  In case of Audio server or client functionality, the remote protocol is either RTP or Bluetooth (BT_HFB, BT_A2DP).
  Other remote protocols defined are DAP (Device Attestation Protocol) or proprietary ones.
 */
/*!
  \property QTmRemotingInfo::format
  \brief Holds the format of the remoting protocol.

  The format entry holds further information on the remote protocol used.
  In case of RTP, this entry will contain the supported RTP payload type(s).
  This entry does not contain meaningful information in case of any other remote protocol.
 */
/*!
  \property QTmRemotingInfo::direction
  \brief Holds the direction of the remoting protocol. Used for audio protocols only.

  The direction entry holds further information, whether the remote protocol component is an input or an output
  at the Terminal Mode server. This entry is meaningfull for RTP protocol. An RTP server is identifiewd with
  an output direction, whereas an RTP client is identified with an input direction.
 */

/*!
  Class constructor.
 */
QTmRemotingInfo::QTmRemotingInfo() :
        m_protocolId(QTmGlobal::NONE),
        m_direction(QTmGlobal::DirectionNone) {
    m_format = QString();
}

void QTmRemotingInfo::setProtocolId(QTmGlobal::TmRemoteProtocol protocolId) {
    m_protocolId = protocolId;
}

QTmGlobal::TmRemoteProtocol QTmRemotingInfo::protocolId() {
    return m_protocolId;
}

void QTmRemotingInfo::setFormat(QString format) {
    m_format = format;
}
QString QTmRemotingInfo::format() {
    return format();
}

void QTmRemotingInfo::setDirection(QTmGlobal::TmDirection direction) {
    m_direction = direction;
}
QTmGlobal::TmDirection QTmRemotingInfo::direction() {
    return m_direction;
}
