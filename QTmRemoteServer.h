/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#ifndef QTMREMOTESERVER_H
#define QTMREMOTESERVER_H

#include <QObject>
#include <QMutex>

#include "QTmGlobal.h"
#include "QTmIcon.h"
#include "QTmApplication.h"
#include "QTmClientProfile.h"
#include "QTmUpnpControlPoint.h"

// Terminal Mode UPnP service names
#define UPNP_APP_SERVER_SERVICE                    "urn:schemas-upnp-org:service:TmApplicationServer:1"
#define UPNP_APP_SERVER_SERVICE_METHOD             "GetApplicationList"
#define UPNP_CLIENT_PROFILE_SERVICE                "urn:schemas-upnp-org:service:TmClientProfile:1"
#define UPNP_CLIENT_PROFILE_SERVICE_METHOD         "GetClientProfile"

class QTmUpnpControlPoint;

class QTmRemoteServer : public QObject, QUpnpPropertyChangeListener {
    Q_OBJECT

    Q_PROPERTY(int TmSpecVersionMajor                   READ TmSpecVersionMajor WRITE setTmSpecVersionMajor)
    Q_PROPERTY(int TmSpecVersionMinor                   READ TmSpecVersionMinor WRITE setTmSpecVersionMinor)
    Q_PROPERTY(QString friendlyName                     READ friendlyName       WRITE setFriendlyName)
    Q_PROPERTY(QString manufacturerName                 READ manufacturerName   WRITE setManufacturerName)
    Q_PROPERTY(QUrl manufacturerUrl                     READ manufacturerUrl    WRITE setManufacturerUrl)
    Q_PROPERTY(QString modelDescription                 READ modelDescription   WRITE setModelDescription)
    Q_PROPERTY(QString modelName                        READ modelName          WRITE setModelName)
    Q_PROPERTY(QString modelNumber                      READ modelNumber        WRITE setModelNumber)
    Q_PROPERTY(QUrl modelUrl                            READ modelUrl           WRITE setModelUrl)
    Q_PROPERTY(QString serialNumber                     READ serialNumber       WRITE setSerialNumber)
    Q_PROPERTY(QString UDN                              READ UDN                WRITE setUDN)
    Q_PROPERTY(QString UPC                              READ UPC                WRITE setUPC)
    Q_PROPERTY(QString serverBTAddress                  READ serverBTAddress    WRITE setServerBTAddress)
    Q_PROPERTY(bool canServerStartBT                    READ canServerStartBT   WRITE setCanServerStartBT)
    Q_PROPERTY(QList<QTmIcon *> iconList                READ iconList           WRITE setIconList)
    Q_PROPERTY(QList<QTmApplication *> applicationList  READ applicationList)
    Q_PROPERTY(int remoteServerId                       READ remoteServerId)
    Q_PROPERTY(QHostAddress localAddress                READ localAddress)

public:
    QTmRemoteServer(QString USN, QHostAddress localAddress, QUrl remoteServerURL, QUpnpHttpServer *eventingHttpServer);
    ~QTmRemoteServer();

    void                     setTmSpecVersionMajor(int version);
    int                      TmSpecVersionMajor();
    void                     setTmSpecVersionMinor(int version);
    int                      TmSpecVersionMinor();
    void                     setFriendlyName(QString friendlyName);
    QString                  friendlyName();
    void                     setManufacturerName(QString manufacturerName);
    QString                  manufacturerName();
    void                     setManufacturerUrl(QUrl manufacturerUrl);
    QUrl                     manufacturerUrl();
    void                     setModelDescription(QString modelDescription);
    QString                  modelDescription();
    void                     setModelName(QString modelName);
    QString                  modelName();
    void                     setModelNumber(QString modelNumber);
    QString                  modelNumber();
    void                     setModelUrl(QUrl modelUrl);
    QUrl                     modelUrl();
    void                     setSerialNumber(QString serialNumber);
    QString                  serialNumber();
    void                     setUDN(QString UDN);
    QString                  UDN();
    void                     setUPC(QString UPC);
    QString                  UPC();
    QString                  USN();
    void                     setServerBTAddress(QString canServerStartBT);
    QString                  serverBTAddress();
    void                     setCanServerStartBT(bool canStartBT);
    bool                     canServerStartBT();
    void                     setIconList(QList<QTmIcon *> iconList);
    QList<QTmIcon *>         iconList();
    QList<QTmApplication *>  applicationList();
    QTmApplication*          applicationById(QString appId);
    int                      remoteServerId();
    QHostAddress             localAddress();
    QUrl                     remoteServerUrl();

    //TmApplicationServer:1 service actions
    bool                     launchApplication(QTmApplication *application, int profileID = 0);
    bool                     terminateApplication(QTmApplication *application, int profileID = 0);
    QTmGlobal::TmStatus      getApplicationStatus(QTmApplication *application);
    bool                     getApplicationList(QString appListingFilter = "*", int profileID = 0);

    //TmClientProfile:1 service actions
    int                      getMaxNumProfiles();
    QTmClientProfile         getClientProfile(int profileID = 0);
    QTmClientProfile         setClientProfile(QTmClientProfile clientProfile, int profileID = 0);

    //UPnP Event subscription/unsubscription
    int                      subscribeToTmApplicationServerEvents();
    int                      unsubscribeFromTmApplicationServerEvents();
    int                      subscribeToTmClientProfileEvents();
    int                      unsubscribeFromTmClientProfileEvents();

signals:
    void                     applicationLaunched(QTmApplication *application);
    void                     applicationTerminated(QTmApplication *application);
    void                     applicationStatusChanged(QTmApplication *application);
    void                     applicationAdded(QTmApplication *application);
    void                     applicationListingChanged(QTmRemoteServer *remoteServer);

private:
    struct QUpnpService
    {
        QString serviceType; /**< UPnP service type */
        QString serviceId;   /**< Service identifier */
        QUrl scdpURL;        /**< URL for service description */
        QUrl controlUrl;     /**< URL for control */
        QUrl eventSubUrl;    /**< URL for eventing */
        QString sid;         /**< Subscription ID - includes 'uuid:' at the beginning */
    };
    static int newRemoteServerId();
    int soapGetApplicationList(const QUrl &serviceControl, QString appListingFilter, int profileID);
    int soapLaunchApplication(const QUrl &serviceControl, QString appID, int profileID, QString &appUri);
    int soapGetApplicationStatus(const QUrl &serviceControl, QString appID, QTmGlobal::TmStatus *appStatus);
    int soapTerminateApplication(const QUrl &serviceControl, QString appID, int profileID);
    int soapGetMaxNumProfiles(const QUrl &serviceControl, int *maxNumProfiles);
    int soapGetClientProfile(const QUrl &serviceControl, int profileID, QDomDocument *clientProfile);
    int soapSetClientProfile(const QUrl &serviceControl, int profileID, QString clientProfile, QDomDocument *resultProfile);
    int subscribeToUPnPEvent(const QUrl &eventSubUrl, QString &sid);
    int unsubscribeFromUPnPEvent(const QUrl &eventSubUrl, QString &sid);
    bool retrieveIcon(const QUrl &iconPath, QUrl &url);
    int setService(QUpnpService service);
    void clearApplicationList();
    void clearDeviceIconList();
    static bool performHTTPAction(const QUrl &url, const QByteArray &sendBuffer, QByteArray &receiveBuffer);
    static bool performSOAPAction(const QUrl &url, const QString &action, const QString &body, QString &result);
    static QString escapeXML(QString inputXML);
    static QString unescapeXML(QString inputXML);

    virtual void processPropertyChangeEvent(const QString &name, const QString &value);

    QUpnpService             m_tmApplicationServer;               /**< UPnP TmApplicationServer service */
    QUpnpService             m_tmClientProfile;                   /**< UPnP TmClientProfile service */
    QHostAddress             m_localAddress;                      /**< local <ip address> of network interface */
    QUrl                     m_remoteServerUrl;
    int                      m_TmSpecVersionMajor;
    int                      m_TmSpecVersionMinor;
    QString                  m_friendlyName;
    QString                  m_manufacturerName;
    QUrl                     m_manufacturerUrl;
    QString                  m_modelDescription;
    QString                  m_modelName;
    QString                  m_modelNumber;
    QUrl                     m_modelUrl;
    QString                  m_serialNumber;
    QString                  m_UDN;
    QString                  m_UPC;
    QString                  m_USN;
    QString                  m_serverBTAddress;
    bool                     m_canServerStartBT;
    QList<QTmIcon *>         m_iconList;
    int                      m_remoteServerId;
    QList<QTmApplication *>  m_applicationList;
    QMutex                   m_applicationListMutex;
    QUpnpHttpServer         *m_eventingHttpServer;
    QMutex                   m_ctrlMutex;
    friend class             QTmUpnpControlPoint;
};

#endif // QTMREMOTESERVER_H
