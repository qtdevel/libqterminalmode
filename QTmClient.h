/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#ifndef QTMCLIENT_H
#define QTMCLIENT_H

#include <QtCore/QHash>
#include <QtCore/QSignalMapper>

#include "QTmWidget.h"
#include "QTmRfbClient.h"
#include "QTmUpnpControlPoint.h"
#include "QTmNetworkObserver.h"
#include "QTmClientProfile.h"
#include "QTmClientRules.h"
#include "QTmDeclarativeItem.h"

class QTmClient : public QObject {
    Q_OBJECT

public:
                            QTmClient(QObject *parent = 0);
                            ~QTmClient();

    void                    networkStartObserver(bool monitorIPv4 = true, bool monitorIPv6 = false);
    void                    networkStopObserver();
    QNetworkConfiguration::BearerType networkBearerType(QString interfaceName);

    int                     vncStartClient();
    void                    vncConnectClient(QTmApplication *application, int id);
    void                    vncDisconnectClient(int id);
    void                    vncStopClient(int id);
    QWidget                *vncClientWidget(int id);
    void                    setClientDeclarativeItem(QTmDeclarativeItem* item, int id);

    void                    vncSetColorFormat      (QTmGlobal::TmColorFormat colorFormat, int id);
    void                    vncSetRunLengthEncoding(bool enable, int id);
    void                    vncSetIncrementalUpdate(bool enable, int id);
    void                    vncSetPreferredSize(QSize size, bool scaling, int id);
    void                    vncClickVirtualKey     (int keySym,  int id);
    void                    vncBlockFramebuffer(QRect area, unsigned int appId, unsigned int reason, int id);

    bool                    upnpStartControlPoint();
    bool                    upnpConnectControlPoint(QString interfaceName, QHostAddress interface);
    void                    upnpDisconnectControlPoint(QHostAddress hostAddress);
    void                    upnpStopControlPoint();
    bool                    upnpLaunchApplication(QTmApplication *application);
    bool                    upnpTerminateApplication(QTmApplication *application);
    QTmGlobal::TmStatus     upnpGetApplicationStatus(QTmApplication *application);

signals:
    void     networkDeviceDetected(QString interfaceName, QHostAddress ipAddress);
    void     networkDeviceLost(QHostAddress ipAddress);

    void     vncClientResized(QSize size, int id);
    void     vncClientConnected(int id);
    void     vncClientDisconnected(int id);
    void     vncClientDisplayConfiguration(int id);
    void     vncClientEventConfiguration(int id);
    void     vncClientKeyboardTrigger(QPoint point, bool remove, int id);
    void     vncClientKeyEventList(QList<unsigned int> *list, unsigned int counter, int id);
    void     vncClientFbAlternativeText(unsigned int appId, QString text, int id);
    void     vncClientContextInformation(QRect area, int appId, QTmAppInfo *appCategory, QTmDisplayInfo *conCategory, int id);

    void     remoteServerDeviceAdded(QTmRemoteServer *remoteServerDevice);
    void     remoteServerDeviceRemoved(QTmRemoteServer *remoteServerDevice);

private slots:
    void     deviceDetected(QString networkInterfaceName, QNetworkAddressEntry networkEntry);
    void     deviceLost(QNetworkAddressEntry networkEntry);

    // RfbClient signal-wrappers:
    void rfbDesktopSize(QSize framebufferSize);
    void rfbSessionConnected(QString serverName);
    void rfbSessionDisconnected();
    void rfbEventConfiguration();
    void rfbDisplayConfiguration();
    void rfbVirtualKeyboardTrigger(QPoint point, bool remove);
    void rfbKeyEventList(QList<unsigned int> *list, unsigned int counter);
    void rfbFramebufferAlternativeText(unsigned int appId, QString text);
    void rfbContextInformation(QRect area, int appId, QTmAppInfo *appCategory, QTmDisplayInfo *conCategory);
    void rfbDeviceStatus(unsigned int);

private:
    QTmRemoteServer* remoteServerByApplication(QTmApplication *application);
    bool connectRfbClientToSurface(int id);
    void disconnectRfbClientFromSurface(int id);

    QTmUpnpControlPoint            *m_upnpControlPoint;

    QHash<int, QTmRfbClient*>       m_rfbClientMap;
    QHash<int, QTmWidget* >         m_widgetMap;
    QHash<int, QThread* >           m_threadMap;
    QHash<int, QTmDeclarativeItem*> m_declarativeMap;
    int                             m_maxVncId;

    QTmNetworkObserver             *m_networkObserver;
};

#endif // QTMCLIENT_H
