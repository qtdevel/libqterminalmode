/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include <QtCore/QDir>
#include <QtCore/QDebug>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtNetwork/QTcpSocket>

#include "QTmUpnpControlPoint.h"


#ifdef Q_OS_WIN
#include <winsock2.h>
#include <ws2tcpip.h>
#undef interface
#endif

#ifdef Q_OS_LINUX
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <net/if.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <ifaddrs.h>
#include <ctype.h>
#include <unistd.h>
#endif

//Types of UPnP Device Discovery Messages
#define SSDP_ALIVE	1
#define SSDP_BYEBYE	2
#define SSDP_UPDATE	3

//UPnP Terminal Mode
#define UPNP_TM_SERVER_DEVICE_TYPE "urn:schemas-upnp-org:device:TmServerDevice:1"

// Class constructor
QTmUpnpControlPoint::QTmUpnpControlPoint(QObject *parent) :
        QObject(parent),
        m_socket(0)
{
    //Init network access manager
    connect(&m_netManager, SIGNAL(finished(QNetworkReply*)),
            this,          SLOT(readDeviceDescription(QNetworkReply *)),
            Qt::AutoConnection);
}

// Class Destructor
QTmUpnpControlPoint::~QTmUpnpControlPoint()
{
    //Close & Delete all sockets
    m_socketLock.lockForWrite();
    if (m_socket) {
#ifdef QT_DEBUG_TM_UPNP
        qDebug() << "QTmUpnpControlPoint:" << "Stop listening for UPnP service discovery advertisements";
#endif
        m_socket->close();
        disconnect(m_socket, SIGNAL(readyRead()), this, SLOT(readDiscoveryMessage()));
        delete m_socket;
        m_socket = 0;
    }

    QHash<QHostAddress, QTmUpnpControlPointTx*>::iterator i = m_socketMap.begin();
    while (i != m_socketMap.end()) {
        delete i.value();
        i = m_socketMap.erase(i);
    }

#ifdef QT_DEBUG_TM_UPNP
    qDebug() << "QTmUpnpControlPoint:" << "Stop UPnP HTTP Server from receiving event notifications";
#endif
    if (m_eventingHttpServer.isListening())
        m_eventingHttpServer.close();
    m_socketLock.unlock();

    //Clear remote server devices
    m_serverLock.lockForWrite();
    QList<QTmRemoteServer*>::iterator j = m_serverList.begin();
    while (j != m_serverList.end()) {
        QTmRemoteServer *server = *j;
        j = m_serverList.erase(j);
        emit remoteServerDeviceRemoved(server);
        server->deleteLater();
    }
    m_serverLock.unlock();
}

bool QTmUpnpControlPoint::connectUpnp(QString interfaceName, QHostAddress interfaceAddress)
{
    //Create Multi-cast socket to listen for UPnP service discovery advertisements
    if (!m_socket) {
#ifdef QT_DEBUG_TM_UPNP
        qDebug() << "QTmUpnpControlPoint:" << "Start listen for UPnP service discovery advertisements";
#endif
        m_socket = new QUdpSocket();
        if (!m_socket->bind(1900, QUdpSocket::ReuseAddressHint)) {
            qWarning() << "QTmUpnpControlPoint:" << "cannot bind socket to port 1900";
            delete m_socket;
            m_socket = 0;
            return false;
        }
        connect(m_socket, SIGNAL(readyRead()),
                this,     SLOT (readDiscoveryMessage()),
                Qt::AutoConnection);
    }

    //Start HTTP Server of UPnP Control Point
    if (!m_eventingHttpServer.isListening()) {
#ifdef QT_DEBUG_TM_UPNP
        qDebug() << "QTmUpnpControlPoint:" << "Starting UPnP HTTP Server (for receiving event notifications)";
#endif
        if (!m_eventingHttpServer.listen(QHostAddress::Any)) {
            qDebug() << "QTmUpnpControlPoint: cannot start HTTP Server";
            return false;
        }
    }

    //Check if already listening
    if (m_socketMap.contains(interfaceAddress)) {
        qDebug() << "QTmUpnpControlPoint: already listening to" << interfaceName << "(" << interfaceAddress.toString() << ")";
        sendMSearchQuery(interfaceAddress);
        return false;
    }

    //Setup UPnP multicasting
#ifdef QT_DEBUG_TM_UPNP
    qDebug() << "QTmUpnpControlPoint:" << "Starting UPnP Control Point" << "for" << interfaceName << "(" << interfaceAddress.toString() << ")";
#endif
    if (interfaceAddress.protocol() == QAbstractSocket::IPv4Protocol) {
        if(!setupUpnpIPv4(interfaceName, interfaceAddress)) {
            qDebug() << "QTmUpnpControlPoint: cannot start UPnP control point on IPv4 for" << interfaceName << "(" << interfaceAddress.toString() << ")";
            return false;
        }
    } else if (interfaceAddress.protocol() == QAbstractSocket::IPv6Protocol) {
        if(!setupUpnpIPv6(interfaceName, interfaceAddress)) {
            qDebug() << "QTmUpnpControlPoint: cannot start UPnP control point on IPv6 for" << interfaceName << "(" << interfaceAddress.toString() << ")";
            return false;
        }
    } else {
        qDebug() << "QTmUpnpControlPoint: protocol for" << interfaceName << "(" << interfaceAddress.toString() << "not supported";
        return false;
    }

#ifdef QT_DEBUG_TM_UPNP
    qDebug() << "QTmUpnpControlPoint:" << "Started UPnP Control Point for" << interfaceName << "(" << interfaceAddress.toString() << ")";
#endif
    return true;
}

void QTmUpnpControlPoint::disconnectUpnp(QHostAddress interfaceAddress)
{
    //TODO leave multicast group

    //Close socket
    QTmUpnpControlPointTx *tx = m_socketMap.value(interfaceAddress);
    if (tx) {
        m_socketMap.remove(interfaceAddress);
        delete tx;
    }

    //Remove all devices within same network
    m_serverLock.lockForWrite();
    QList<QTmRemoteServer*>::iterator i = m_serverList.begin();
    while (i != m_serverList.end()) {
        QTmRemoteServer *server = *i;
        if (server->localAddress() == interfaceAddress) {
#ifdef QT_DEBUG_TM_UPNP
            qDebug() << "QTmUpnpControlPoint:" << "Removed server device:" << server->USN();
#endif
            //Inform others
            emit remoteServerDeviceRemoved(server);
            //Delete device
            server->deleteLater();
            i = m_serverList.erase(i);
        } else
            i++;
    }
    m_serverLock.unlock();
}

void QTmUpnpControlPoint::readDiscoveryMessage()
{
#ifdef QT_DEBUG_TM_UPNP
//    qDebug() << "QTmUpnpControlPoint:" << "read discovery message";
#endif
    readMessage(m_socket);
}

void QTmUpnpControlPoint::readMessage(QUdpSocket *socket)
{
#ifdef QT_DEBUG_TM_UPNP
   qDebug() << "QTmUpnpControlPoint:" << "read message";
#endif
    while(socket->hasPendingDatagrams()) {
        //Read datagram
        QByteArray datagram;
        datagram.resize(socket->pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;
        socket->readDatagram(datagram.data(), datagram.size(), &sender, &senderPort);

        //Parse datagram
        parseMessage(QString(datagram));
    }
}

void QTmUpnpControlPoint::parseMessage(const QString &buffer)
{
#ifdef QT_DEBUG_TM_UPNP
      //qDebug() << "QTmUpnpControlPoint: Parse discovery message/response:\n" << buffer;
#endif

    //Parse header
    int msgType = 0;
    QString location, USN, devType;
    QStringList lines = buffer.split('\n');
    foreach( QString line, lines) {
        line = line.trimmed();
        if (line.startsWith("NTS:", Qt::CaseInsensitive)) {
            // Check content of NTS header
            if (line.contains("ssdp:alive", Qt::CaseInsensitive)) {
                msgType = SSDP_ALIVE;
            } else if (line.contains("ssdp:byebye", Qt::CaseInsensitive)) {
                msgType = SSDP_BYEBYE;
            } else if (line.contains("ssdp:update", Qt::CaseInsensitive)) {
                msgType = SSDP_UPDATE;
            } else {
                msgType = 0;
                return;
            }
        } else if (line.startsWith("NT:", Qt::CaseInsensitive)) {
            // Check content of NT header
            devType = line.remove("NT:", Qt::CaseInsensitive).trimmed();
        }
        else if (line.startsWith("ST:", Qt::CaseInsensitive)) {
            // Check content of ST header
            devType = line.remove("ST:", Qt::CaseInsensitive).trimmed();
            // If we find ST header then it means it is a response to a M-Search request
            // hence, set msgType equivalent to an SSDP service advertisement
            msgType = SSDP_ALIVE;
        }
        else if (line.startsWith("USN:", Qt::CaseInsensitive)) {
            // Check content of USN header
            USN = line.remove("USN:", Qt::CaseInsensitive).trimmed();
        } else if (line.startsWith("LOCATION:", Qt::CaseInsensitive)) {
            // Get content of Location header
            location = line.remove("LOCATION:", Qt::CaseInsensitive).trimmed();
        }
    }

    //Sanity check
    if (!msgType || USN.isEmpty() || devType.isEmpty())
    {
#ifdef QT_DEBUG_TM_UPNP
        qDebug() << "QTmUpnpControlPoint:" << "Parse message/response failed!";
#endif
        return; // do not proceed any further
    }

    if ((msgType == SSDP_ALIVE || msgType == SSDP_UPDATE) && location.isEmpty())
    {
#ifdef QT_DEBUG_TM_UPNP
        qDebug() << "QTmUpnpControlPoint:" << "Did not found the location entry!";
#endif
        return; // do not proceed any further
    }

    //Check whether the device is supported
    if (devType != UPNP_TM_SERVER_DEVICE_TYPE)
    {
#ifdef QT_DEBUG_TM_UPNP
        qDebug() << "QTmUpnpControlPoint:" << "Unknown device type:" << devType;
#endif
        return; // do nothing
    }

    //Get device if already known
    m_serverLock.lockForRead();
    QTmRemoteServer *dev = remoteServerByUsn(USN);
    m_serverLock.unlock();

    //If discovery message is of right kind, then proceed
    switch(msgType) {
    case SSDP_ALIVE:
        {
            //Check whether the device is already known
            if (dev)
                return; // do nothing
#ifdef QT_DEBUG_TM_UPNP
            qDebug() << "QTmUpnpControlPoint:" << "SSDP advertisement/M-Search response received, from" << USN << "type:" << devType << "location:" << location;
#endif
            //Retrieve device description
            m_netManager.get(QNetworkRequest(QUrl(location)));

            break;
        }

    case SSDP_BYEBYE:
        {
            if (!dev)
                return; // do nothing
#ifdef QT_DEBUG_TM_UPNP
            qDebug() << "QTmUpnpControlPoint:" << "SSDP byebye received, from" << USN;
#endif
            //Remove from device list
            m_serverLock.lockForWrite();
            m_serverList.removeAll(dev);
            m_serverLock.unlock();
#ifdef QT_DEBUG_TM_UPNP
            qDebug() << "QTmUpnpControlPoint:" << "Removed server device:" << dev->USN();
#endif

            //Inform others
            emit remoteServerDeviceRemoved(dev);

            //Delete device
            dev->deleteLater();

            break;
        }

    case SSDP_UPDATE:
        {
#ifdef QT_DEBUG_TM_UPNP
            qDebug() << "QTmUpnpControlPoint:" << "SSDP update received, from" << USN << "type:" << devType;
#endif
            //TODO SSDP_UPDATE

            break;
        }
    }
}

void QTmUpnpControlPoint::readDeviceDescription(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "QTmUpnpControlPoint:" << "Error retrieving device description";
        return;
    }

    QUrl url = reply->url();
#ifdef QT_DEBUG_TM_UPNP
    qDebug() << "QTmUpnpControlPoint:" << "Parsing UPnP device description from location:" << url.path();
#endif

    QDomDocument doc;
    //Parse XML into struct for processing
    if (!doc.setContent(reply)) {
        qDebug() << "QTmUpnpControlPoint:" << "XML format error";
        return;
    }

    QDomElement specVersionElement = doc.documentElement().firstChildElement("specVersion");
    QDomElement minorVersionElement;
    QDomElement majorVersionElement;
    if(!specVersionElement.isNull())
    {
        minorVersionElement = specVersionElement.firstChildElement("minor");
        majorVersionElement = specVersionElement.firstChildElement("major");
    }

    QDomElement urlBaseElement = doc.documentElement().firstChildElement("URLBase");
    if(!urlBaseElement.isNull())
        url = QUrl(urlBaseElement.text());

    QDomNodeList deviceNodes = doc.elementsByTagName("device");
    if (deviceNodes.isEmpty()) {
#ifdef QT_DEBUG_TM_UPNP
        qDebug() << "QTmUpnpControlPoint:" << "Error: No device found within root device description.";
#endif
        return;
    }

    QHostAddress localAddress = getLocalAddress(QHostAddress(url.host()));
    for(int i = 0; ; i++) {
        QDomNode deviceNode = deviceNodes.at(i);
        if(deviceNode.isNull())
            break;

        //Get device type & id
        QDomElement devTypeElement = deviceNode.firstChildElement("deviceType");
        QDomElement udnElement = deviceNode.firstChildElement("UDN");
        if (udnElement.isNull() || devTypeElement.isNull())
            continue;
        QString USN = udnElement.text() + "::" + devTypeElement.text();

        //Check whether the device is supported
        if (devTypeElement.text() != UPNP_TM_SERVER_DEVICE_TYPE)
            continue;

        //Check whether server device
        m_serverLock.lockForWrite();
        QTmRemoteServer *tmServerDevice = remoteServerByUsn(USN);
        if (tmServerDevice) {
            m_serverLock.unlock();
            continue; // server device already known
        }

#ifdef QT_DEBUG_TM_UPNP
        qDebug() << "QTmUpnpControlPoint:" << "New server device:" << USN;
#endif
        //Init new server device
        tmServerDevice = new QTmRemoteServer(USN, localAddress, url, &m_eventingHttpServer);
        tmServerDevice->setUDN(udnElement.text());

        QDomElement friendlyNameElement = deviceNode.firstChildElement("friendlyName");
        QDomElement manufacturerNameElement = deviceNode.firstChildElement("manufacturer");
        QDomElement manufacturerUrlElement = deviceNode.firstChildElement("manufacturerURL");
        QDomElement modelNameElement = deviceNode.firstChildElement("modelName");
        QDomElement modelDescriptionElement = deviceNode.firstChildElement("modelDescription");
        QDomElement modelNumberElement = deviceNode.firstChildElement("modelNumber");
        QDomElement modelUrlElement = deviceNode.firstChildElement("modelURL");
        QDomElement serialNumberElement = deviceNode.firstChildElement("serialNumber");
        QDomElement upcElement = deviceNode.firstChildElement("UPC");

        if(!minorVersionElement.isNull())
            tmServerDevice->setTmSpecVersionMinor(minorVersionElement.text().toInt());

        if(!majorVersionElement.isNull())
            tmServerDevice->setTmSpecVersionMinor(majorVersionElement.text().toInt());

        if(!friendlyNameElement.isNull())
            tmServerDevice->setFriendlyName(friendlyNameElement.text());

        if(!manufacturerNameElement.isNull())
            tmServerDevice->setManufacturerName(manufacturerNameElement.text());

        if(!manufacturerUrlElement.isNull())
            tmServerDevice->setManufacturerUrl(QUrl(manufacturerUrlElement.text()));

        if(!modelNameElement.isNull())
            tmServerDevice->setModelName(modelNameElement.text());

        if(!modelDescriptionElement.isNull())
            tmServerDevice->setModelDescription(modelDescriptionElement.text());

        if(!modelNumberElement.isNull())
            tmServerDevice->setModelNumber(modelNumberElement.text());

        if(!modelUrlElement.isNull())
            tmServerDevice->setModelUrl(QUrl(modelUrlElement.text()));

        if(!serialNumberElement.isNull())
            tmServerDevice->setSerialNumber(serialNumberElement.text());

        if(!upcElement.isNull())
            tmServerDevice->setUPC(upcElement.text());

        QList<QTmIcon *> iconList;
        QDomElement iconListElement = deviceNode.firstChildElement("iconList");
        if(!iconListElement.isNull())
        {
            QDomElement iconElement = iconListElement.firstChildElement("icon");
            if(!iconElement.isNull())
            {
                QDomElement iconUrlElement = iconElement.firstChildElement("url");
                QDomElement iconMimeTypeElement = iconElement.firstChildElement("mimetype");
                QDomElement iconHeightElement = iconElement.firstChildElement("height");
                QDomElement iconWidthElement = iconElement.firstChildElement("width");
                QDomElement iconDepthElement = iconElement.firstChildElement("depth");
                QTmIcon *icon = new QTmIcon();
                if(!iconWidthElement.isNull())
                    icon->setWidth(iconWidthElement.text().toInt());
                if(!iconHeightElement.isNull())
                    icon->setHeight(iconHeightElement.text().toInt());
                if(!iconDepthElement.isNull())
                    icon->setDepth(iconDepthElement.text().toInt());
                if(!iconMimeTypeElement.isNull())
                    icon->setMimeType(iconMimeTypeElement.text());
                if(!iconUrlElement.isNull())
                {
                    //Retrieve and cache the application icon
                    QUrl cacheUrl;
                    QUrl iconUrl = url.resolved(QUrl(iconUrlElement.text()));
                    if (tmServerDevice->retrieveIcon(iconUrl, cacheUrl))
                        icon->setUrl(cacheUrl);
                    else
                        icon->setUrl(iconUrl);
                }
                tmServerDevice->iconList().append(icon);
            }
        }
        tmServerDevice->setIconList(iconList);

        QDomElement connElement = deviceNode.firstChildElement("X_connectivity");
        QDomElement btElement, bdAddrElement, startConnElement;
        if(!connElement.isNull())
            btElement = connElement.firstChildElement("bluetooth");
        if(!btElement.isNull())
        {
            bdAddrElement = btElement.firstChildElement("bdAddr");
            startConnElement = btElement.firstChildElement("startConnection");
        }

        if(!bdAddrElement.isNull())
            tmServerDevice->setServerBTAddress(bdAddrElement.text());
        if(!startConnElement.isNull())
        {
            if(!startConnElement.text().compare("true", Qt::CaseInsensitive))
                tmServerDevice->setCanServerStartBT(true);
            else
                tmServerDevice->setCanServerStartBT(false);
        }

        //Parse services
        QDomElement serviceListElement = deviceNode.firstChildElement("serviceList");
        if (serviceListElement.isNull() || serviceListElement.parentNode() != deviceNode) {
            qDebug() << "QTmUpnpControlPoint:" << "Error: No service list found within root device description.";
            delete tmServerDevice;
            m_serverLock.unlock();
            continue;
        }
        QDomNodeList serviceNodes = serviceListElement.elementsByTagName("service");
        if (serviceNodes.isEmpty()) {
            qDebug() << "QTmUpnpControlPoint:" << "Error: No services found within service list.";
            delete tmServerDevice;
            m_serverLock.unlock();
            continue;
        }

        for (int j = 0; ; j++) {
            //Parse service
            QDomNode serviceNode = serviceNodes.at(j);
            if (serviceNode.isNull())
                break;

            QDomElement serviceTypeElement = serviceNode.firstChildElement("serviceType");
            QDomElement serviceIdElement   = serviceNode.firstChildElement("serviceId");
            QDomElement scpdUrlElement     = serviceNode.firstChildElement("SCPDURL");
            QDomElement controlUrlElement  = serviceNode.firstChildElement("controlURL");
            QDomElement eventSubUrlElement = serviceNode.firstChildElement("eventSubURL");
            //Check required elements
            if (serviceTypeElement.isNull() || serviceIdElement.isNull() || scpdUrlElement.isNull() || controlUrlElement.isNull())
                continue;

            //TODO QUrl::resolved ( const QUrl & relative ) const
            QTmRemoteServer::QUpnpService service;
            service.serviceType     = serviceTypeElement.text();
            service.serviceId       = serviceIdElement.text();
            service.scdpURL         = url.resolved(QUrl(scpdUrlElement.text()));
            service.controlUrl      = url.resolved(QUrl(controlUrlElement.text()));
            if (!eventSubUrlElement.isNull())
                service.eventSubUrl = url.resolved(QUrl(eventSubUrlElement.text()));
#ifdef QT_DEBUG_TM_UPNP
            qDebug() << "QTmUpnpControlPoint:" << "Retrieving Service details...";
            qDebug() << "QTmUpnpControlPoint:" << "serviceType:" << service.serviceType;
            qDebug() << "QTmUpnpControlPoint:" << "serviceId:" << service.serviceId;
            qDebug() << "QTmUpnpControlPoint:" << "SCPDURL:" << service.scdpURL.toString();
            qDebug() << "QTmUpnpControlPoint:" << "controlURL:" << service.controlUrl.toString();
            qDebug() << "QTmUpnpControlPoint:" << "eventSubURL:" << service.eventSubUrl.toString();
#endif
            //Store service
            tmServerDevice->setService(service);
        }

        //TODO Verifying UPnP Service is Terminal Mode compliant

        //Add to server device list
        m_serverList.append(tmServerDevice);
        m_serverLock.unlock();
#ifdef QT_DEBUG_TM_UPNP
        qDebug() << "QTmUpnpControlPoint:" << "Added server device:" << tmServerDevice->USN();
#endif

        //Inform others
        emit remoteServerDeviceAdded(tmServerDevice);

        //TODO to be moved to TmClient
        //Subscribe to events from TmApplicationServer:1 service
#ifdef QT_DEBUG_TM_UPNP
        qDebug() << "QTmUpnpControlPoint:" << "Subscribing to Events for TmApplicationServer:1 service";
#endif
        if(!tmServerDevice->subscribeToTmApplicationServerEvents())
            qDebug() << "QTmUpnpControlPoint:" << "Error subscribing to events for TmApplicationServer:1 service";


        //Subscribe to events from TmClientProfile:1 service
#ifdef QT_DEBUG_TM_UPNP
        qDebug() << "QTmUpnpControlPoint:" << "Subscribing to Events for TmClientProfile:1 service";
#endif
        if(!tmServerDevice->subscribeToTmClientProfileEvents())
            qDebug() << "QTmUpnpControlPoint:" << "Error subscribing to events for TmClientProfile:1 service";
    }
}

// Set up multi-cast listener to listen for UPnP SSDP advertisements
// Returns 1 for success, 0 for failure
int QTmUpnpControlPoint::setupUpnpIPv4(QString interfaceName, QHostAddress interfaceAddress)
{
    qDebug() << "QTmUpnpControlPoint:" << "Setup Upnp IPv4 on" << interfaceName << "(" << interfaceAddress.toString() << ")";

    //Join UPnP Device Discovery multicast group
    struct ip_mreq group;
    memset(&group, 0, sizeof(group));
    group.imr_multiaddr.s_addr = inet_addr("239.255.255.250");
    group.imr_interface.s_addr = inet_addr(interfaceAddress.toString().toAscii().constData());

    qDebug() << "QTmUpnpControlPoint:" << "Interface Address is:" << inet_ntoa(group.imr_interface);

    int err = setsockopt(m_socket->socketDescriptor(), IPPROTO_IP, IP_ADD_MEMBERSHIP, (const char *) &group, sizeof(group));
    if (err < 0) {
        qWarning() << "QTmUpnpControlPoint:" << "Adding multicast group error (" << err << ")";
        perror("QTmUpnpControlPoint: IP_ADD_MEMBERSHIP");
        return 0;
    }

    //Create Multi-cast socket to send UPnP M-SEARCH multicast queries
    QUdpSocket *socketTx = new QUdpSocket();
    if (!socketTx->bind()) {
            qWarning() << "QTmUpnpControlPoint:" << "Cannot bind socket to send UPnP M-SEARCH multicast queries";
            delete socketTx;
            return 0;
    }

    //Disable MC loopback
    char loopch = 0;
    err = setsockopt(socketTx->socketDescriptor(), IPPROTO_IP, IP_MULTICAST_LOOP, (char *)&loopch, sizeof(loopch));
    if (err < 0) {
         qWarning() << "QTmUpnpControlPoint:" << "Disabling multicast loopback error (" << err << ")";
         perror("QTmUpnpControlPoint: IP_MULTICAST_LOOP");
         socketTx->close();
         delete socketTx;
         return 0;
    }

    //Set local interface for outbound multicast datagrams
    //The IP address specified must be associated with the preferred interface
    struct in_addr localInterface;
    memset(&localInterface, 0, sizeof(localInterface));
    localInterface.s_addr = inet_addr(interfaceAddress.toString().toAscii().constData());
    err = setsockopt(socketTx->socketDescriptor(), IPPROTO_IP, IP_MULTICAST_IF, (char *)&localInterface, sizeof(localInterface));
    if (err < 0) {
         qWarning() << "QTmUpnpControlPoint:" << "Error while setting local network interface (" << err << ")";
         perror("QTmUpnpControlPoint: IP_MULTICAST_IF");
         socketTx->close();
         delete socketTx;
         return 0;
    }
#ifdef QT_DEBUG_TM_UPNP
      else
         qDebug() << "QTmUpnpControlPoint:" << "Setting the local interface for outbound datagrams on: " << interfaceAddress.toString() << "...OK.\n";
#endif

    //Add to transceiver list
    m_socketMap.insert(interfaceAddress, new QTmUpnpControlPointTx(this, socketTx));

    return 1;
}

// Set up multi-cast listener to listen for UPnP SSDP advertisements
// Returns 1 for success, 0 for failure
int QTmUpnpControlPoint::setupUpnpIPv6(QString interfaceName, QHostAddress interfaceAddress)
{
    qDebug() << "QTmUpnpControlPoint:" << "Setup Upnp IPv6 on" << interfaceName << "(" << interfaceAddress.toString() << ")";

    //Join UPnP Device Discovery multicast group
    struct ipv6_mreq group;
    memset(&group, 0, sizeof(group));
    inet_pton(AF_INET6, (const char*)"FF02::C", group.ipv6mr_multiaddr.s6_addr);

    //Obtain index associated with network interface
    group.ipv6mr_interface = if_nametoindex(interfaceName.toAscii().constData());
    //If index cannot be found, exit
    if(group.ipv6mr_interface == 0)
        return 0;

    qDebug() << "QTmUpnpControlPoint:" << "Interface Address is:" << interfaceAddress.toString();

    int err = setsockopt(m_socket->socketDescriptor(), IPPROTO_IPV6, IPV6_JOIN_GROUP, (const char *) &group, sizeof(group));
    if (err < 0) {
        qWarning() << "QTmUpnpControlPoint:" << "Adding IPv6 multicast group error (" << err << ")";
        perror("QTmUpnpControlPoint: IPV6_JOIN_GROUP");
        return 0;
    }

    //Create Multi-cast socket to send UPnP M-SEARCH multicast queries
    QUdpSocket *socketTx = new QUdpSocket();
    if (!socketTx->bind()) {
            qWarning() << "QTmUpnpControlPoint:" << "Cannot bind socket to send UPnP M-SEARCH multicast queries";
            delete socketTx;
            return 0;
    }

    //Disable MC loopback
    char loopch = 0;
    err = setsockopt(socketTx->socketDescriptor(), IPPROTO_IPV6, IPV6_MULTICAST_LOOP, (char *)&loopch, sizeof(loopch));
    if (err < 0) {
         qWarning() << "QTmUpnpControlPoint:" << "Disabling IPv6 multicast loopback error (" << err << ")";
         perror("QTmUpnpControlPoint: IPV6_MULTICAST_LOOP");
         socketTx->close();
         delete socketTx;
         return 0;
    }

    //Set local interface for outbound multicast datagrams
    //The IP address specified must be associated with the preferred interface
    struct in6_addr localInterface;
    memset(&localInterface, 0, sizeof(localInterface));
    inet_pton(AF_INET6, interfaceAddress.toString().toAscii().constData(), localInterface.s6_addr);
    err = setsockopt(socketTx->socketDescriptor(), IPPROTO_IPV6, IPV6_MULTICAST_IF, (char *)&localInterface, sizeof(localInterface));
    if (err < 0) {
         qWarning() << "QTmUpnpControlPoint:" << "Error while setting IPv6 local network interface (" << err << ")";
         perror("QTmUpnpControlPoint: IPV6_MULTICAST_IF");
         socketTx->close();
         delete socketTx;
         return 0;
    }
#ifdef QT_DEBUG_TM_UPNP
      else
         qDebug() << "QTmUpnpControlPoint:" << "Setting the local interface for outbound IPv6 datagrams on: " << interfaceAddress.toString() << "...OK.\n";
#endif

    //Add to transceiver list
    m_socketMap.insert(interfaceAddress, new QTmUpnpControlPointTx(this, socketTx));

    return 1;
}

QTmRemoteServer* QTmUpnpControlPoint::remoteServerById(int remoteServerId)
{
    QWriteLocker wLocker(&m_serverLock);
    foreach (QTmRemoteServer *server, m_serverList) {
        if (remoteServerId == server->remoteServerId())
            return server;
    }
    return 0;
}

QTmRemoteServer* QTmUpnpControlPoint::remoteServerByUsn(const QString &USN)
{
    foreach (QTmRemoteServer *server, m_serverList) {
        if (USN == server->USN()) {
            return server;
        }
    }
    return 0;
}

// Send M-Search Query to discover Terminal Mode UPnP Services
bool QTmUpnpControlPoint::sendMSearchQuery(QHostAddress interfaceAddress)
{
    qDebug() << "QTmUpnpControlPoint: Sending M-Search query for Terminal Mode UPnP services.";
    QWriteLocker wLocker(&m_socketLock);
    QTmUpnpControlPointTx *tx = m_socketMap.value(interfaceAddress);
    return tx ? tx->sendMSearchQuery() : 0;
}

QHostAddress QTmUpnpControlPoint::getLocalAddress(QHostAddress address)
{
    QList<QNetworkInterface> interfaces = QNetworkInterface::allInterfaces();
    foreach (QNetworkInterface interface, interfaces) {
        QList<QNetworkAddressEntry> entries = interface.addressEntries();
        foreach (QNetworkAddressEntry entry, entries) {
            if (address.isInSubnet(entry.ip(), entry.prefixLength()))
                return entry.ip();
        }
    }
    return QHostAddress();
}

/******************************************************************************/

QTmUpnpControlPointTx::QTmUpnpControlPointTx(QTmUpnpControlPoint *controlPoint, QUdpSocket *socket)
    : QObject(controlPoint)
    , m_controlPoint(controlPoint)
    , m_socketS(socket)
{
    if(m_socketS->localAddress().protocol() == QAbstractSocket::IPv4Protocol)
    {
        //Initialize the group sockaddr structure with
        //SSDP group IPv4 address of 239.255.255.250 and port 1900
        memset((char *) &groupSock, 0, sizeof(groupSock));
        groupSock.sin_family = AF_INET;
        groupSock.sin_addr.s_addr = inet_addr("239.255.255.250");
        //inet_aton((const char*)"239.255.255.250", &groupSock.sin_addr);
        groupSock.sin_port = htons(1900);
    }
    else if(m_socketS->localAddress().protocol() == QAbstractSocket::IPv6Protocol)
    {
        //Initialize the group sockaddr structure with
        //SSDP group IPv6 address of FF02::C and port 1900
        memset((char *) &groupSock6, 0, sizeof(groupSock6));
        groupSock6.sin6_family = AF_INET6;
        inet_pton(AF_INET6, (const char*)"FF02::C", groupSock6.sin6_addr.s6_addr);
        groupSock6.sin6_flowinfo = 0;
        groupSock6.sin6_port = htons(1900);
    }
    connect(m_socketS, SIGNAL(readyRead()),
            this,      SLOT  (readUnicastResponse()),
            Qt::AutoConnection);
}

QTmUpnpControlPointTx::~QTmUpnpControlPointTx()
{
    m_socketS->close();
    disconnect(m_socketS, SIGNAL(readyRead()), this, SLOT(readUnicastResponse()));
    delete m_socketS;
}


void QTmUpnpControlPointTx::readUnicastResponse()
{
#ifdef QT_DEBUG_TM_UPNP
    qDebug() << "QTmUpnpControlPoint:" << "read unicast message";
#endif
    m_controlPoint->readMessage(m_socketS);
}

bool QTmUpnpControlPointTx::sendMSearchQuery()
{
    //Send multicast M-SEARCH query
    if(m_socketS->localAddress().protocol() == QAbstractSocket::IPv4Protocol)
    {
        char *M_SEARCH_QUERY = (char*)"M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nMAN: \"ssdp:discover\"\r\nMX: 1\r\nST: urn:schemas-upnp-org:device:TmServerDevice:1\r\n\r\n";
        //char *M_SEARCH_QUERY = (char*)"M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nMAN: \"ssdp:discover\"\r\nMX: 1\r\nST: urn:schemas-upnp-org:service:TmApplicationServer:1\r\n\r\n";
        //char *M_SEARCH_QUERY = (char*)"M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nMAN: \"ssdp:discover\"\r\nMX: 1\r\nST: urn:schemas-upnp-org:service:TmClientProfile:1\r\n\r\n";
        //char *M_SEARCH_QUERY = (char*)"M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nMAN: \"ssdp:discover\"\r\nMX: 1\r\nST: ssdp:all\r\n\r\n";
        sendto(m_socketS->socketDescriptor(), M_SEARCH_QUERY, strlen(M_SEARCH_QUERY), 0, (struct sockaddr*)&groupSock, sizeof(groupSock));
    }
    else if(m_socketS->localAddress().protocol() == QAbstractSocket::IPv6Protocol)
    {
        char *M_SEARCH_QUERY = (char*)"M-SEARCH * HTTP/1.1\r\nHOST: FF02::C:1900\r\nMAN: \"ssdp:discover\"\r\nMX: 1\r\nST: urn:schemas-upnp-org:device:TmServerDevice:1\r\n\r\n";
        //char *M_SEARCH_QUERY = (char*)"M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nMAN: \"ssdp:discover\"\r\nMX: 1\r\nST: urn:schemas-upnp-org:service:TmApplicationServer:1\r\n\r\n";
        //char *M_SEARCH_QUERY = (char*)"M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nMAN: \"ssdp:discover\"\r\nMX: 1\r\nST: urn:schemas-upnp-org:service:TmClientProfile:1\r\n\r\n";
        //char *M_SEARCH_QUERY = (char*)"M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nMAN: \"ssdp:discover\"\r\nMX: 1\r\nST: ssdp:all\r\n\r\n";
        sendto(m_socketS->socketDescriptor(), M_SEARCH_QUERY, strlen(M_SEARCH_QUERY), 0, (struct sockaddr*)&groupSock6, sizeof(groupSock6));
    }
    else
    {
        return false;
    }
    return true;
}
