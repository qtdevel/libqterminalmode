/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#ifndef QTMCLIENTRULES_H
#define QTMCLIENTRULES_H
#include <QHash>
#include "QTmGlobal.h"

class QTmClientRules {

 public:
    QTmClientRules();
    ~QTmClientRules();
    QString                   getRule(int ruleId);
    void                      setRule(int ruleId, QString ruleValue);
    void                      deleteRule(int ruleId);
    QHash<int, QString>       getAllRules();

 private:
    QHash<int, QString>       m_rulesHash;
};

#endif // QTMCLIENTRULES_H
