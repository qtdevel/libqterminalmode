/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QTmRemoteServer.h"

QTmRemoteServer::QTmRemoteServer(QString USN, QHostAddress localAddress, QUrl remoteServerURL, QUpnpHttpServer *eventingHttpServer) :
        m_localAddress(localAddress),
        m_remoteServerUrl(remoteServerURL),
        m_USN(USN),
        m_iconList(QList<QTmIcon *>()),
        m_remoteServerId(QTmRemoteServer::newRemoteServerId()),
        m_applicationList(QList<QTmApplication *>()),
        m_eventingHttpServer(eventingHttpServer) {
}

QTmRemoteServer::~QTmRemoteServer() {
    //Clear application list
    clearApplicationList();

    //Clear device icon list
    clearDeviceIconList();

    //Unsubscribe from eventing
    if (m_eventingHttpServer) {
        m_eventingHttpServer->removePropertyChangeListener(m_tmApplicationServer.sid);
        m_eventingHttpServer->removePropertyChangeListener(m_tmClientProfile.sid);
    }
}

void QTmRemoteServer::setTmSpecVersionMajor(int version) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "TM major version" << version;
#endif
    m_TmSpecVersionMajor = version;
}

int  QTmRemoteServer::TmSpecVersionMajor() {
    return m_TmSpecVersionMajor;
}

void QTmRemoteServer::setTmSpecVersionMinor(int version) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "TM minor version" << version;
#endif
    m_TmSpecVersionMinor = version;
}

int QTmRemoteServer::TmSpecVersionMinor() {
    return m_TmSpecVersionMinor;
}

void QTmRemoteServer::setFriendlyName(QString friendlyName) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Friendly name" << friendlyName;
#endif
    m_friendlyName = friendlyName;
}

QString QTmRemoteServer::friendlyName() {
    return m_friendlyName;
}

void QTmRemoteServer::setManufacturerName(QString manufacturerName) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Manufacturer Name" << manufacturerName;
#endif
    m_manufacturerName = manufacturerName;
}

QString QTmRemoteServer::manufacturerName() {
    return m_manufacturerName;
}

void QTmRemoteServer::setManufacturerUrl(QUrl manufacturerUrl) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Manufacturer Url" << manufacturerUrl;
#endif
   m_manufacturerUrl = manufacturerUrl;
}

QUrl QTmRemoteServer::manufacturerUrl() {
    return m_manufacturerUrl;
}

void QTmRemoteServer::setModelDescription(QString modelDescription) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Model Description" << modelDescription;
#endif
    m_modelDescription = modelDescription;
}

QString QTmRemoteServer::modelDescription() {
    return m_modelDescription;
}

void QTmRemoteServer::setModelName(QString modelName) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Model Name" << modelName;
#endif
    m_modelName = modelName;
}

QString QTmRemoteServer::modelName() {
    return m_modelName;
}

void QTmRemoteServer::setModelNumber(QString modelNumber) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Model Number" << modelNumber;
#endif
    m_modelNumber = modelNumber;
}

QString QTmRemoteServer::modelNumber() {
    return m_modelNumber;
}

void QTmRemoteServer::setModelUrl(QUrl modelUrl) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Model Url" << modelUrl;
#endif
    m_modelUrl = modelUrl;
}

QUrl QTmRemoteServer::modelUrl() {
    return m_modelUrl;
}

void QTmRemoteServer::setSerialNumber(QString serialNumber) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Serial Number" << serialNumber;
#endif
    m_serialNumber = serialNumber;
}

QString QTmRemoteServer::serialNumber() {
    return m_serialNumber;
}

void QTmRemoteServer::setUDN(QString UDN) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "UDN" << UDN;
#endif
    m_UDN = UDN;
}

QString QTmRemoteServer::UDN() {
    return m_UDN;
}

void QTmRemoteServer::setUPC(QString UPC) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "UPC" << UPC;
#endif
    m_UPC = UPC;
}

QString QTmRemoteServer::UPC() {
    return m_UPC;
}

void QTmRemoteServer::setServerBTAddress(QString serverBTAddress) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Server BT Address" << serverBTAddress;
#endif
    m_serverBTAddress = serverBTAddress;
}

QString QTmRemoteServer::serverBTAddress() {
    return m_serverBTAddress;
}

void QTmRemoteServer::setCanServerStartBT(bool canServerStartBT) {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Can Server Start BT" << canServerStartBT;
#endif
    m_canServerStartBT = canServerStartBT;
}

bool QTmRemoteServer::canServerStartBT() {
    return m_canServerStartBT;
}

void QTmRemoteServer::setIconList(QList<QTmIcon *> iconList) {
    m_iconList = iconList;
}

QList<QTmIcon *> QTmRemoteServer::iconList() {
    return m_iconList;
}

int QTmRemoteServer::remoteServerId() {
    return m_remoteServerId;
}

QHostAddress QTmRemoteServer::localAddress()
{
    return m_localAddress;
}

QUrl QTmRemoteServer::remoteServerUrl()
{
    return m_remoteServerUrl;
}

// Fill in the TM UPnP service structures
// Returns @c 1 for success, @c 0 for failure
int QTmRemoteServer::setService(QUpnpService service)
{
       if (service.serviceType == UPNP_APP_SERVER_SERVICE)
       {
           m_tmApplicationServer.serviceType = UPNP_APP_SERVER_SERVICE;
           m_tmApplicationServer.controlUrl = service.controlUrl;
           m_tmApplicationServer.eventSubUrl = service.eventSubUrl;
           m_tmApplicationServer.scdpURL = service.scdpURL;
           m_tmApplicationServer.serviceId = service.serviceId;
           return 1;
       } else if (service.serviceType == UPNP_CLIENT_PROFILE_SERVICE)
       {
           m_tmClientProfile.serviceType = UPNP_CLIENT_PROFILE_SERVICE;
           m_tmClientProfile.controlUrl = service.controlUrl;
           m_tmClientProfile.eventSubUrl = service.eventSubUrl;
           m_tmClientProfile.scdpURL = service.scdpURL;
           m_tmClientProfile.serviceId = service.serviceId;
           return 1;
       } else
           return 0;
}

// Get the cached application list which was previously obtained from TmApplicationServer:1 service
// Returns @c QList<QTmApplication *> containing list of applications each represented by a QTmApplication object
QList<QTmApplication *> QTmRemoteServer::applicationList()
{
    return m_applicationList;
}

bool QTmRemoteServer::getApplicationList(QString appListingFilter, int profileID)
{
    m_ctrlMutex.lock();
    int ret = soapGetApplicationList(m_tmApplicationServer.controlUrl, appListingFilter, profileID);
    m_ctrlMutex.unlock();
    return ret != 0;
}

// Clear application list after deleting all QTmApplication objects from memory
void QTmRemoteServer::clearApplicationList()
{
    int i;
    QTmApplication *appRef;
    m_applicationListMutex.lock();
    for(i = 0; i < m_applicationList.length(); i++)
    {
        appRef = (QTmApplication*)m_applicationList.at(i);
        delete appRef;
    }
    m_applicationList.clear();
    m_applicationListMutex.unlock();

    emit applicationListingChanged(this);
}

// Clear list of Remote Server Device icons
void QTmRemoteServer::clearDeviceIconList()
{
    while (!m_iconList.isEmpty())
    {
        delete m_iconList.first();
        m_iconList.removeFirst();
    }
}

// Sends the GetApplicationList SOAP action to the URL specified in \a serviceControl.
// Returns 1 for success, 0 for failure
int QTmRemoteServer::soapGetApplicationList(const QUrl &serviceControl, QString appListingFilter, int profileID)
{
    //SOAP Methods invocation for GetCompatibleUIs call for RemoteUIServer:1 UPnP service
    QString body = QString("<?xml version=\"1.0\"?>"
                           "<s:Envelope"
                           " xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\""
                           " s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                           "<s:Body>"
                           "<u:GetApplicationList xmlns:u=\"urn:schemas-upnp-org:service:TmApplicationServer:1\">"
                           "<AppListingFilter>%1</AppListingFilter>"
                           "<ProfileID>%2</ProfileID>"
                           "</u:GetApplicationList>"
                           "</s:Body>"
                           "</s:Envelope>\r\n")
            .arg(appListingFilter)
            .arg(profileID);

    QString result;

    //Send to HTTP server and get back results
    if(!performSOAPAction(serviceControl, "urn:schemas-upnp-org:service:TmApplicationServer:1#GetApplicationList", body, result))
        return 0;

    //Parse SOAP response XML into struct for processing
    QDomDocument doc;

    if (!doc.setContent(result)) {
        qWarning() << "QTmRemoteServer:" << "HTTP Server sent invalid SOAP response!";
        return 0;
    }


    QDomElement bodyElement = doc.documentElement().firstChildElement();
    QDomElement appListResponse = bodyElement.firstChildElement();
    QDomElement appListing = appListResponse.firstChildElement("AppListing");
    QDomElement appList = appListing.firstChildElement("appList");

    int appCount = 0;

    //Clear old application list
    clearApplicationList();

    //Retrieve remote app records
    for(QDomElement appElement = appList.firstChildElement("app"); !appElement.isNull(); appElement = appElement.nextSiblingElement("app"))
    {
        //Retrieve remote app details
        QDomElement appIDElement = appElement.firstChildElement("appID");
        QDomElement appNameElement = appElement.firstChildElement("name");
        QDomElement appDescriptionElement = appElement.firstChildElement("description");
        QDomElement variantElement = appElement.firstChildElement("variant");
        QDomElement providerNameElement = appElement.firstChildElement("providerName");
        QDomElement providerURLElement = appElement.firstChildElement("providerURL");
        QDomElement remotingInfoElement = appElement.firstChildElement("remotingInfo");
        QDomElement protocolIDElement = remotingInfoElement.firstChildElement("protocolID");
        QDomElement formatElement = remotingInfoElement.firstChildElement("format");
        QDomElement directionElement = remotingInfoElement.firstChildElement("direction");
        QDomElement allowedProfileIDElement = appElement.firstChildElement("allowedProfileIDs");
        QDomElement appCertificateUrlElement = appElement.firstChildElement("appCertificateURL");
        QDomElement signatureElement = appElement.firstChildElement("Signature");

        if(appIDElement.isNull() || appNameElement.isNull())
            continue;

        QTmApplication *application = new QTmApplication(m_remoteServerId, appIDElement.text(), !signatureElement.isNull(), false);
        application->setName(appNameElement.text());

        QList<QTmIcon *> iconList;
        QDomElement iconListElement = appElement.firstChildElement("iconList");
        if(!iconListElement.isNull())
        {
            QDomElement iconElement = iconListElement.firstChildElement("icon");
            if(!iconElement.isNull())
            {
                QDomElement iconUrlElement = iconElement.firstChildElement("url");
                QDomElement iconMimeTypeElement = iconElement.firstChildElement("mimetype");
                QDomElement iconHeightElement = iconElement.firstChildElement("height");
                QDomElement iconWidthElement = iconElement.firstChildElement("width");
                QDomElement iconDepthElement = iconElement.firstChildElement("depth");
                QTmIcon *icon = new QTmIcon();
                if(!iconWidthElement.isNull())
                    icon->setWidth(iconWidthElement.text().toInt());
                if(!iconHeightElement.isNull())
                    icon->setHeight(iconHeightElement.text().toInt());
                if(!iconDepthElement.isNull())
                    icon->setDepth(iconDepthElement.text().toInt());
                if(!iconMimeTypeElement.isNull())
                    icon->setMimeType(iconMimeTypeElement.text());
                if(!iconUrlElement.isNull())
                {
                    //Retrieve and cache the application icon
                    QUrl cacheUrl;
                    QUrl iconUrl = m_remoteServerUrl.resolved(QUrl(iconUrlElement.text()));
                    if (retrieveIcon(iconUrl, cacheUrl))
                        icon->setUrl(cacheUrl);
                    else
                        icon->setUrl(iconUrl);
                }
                iconList.append(icon);
            }
        }
        application->setIconList(iconList);

        if(!appDescriptionElement.isNull())
            application->setDescription(appDescriptionElement.text());

        QTmRemotingInfo *remotingInfo = new QTmRemotingInfo;
        application->setRemotingInfo(remotingInfo);
        if(!protocolIDElement.isNull())
        {
            if(!protocolIDElement.text().compare("VNC", Qt::CaseInsensitive))
                remotingInfo->setProtocolId(QTmGlobal::VNC);
            else
            if(!protocolIDElement.text().compare("RTP", Qt::CaseInsensitive))
                remotingInfo->setProtocolId(QTmGlobal::RTP);
            else
            if(!protocolIDElement.text().compare("BTA2DP", Qt::CaseInsensitive))
                remotingInfo->setProtocolId(QTmGlobal::BT_A2DP);
            else
            if(!protocolIDElement.text().compare("BTHFP", Qt::CaseInsensitive))
                remotingInfo->setProtocolId(QTmGlobal::BT_HFP);
            else
            if(!protocolIDElement.text().compare("DAP", Qt::CaseInsensitive))
                remotingInfo->setProtocolId(QTmGlobal::DAP);
            else
            if(!protocolIDElement.text().compare("NONE", Qt::CaseInsensitive))
                remotingInfo->setProtocolId(QTmGlobal::NONE);
            else
                remotingInfo->setProtocolId(QTmGlobal::PROPRIETARY);
        }

        if(!formatElement.isNull())
            remotingInfo->setFormat(formatElement.text());

        if(!directionElement.isNull())
        {
            if(!directionElement.text().compare("in", Qt::CaseInsensitive))
                remotingInfo->setDirection(QTmGlobal::DirectionIn);
            else
            if(!directionElement.text().compare("out", Qt::CaseInsensitive))
                remotingInfo->setDirection(QTmGlobal::DirectionOut);
            else
            if(!directionElement.text().compare("bi", Qt::CaseInsensitive))
                remotingInfo->setDirection(QTmGlobal::DirectionBi);
            else
                remotingInfo->setDirection(QTmGlobal::DirectionNone);
        }

        if(!providerNameElement.isNull())
            application->setProviderName(providerNameElement.text());
        if(!providerURLElement.isNull())
            application->setProviderUrl(providerURLElement.text());
        if(!variantElement.isNull())
            application->setVariants(variantElement.text().split(","));

        if(!allowedProfileIDElement.isNull())
        {
            QStringList profileIDList = allowedProfileIDElement.text().split(",");
            int i;
            for(i = 0; i < profileIDList.length(); i++)
            {
                application->allowedProfileIDs().append(((QString)profileIDList.at(i)).toInt());
            }
        }

        if(!appCertificateUrlElement.isNull())
            application->setAppCertificateURL(QUrl(appCertificateUrlElement.text()));

        QDomElement appInfoElement = appElement.firstChildElement("appInfo");
        QDomElement appCatElement;
        QDomElement appTrustElement;
        if(!appInfoElement.isNull())
        {
            appCatElement = appInfoElement.firstChildElement("appCategory");
            appTrustElement = appInfoElement.firstChildElement("trustLevel");
        }

        QTmAppInfo *appInfo = new QTmAppInfo();
        application->setAppInfo(appInfo);
        if(!appCatElement.isNull())
            appInfo->setCategory(appCatElement.text().toInt());
        if(!appTrustElement.isNull())
        {
            if(appTrustElement.text().toInt() == QTmGlobal::TrustCertified)
                appInfo->setTrust(QTmGlobal::TrustCertified);
            else
            if(appTrustElement.text().toInt() == QTmGlobal::TrustRegistered)
                appInfo->setTrust(QTmGlobal::TrustRegistered);
            else
            if(appTrustElement.text().toInt() == QTmGlobal::TrustSelfRegistered)
                appInfo->setTrust(QTmGlobal::TrustSelfRegistered);
            else
            if(appTrustElement.text().toInt() == QTmGlobal::TrustUser)
                appInfo->setTrust(QTmGlobal::TrustUser);
            else
                appInfo->setTrust(QTmGlobal::TrustUnknown);
        }

        QDomElement displayInfoElement = appElement.firstChildElement("displayInfo");
        QDomElement displayCatElement;
        QDomElement displayRulesElement;
        QDomElement displayTrustElement;
        if(!displayInfoElement.isNull())
        {
            displayCatElement = displayInfoElement.firstChildElement("contentCategory");
            displayRulesElement = displayInfoElement.firstChildElement("contentRules");
            displayTrustElement = displayInfoElement.firstChildElement("trustLevel");
        }
        QTmDisplayInfo *displayInfo = new QTmDisplayInfo();
        application->setDisplayInfo(displayInfo);
        if(!displayCatElement.isNull())
            displayInfo->setCategory(displayCatElement.text().toInt());
        if(!displayRulesElement.isNull())
            displayInfo->setRules(displayRulesElement.text().toInt());
        if(!displayTrustElement.isNull())
        {
            if(displayTrustElement.text().toInt() == QTmGlobal::TrustCertified)
                displayInfo->setTrust(QTmGlobal::TrustCertified);
            else
            if(displayTrustElement.text().toInt() == QTmGlobal::TrustRegistered)
                displayInfo->setTrust(QTmGlobal::TrustRegistered);
            else
            if(displayTrustElement.text().toInt() == QTmGlobal::TrustSelfRegistered)
                displayInfo->setTrust(QTmGlobal::TrustSelfRegistered);
            else
            if(displayTrustElement.text().toInt() == QTmGlobal::TrustUser)
                displayInfo->setTrust(QTmGlobal::TrustUser);
            else
                displayInfo->setTrust(QTmGlobal::TrustUnknown);
        }

        QDomElement audioInfoElement = appElement.firstChildElement("audioInfo");
        QDomElement audioTypeElement;
        QDomElement audioCatElement;
        QDomElement audioRulesElement;
        QDomElement audioTrustElement;
        if(!audioInfoElement.isNull())
        {
            audioTypeElement = audioInfoElement.firstChildElement("audioType");
            audioCatElement = audioInfoElement.firstChildElement("contentCategory");
            audioRulesElement = audioInfoElement.firstChildElement("contentRules");
            audioTrustElement = audioInfoElement.firstChildElement("trustLevel");
        }
        QTmAudioInfo *audioInfo = new QTmAudioInfo();
        application->setAudioInfo(audioInfo);
        if(!audioTypeElement.isNull())
            audioInfo->setType(audioTypeElement.text().toInt());
        if(!audioCatElement.isNull())
            audioInfo->setCategory(audioCatElement.text().toInt());
        if(!audioRulesElement.isNull())
            audioInfo->setRules(audioRulesElement.text().toInt());
        if(!audioTrustElement.isNull())
        {
            if(audioTrustElement.text().toInt() == QTmGlobal::TrustCertified)
                audioInfo->setTrust(QTmGlobal::TrustCertified);
            else
            if(audioTrustElement.text().toInt() == QTmGlobal::TrustRegistered)
                audioInfo->setTrust(QTmGlobal::TrustRegistered);
            else
            if(audioTrustElement.text().toInt() == QTmGlobal::TrustSelfRegistered)
                audioInfo->setTrust(QTmGlobal::TrustSelfRegistered);
            else
            if(audioTrustElement.text().toInt() == QTmGlobal::TrustUser)
                audioInfo->setTrust(QTmGlobal::TrustUser);
            else
                audioInfo->setTrust(QTmGlobal::TrustUnknown);
        }

        QDomElement resourceStatusElement = appElement.firstChildElement("resourceStatus");
        if(!resourceStatusElement.isNull())
        {
            if(!resourceStatusElement.text().compare("free", Qt::CaseInsensitive))
                application->setResourceStatus(QTmGlobal::ResourceFree);
            else
            if(!resourceStatusElement.text().compare("busy", Qt::CaseInsensitive))
                application->setResourceStatus(QTmGlobal::ResourceBusy);
            else
            if(!resourceStatusElement.text().compare("NA", Qt::CaseInsensitive))
                application->setResourceStatus(QTmGlobal::ResourceNA);
        }

#ifdef QT_DEBUG_TM_REMOTE_SERVER
        qDebug() << "QTmRemoteServer:" << "New Remote Application" << "(ID:" << application->appId() << ", Name:" << application->name() << ", Description:" << application->description() << ")";
#endif
        m_applicationListMutex.lock();
        m_applicationList.append(application);
        m_applicationListMutex.unlock();
        emit applicationAdded(application);
        appCount++;
    }
    emit applicationListingChanged(this);
    return 1;
}

// Subscribe to events from TmApplicationServer Service
// Returns @c 1 for success, @c 0 for failure
int QTmRemoteServer::subscribeToTmApplicationServerEvents()
{
    if(m_tmApplicationServer.eventSubUrl.isEmpty())
        return 0;
    else
        return subscribeToUPnPEvent(m_tmApplicationServer.eventSubUrl, m_tmApplicationServer.sid);
}

// Subscribe to events from TmClientProfile Service
// Returns @c 1 for success, @c 0 for failure
int QTmRemoteServer::subscribeToTmClientProfileEvents()
{
    if(m_tmClientProfile.eventSubUrl.isEmpty())
        return 0;
    else
        return subscribeToUPnPEvent(m_tmClientProfile.eventSubUrl, m_tmClientProfile.sid);
}

// Subscribe to UPnP Service using URL given by @c eventSubUrl
// Returns @c 1 for success, @c 0 for failure
int QTmRemoteServer::subscribeToUPnPEvent(const QUrl &eventSubUrl, QString &sid)
{
   if(!m_eventingHttpServer)
       return 0;

   //Construct callback url
   QString serverAdress = m_localAddress.isNull() ? m_eventingHttpServer->serverAddress().toString()
                                                 : m_localAddress.toString();
   QString eventCallbackUrl = "<http://" + serverAdress + ":" + QString::number(m_eventingHttpServer->serverPort()) + "/>";

   //Create SUBSCRIBE type message
   QString request = QString("SUBSCRIBE %1 HTTP/1.1\r\n"
                             "HOST: %2:%3\r\n"
                             "CALLBACK: %4\r\n"
                             "NT: upnp:event\r\n"
                             "\r\n")
           .arg(eventSubUrl.path())
           .arg(eventSubUrl.host())
           .arg(eventSubUrl.port())
           .arg(eventCallbackUrl);

   QByteArray response;

#ifdef QT_DEBUG_TM_REMOTE_SERVER
   qDebug() << "QTmRemoteServer:" << "Sending subscribe message:" << request.toAscii() << endl;
#endif

   //Send subscription request to HTTP server and get back response
   if(!performHTTPAction(eventSubUrl, request.toAscii(), response))
       return 0;

   QString responseString = QString::fromAscii(response);

#ifdef QT_DEBUG_TM_REMOTE_SERVER
   qDebug() << "QTmRemoteServer:" << "Subscription response message:" << responseString << endl;
#endif

   if(responseString.toUpper().indexOf("SID: ") == -1)
       return 0;

   //Retrieve and store the SID which identifies this UPnP service event subscription
   int sLoc = responseString.toUpper().indexOf("SID:");
   responseString = responseString.mid(sLoc+4);
   QStringList sList = responseString.split("\r\n");
   sid = ((QString)(sList.at(0))).simplified();

   //Register event listener
   m_eventingHttpServer->addPropertyChangeListener(sid, this);
   return 1;
}

// Unsubscribe from events of TmApplicationServer Service
// Returns @c 1 for success, @c 0 for failure
int QTmRemoteServer::unsubscribeFromTmApplicationServerEvents()
{
    if(m_tmApplicationServer.eventSubUrl.isEmpty())
        return 0;
    else
        return unsubscribeFromUPnPEvent(m_tmApplicationServer.eventSubUrl, m_tmApplicationServer.sid);
}

// Subscribe to events from TmClientProfile Service
// Returns @c 1 for success, @c 0 for failure
int QTmRemoteServer::unsubscribeFromTmClientProfileEvents()
{
    if(m_tmClientProfile.eventSubUrl.isEmpty())
        return 0;
    else
        return unsubscribeFromUPnPEvent(m_tmClientProfile.eventSubUrl, m_tmClientProfile.sid);
}

// Unsubscribe from UPnP Service using URL given by @c eventSubUrl
// Returns @c 1 for success, @c 0 for failure
int QTmRemoteServer::unsubscribeFromUPnPEvent(const QUrl &eventSubUrl, QString &sid)
{
    if(sid.isNull() || sid.length() <= 0)
        return 0;

    //Unregister event listener
    if (m_eventingHttpServer)
        m_eventingHttpServer->removePropertyChangeListener(sid);

    //Create UNSUBSCRIBE type message
    QString request = QString("UNSUBSCRIBE %1 HTTP/1.1\r\n"
                              "HOST: %2:%3\r\n"
                              "SID: %4\r\n\r\n")
            .arg(eventSubUrl.path())
            .arg(eventSubUrl.host())
            .arg(eventSubUrl.port())
            .arg(sid);

    QByteArray response;

#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Sending unsubscribe message:" << request.toAscii() << endl;
#endif

    //Send unsubscription request to HTTP server and get back reponse
    if(!performHTTPAction(eventSubUrl, request.toAscii(), response))
        return 0;

    QString responseString = QString::fromAscii(response);

#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Unsubscription response message:" << responseString << endl;
#endif

    //Unregister event listener
    m_eventingHttpServer->removePropertyChangeListener(sid);

    //Reset SID to empty
    sid = QString();
    return 1;
}

// Retrieve Icon file stored at URL given by iconPath
// Returns true for success, false for failure
bool QTmRemoteServer::retrieveIcon(const QUrl &iconUrl, QUrl &url)
{
    if(iconUrl.isEmpty()) {
        qWarning() << "QTmRemoteServer:" << "Cannot retrieve icon file: Icon path is empty.";
        return false;
    }

#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Retrieving Icon" << iconUrl.toString();
#endif

    //Create GET type message
    QString request = QString("GET %1 HTTP/1.1\r\n"
                              "HOST: %2:%3\r\n\r\n")
            .arg(iconUrl.path())
            .arg(iconUrl.host())
            .arg(iconUrl.port());

    QByteArray response;

    //Send to HTTP server and get back the icon file
    if(!performHTTPAction(iconUrl, request.toAscii(), response))
        return false;

    int index = response.indexOf("\r\n\r\n");

    if (index != -1)
    {
        index += 4; // skip "\r\n\r\n"

        //Create directory
        QFileInfo cachePath(QString(QT_TM_CACHE_PATH), m_UDN);
        QDir dir;
        if (!dir.mkpath(cachePath.filePath())) {
            qWarning() << "QTmRemoteServer:" << "Could not create cache path" << cachePath.filePath();
            return false;
        }

        //Store icon
        QString iconFileStoredPath = QFileInfo(cachePath.filePath(), QFileInfo(iconUrl.path()).fileName()).absoluteFilePath();
        QFile fileIcon(iconFileStoredPath);
        if(!fileIcon.open(QIODevice::WriteOnly)) {
            qWarning() << "QTmRemoteServer:" << "Could not create icon on" << iconFileStoredPath;
            return false;
        }
        int pos = index;
        int len = response.length();
        const char *data = response.constData();
        while (pos < len) {
            int err = fileIcon.write(data + pos, len - pos);
            if (err == -1)
                break;
            pos += err;
        }
        fileIcon.close();

#ifdef QT_DEBUG_TM_REMOTE_SERVER
        qDebug() << "QTmRemoteServer:" << "Icon file name cached at:" << iconFileStoredPath;
#endif
        url = QUrl(iconFileStoredPath);
        return true;
    } else {
        qWarning() << "QTmRemoteServer:" << "Icon file was not found!";
        return false;
    }
}


// Connect to HTTP server at m_httpAddress # m_httpPort and send sendBuffer to server and store server response in receivedBuffer
// Retrieve VNC server port number from connString and store in private variable m_vncServerPort
// Returns true for success, false for failure
bool QTmRemoteServer::performHTTPAction(const QUrl &url, const QByteArray &sendBuffer, QByteArray &receiveBuffer)
{
    if(url.isEmpty() || url.isRelative() || sendBuffer.isEmpty()) {
        qWarning() << "QTmRemoteServer:" << "Remote HTTP Server: IP address or port not set or message buffer is empty";
        return false;
    }

    QTcpSocket socket;

    socket.connectToHost(url.host(), url.port());

    socket.setSocketOption(QAbstractSocket::LowDelayOption, 1);

    if (!socket.waitForConnected())
    {
        qWarning() << "QTmRemoteServer:" << "Failed to connect to Remote UPnP HTTP Server" << url.host() << ":" << url.port() << "error" << socket.errorString();
        return false;
    }

    socket.write(sendBuffer);
    socket.waitForBytesWritten();

    //While there's data, read and store it in receivedBuffer
    int headerLength = -1;
    long totalLength = -1;
    bool valid = false;
    while (socket.waitForReadyRead(60000))
    {
        while (socket.bytesAvailable())
        {
            if (totalLength == -1)
                receiveBuffer += socket.readAll();
            else
                receiveBuffer += socket.read(totalLength - receiveBuffer.length());
        }

        // Parse header
        if (headerLength == -1) {
            //Find header
            int index = receiveBuffer.indexOf("\r\n\r\n");
            if (index != -1) {
                headerLength = index + 4; // skip "\r\n\r\n"
                //Parse header lines
                QString header = receiveBuffer.mid(0, index);
                QStringList lines = header.split('\n');
                foreach( QString line, lines) {
                    line = line.trimmed();
                    if (line.startsWith("HTTP/", Qt::CaseInsensitive)) {
                        if (!line.contains("200")) {
                            qWarning() << "QTmRemoteServer:" << "HTTP request failed!\"" << line << "\"";
                            socket.close();
                            return false;
                        } else {
                            valid = true;
                        }
                    } else if (line.startsWith("CONTENT-LENGTH:", Qt::CaseInsensitive)) {
                        long contentLength = line.remove("CONTENT-LENGTH:", Qt::CaseInsensitive).trimmed().toLong();
                        totalLength = contentLength + headerLength;
#ifdef QT_DEBUG_TM_REMOTE_SERVER
                        qDebug() << "QTmRemoteServer:" << "content-length:" << contentLength << "total-length:" << totalLength;
#endif
                    }
                }
            }
        }

        // Check conetent length
        if (totalLength != -1 && receiveBuffer.length() >= totalLength) {
            break;
        }
    }

    //Close HTTP session
    socket.close();

    return valid;
}

bool QTmRemoteServer::performSOAPAction(const QUrl &url, const QString &action, const QString &body, QString &result)
{
    //Create POST message
    QString request = QString("POST %1 HTTP/1.0\r\n"
                              "HOST: %2:%3\r\n"
                              "CONTENT-TYPE: text/xml; charset=\"utf-8\"\r\n"
                              "CONTENT-LENGTH: %4\r\n"
                              "USER-AGENT: Linux/2.6 UPnP/1.1 TMClient/1.1\r\n"
                              "SOAPACTION: \"%5\"\r\n\r\n")
            .arg(url.path())
            .arg(url.host())
            .arg(url.port())
            .arg(body.length())
            .arg(action);
    request += body;

    QByteArray response;

    if(!performHTTPAction(url, request.toAscii(), response)) {
        qWarning() << "QTmRemoteServer:" << "Cannot perform HTTP SOAP Call.";
        return false;
    }

    QString responseString = unescapeXML(QString::fromAscii(response));

    //Try to find beginning of SOAP response
    int start = responseString.indexOf("<s:Envelope");
    int end = responseString.indexOf("</s:Envelope", start);

    if ((start == -1) || (end == -1))
        return false;

    end = responseString.indexOf(">", end);
    if (end == -1)
        return false;
    end++;

    //If this is a SOAP error message, then also exit
    if(responseString.contains("UPnPError")) {
        qWarning() << "QTmRemoteServer:" << "UPnP HTTP Server sent SOAP Error response!";
        return false;
    } else {
        result = responseString.mid(start, end);
        return true;
    }
}

// Implementation function for QTmRemoteServer::launchApplication
int QTmRemoteServer::soapLaunchApplication(const QUrl &serviceControl, QString appID, int profileID, QString &appUri)
{
    //SOAP Methods invocation for LaunchApplication call for TmApplicationServer:1 UPnP service
    QString body = QString("<?xml version=\"1.0\"?>"
                           "<s:Envelope"
                           " xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\""
                           " s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                           "<s:Body>"
                           "<u:LaunchApplication xmlns:u=\"urn:schemas-upnp-org:service:TmApplicationServer:1\">"
                           "<AppID>%1</AppID>"
                           "<ProfileID>%2</ProfileID>"
                           "</u:LaunchApplication>"
                           "</s:Body>"
                           "</s:Envelope>\r\n")
            .arg(appID)
            .arg(profileID);

#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Launching AppID" << appID;
#endif

    QString result;

    //Send to HTTP server and get back results
    if(!performSOAPAction(serviceControl, "urn:schemas-upnp-org:service:TmApplicationServer:1#LaunchApplication", body, result))
        return 0;

    int len = QString("<AppURI>").length();
    int start = result.indexOf("<AppURI>");
    int end = result.indexOf("</AppURI>", start);

    // no <AppURI> tag found
    if ((start == -1) || (end == -1))
        return 0;

    //Extract connection parameters to access and control launched remote application
    appUri = result.mid(start + len, end - start - len);
    return 1;
}

// Wrapper function for QTmRemoteServer::terminateApplication
int QTmRemoteServer::soapTerminateApplication(const QUrl &serviceControl, QString appID, int profileID)
{
    //SOAP Methods invocation for TerminateApplication call for TmApplicationServer:1 UPnP service
    QString body = QString("<?xml version=\"1.0\"?>"
                           "<s:Envelope"
                           " xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\""
                           " s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                           "<s:Body>"
                           "<u:TerminateApplication xmlns:u=\"urn:schemas-upnp-org:service:TmApplicationServer:1\">"
                           "<AppID>%1</AppID>"
                           "<ProfileID>%2</ProfileID>"
                           "</u:TerminateApplication>"
                           "</s:Body>"
                           "</s:Envelope>\r\n")
            .arg(appID)
            .arg(profileID);

#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Terminating AppID" << appID;
#endif

    QString result;

    //Send to HTTP server and get back results
    if(!performSOAPAction(serviceControl, "urn:schemas-upnp-org:service:TmApplicationServer:1#TerminateApplication", body, result))
        return 0;

    int len = QString("<TerminationResult>").length();
    int start = result.indexOf("<TerminationResult>");
    int end = result.indexOf("</TerminationResult>", start);

    if ((start == -1) || (end == -1))
    {
        return 0;
    }

    QString terminationResult = result.mid(start + len, end - start - len);
    if(terminationResult.compare("true", Qt::CaseInsensitive) == 0)
        return 1;
    else
        return 0;
}

// Implementation function for QTmRemoteServer::getApplicationStatus
int QTmRemoteServer::soapGetApplicationStatus(const QUrl &serviceControl, QString appID, QTmGlobal::TmStatus *appStatus)
{
    //SOAP Methods invocation for GetApplicationStatus call for TmApplicationServer:1 UPnP service
    QString body = QString("<?xml version=\"1.0\"?>"
                           "<s:Envelope"
                           " xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\""
                           " s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                           "<s:Body>"
                           "<u:GetApplicationStatus xmlns:u=\"urn:schemas-upnp-org:service:TmApplicationServer:1\">"
                           "<AppID>%1</AppID>"
                           "</u:GetApplicationStatus>"
                           "</s:Body>"
                           "</s:Envelope>\r\n")
            .arg(appID);

#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Getting Status of AppID" << appID;
#endif

    QString result;

    //Send to HTTP server and get back results
    if(!performSOAPAction(serviceControl, "urn:schemas-upnp-org:service:TmApplicationServer:1#GetApplicationStatus", body, result))
        return 0;

    //Currently we assume that we only support 1 client profile and only 1 instance of a specific application
    //can run at any given time
    int len = QString("<statusType>").length();
    int start = result.indexOf("<statusType>");
    int end = result.indexOf("</statusType>", start);

    if ((start == -1) || (end == -1))
        return 0;

    //Extract return parameter data
    QString tempVal = result.mid(start + len, end - start - len);

    //Extract termination result
    if (tempVal == "Foreground")
        *appStatus = QTmGlobal::StatusForeground;
    else if (tempVal == "Background")
        *appStatus = QTmGlobal::StatusBackground;
    else if (tempVal == "Notrunning")
        *appStatus = QTmGlobal::StatusNotrunning;
    else
        return 0;

    return 1;
}

// Launch Application with id \a appID using TmApplicationServer service
// Returns @c 1 for success, @c 0 for failure
bool QTmRemoteServer::launchApplication(QTmApplication *application, int profileID)
{
    if (!application)
        return false;

    QString appUri;
    bool ret = false;
    //Launch Application
    m_ctrlMutex.lock();
    if (soapLaunchApplication(m_tmApplicationServer.controlUrl, application->appId(), profileID, appUri)) {
        application->setUrl(appUri);
        emit applicationLaunched(application);
        ret = true;
    }
    m_ctrlMutex.unlock();
    return ret;
}

// Get Application Status of application with id \a appID using TmApplicationServer service
// Returns application status defined by @c QTmGlobal::TmStatus
QTmGlobal::TmStatus QTmRemoteServer::getApplicationStatus(QTmApplication *application)
{
    if (!application)
        return QTmGlobal::StatusUnknown;

    QTmGlobal::TmStatus appState = QTmGlobal::StatusUnknown;
    //Get Application Status
    m_ctrlMutex.lock();
    if (soapGetApplicationStatus(m_tmApplicationServer.controlUrl, application->appId(), &appState)) {
        if (application->status() != appState) {
            application->setStatus(appState);
            emit applicationStatusChanged(application);
        }
    }
    m_ctrlMutex.unlock();
    return appState;
}

// Terminate Application with id \a appID using TmApplicationServer service
// Returns @c 1 for success, @c 0 for failure
bool QTmRemoteServer::terminateApplication(QTmApplication *application, int profileID)
{
    if (!application)
        return false;

    bool ret = false;
    //Terminate Application
    m_ctrlMutex.lock();
    if (soapTerminateApplication(m_tmApplicationServer.controlUrl, application->appId(), profileID)) {
        application->setUrl(QUrl());
        emit applicationTerminated(application);
        ret = true;
    }
    m_ctrlMutex.unlock();
    return ret;
}

// Replace reserved characters in a XML formatted string with escaped character codes
// and return modified string
QString QTmRemoteServer::escapeXML(QString inputXML)
{
    QString escapedXMLString = inputXML;
    escapedXMLString = escapedXMLString.replace("&", "&amp;");
    escapedXMLString = escapedXMLString.replace("<", "&lt;");
    escapedXMLString = escapedXMLString.replace(">", "&gt;");
    return escapedXMLString;
}

// Replace escaped characters in a XML formatted string with literal characters
// and return modified string
QString QTmRemoteServer::unescapeXML(QString inputXML)
{
    QString unescapedXMLString = inputXML;
    unescapedXMLString = unescapedXMLString.replace("&amp;", "&");
    unescapedXMLString = unescapedXMLString.replace("&lt;", "<");
    unescapedXMLString = unescapedXMLString.replace("&gt;", ">");
    return unescapedXMLString;
}

// Get maximum number of client profiles supported by TmClientProfile service
// Returns @c number of profiles allowed
int QTmRemoteServer::getMaxNumProfiles()
{
    int numProfilesAllowed;

    m_ctrlMutex.lock();
    if(!soapGetMaxNumProfiles(m_tmClientProfile.controlUrl, &numProfilesAllowed))
        numProfilesAllowed = -1;
    m_ctrlMutex.unlock();

    return numProfilesAllowed;
}

// Get contents of a client profile with id \a profileID from the TmClientProfile service with profile ID
// Returns @c client profile of type QTmClientProfile
QTmClientProfile QTmRemoteServer::getClientProfile(int profileID)
{
    QDomDocument clientProfile;
    QTmClientRules   clientRules;

    m_ctrlMutex.lock();
    if(!soapGetClientProfile(m_tmClientProfile.controlUrl, profileID, &clientProfile)) {
        m_ctrlMutex.unlock();
        return QTmClientProfile("null");
    }
    m_ctrlMutex.unlock();

    QString clientID = clientProfile.documentElement().firstChildElement("clientID").text();
    QTmClientProfile clientProfileObj(clientID);

    QString friendlyName = clientProfile.documentElement().firstChildElement("friendlyName").text();
    QString manufacturer = clientProfile.documentElement().firstChildElement("manufacturer").text();
    QString modelName = clientProfile.documentElement().firstChildElement("modelName").text();
    QString modelNumber = clientProfile.documentElement().firstChildElement("modelNumber").text();
    QString mimetype = clientProfile.documentElement().firstChildElement("iconPreference").firstChildElement("mimetype").text();
    QString width = clientProfile.documentElement().firstChildElement("iconPreference").firstChildElement("width").text();
    QString height = clientProfile.documentElement().firstChildElement("iconPreference").firstChildElement("height").text();
    QString depth = clientProfile.documentElement().firstChildElement("iconPreference").firstChildElement("depth").text();
    QString bdAddr = clientProfile.documentElement().firstChildElement("connectivity").firstChildElement("bluetooth").firstChildElement("bdAddr").text();
    QString startConnection = clientProfile.documentElement().firstChildElement("connectivity").firstChildElement("bluetooth").firstChildElement("startConnection").text();
    QDomNodeList ruleNodes = clientProfile.documentElement().firstChildElement("contentRules").childNodes();

    clientProfileObj.setClientName(friendlyName);
    clientProfileObj.setManufacturerName(manufacturer);
    clientProfileObj.setModelName(modelName);
    clientProfileObj.setModelNumber(modelNumber);
    clientProfileObj.setIconMimeType(mimetype);
    clientProfileObj.setIconWidth(width.toInt());
    clientProfileObj.setIconHeight(height.toInt());
    clientProfileObj.setIconDepth(depth.toInt());
    clientProfileObj.setBdAddr(bdAddr);
    if (!startConnection.compare("true", Qt::CaseInsensitive))
        clientProfileObj.setStartBTConnection(true);
    else
        clientProfileObj.setStartBTConnection(false);

    unsigned int i;
    for (i = 0; i < ruleNodes.length(); i++) {
        clientRules.setRule(ruleNodes.at(i).firstChildElement("ruleId").text().toInt(), ruleNodes.at(i).firstChildElement("ruleValue").text());
    }
    clientProfileObj.setClientRules(clientRules);

    return clientProfileObj;
}

// Setting contents of a client profile with id \a profileID from the service to contents of the
// client profile given by \a clientProfile
// Returns @c QTmClientProfile object containing the new profile settings
QTmClientProfile QTmRemoteServer::setClientProfile(QTmClientProfile profile, int profileID)
{
    QString CLIENT_PROFILE_TEMPLATE = "<clientProfile>"
                                      "<clientID>%1</clientID>"
                                      "<friendlyName>%2</friendlyName>"
                                      "<manufacturer>%3</manufacturer>"
                                      "<modelName>%4</modelName>"
                                      "<modelNumber>%5</modelNumber>"
                                      "<iconPreference>"
                                      "<mimetype>%6</mimetype>"
                                      "<width>%7</width>"
                                      "<height>%8</height>"
                                      "<depth>%9</depth>"
                                      "</iconPreference>"
                                      "<connectivity>"
                                      "<bluetooth>"
                                      "<bdAddr>%10</bdAddr>"
                                      "<startConnection>%11</startConnection>"
                                      "</bluetooth>"
                                      "</connectivity>"
                                      "<contentRules>%12</contentRules>"
                                      "</clientProfile>\r\n";

    QString startBTConn;
    if(profile.startBTConnection() == true)
        startBTConn = "true";
    else
        startBTConn = "false";

    QString rulesStr = "";
    QHash<int, QString>inputRulesHash = profile.clientRules().getAllRules();
    if (inputRulesHash.size() > 0) {
        QHashIterator<int, QString> i(inputRulesHash);
        while (i.hasNext()) {
            i.next();
            rulesStr += "<rule><ruleId>";
            rulesStr += QString::number(i.key());
            rulesStr += "</ruleId><ruleValue>";
            rulesStr += i.value();
            rulesStr += "</ruleValue></rule>";
        }
    }

    QString CLIENT_PROFILE = QString(CLIENT_PROFILE_TEMPLATE).arg(profile.clientId()).arg(profile.clientName()).arg(profile.manufacturerName()).arg(profile.modelName())
                             .arg(profile.modelNumber()).arg(profile.iconMimeType()).arg(QString::number(profile.iconWidth())).arg(QString::number(profile.iconHeight()))
                             .arg(QString::number(profile.iconDepth())).arg(profile.bdAddr()).arg(startBTConn).arg(rulesStr);

#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Setting Client Profile to:\n" << CLIENT_PROFILE;
#endif

    QString CLIENT_PROFILE_ESC = escapeXML(CLIENT_PROFILE);
    QDomDocument clientProfile;
    QTmClientRules   clientRules;

    m_ctrlMutex.lock();
    if (!soapSetClientProfile(m_tmClientProfile.controlUrl, profileID, CLIENT_PROFILE_ESC, &clientProfile)) {
        m_ctrlMutex.unlock();
        return QTmClientProfile("null");
    }
    m_ctrlMutex.unlock();

    QString clientID = clientProfile.documentElement().firstChildElement("clientID").text();
    QTmClientProfile clientProfileObj = QTmClientProfile(clientID);

    QString friendlyName = clientProfile.documentElement().firstChildElement("friendlyName").text();
    QString manufacturer = clientProfile.documentElement().firstChildElement("manufacturer").text();
    QString modelName = clientProfile.documentElement().firstChildElement("modelName").text();
    QString modelNumber = clientProfile.documentElement().firstChildElement("modelNumber").text();
    QString mimetype = clientProfile.documentElement().firstChildElement("iconPreference").firstChildElement("mimetype").text();
    QString width = clientProfile.documentElement().firstChildElement("iconPreference").firstChildElement("width").text();
    QString height = clientProfile.documentElement().firstChildElement("iconPreference").firstChildElement("height").text();
    QString depth = clientProfile.documentElement().firstChildElement("iconPreference").firstChildElement("depth").text();
    QString bdAddr = clientProfile.documentElement().firstChildElement("connectivity").firstChildElement("bluetooth").firstChildElement("bdAddr").text();
    QString startConnection = clientProfile.documentElement().firstChildElement("connectivity").firstChildElement("bluetooth").firstChildElement("startConnection").text();
    QDomNodeList ruleNodes = clientProfile.documentElement().firstChildElement("contentRules").childNodes();

    clientProfileObj.setClientName(friendlyName);
    clientProfileObj.setManufacturerName(manufacturer);
    clientProfileObj.setModelName(modelName);
    clientProfileObj.setModelNumber(modelNumber);
    clientProfileObj.setIconMimeType(mimetype);
    clientProfileObj.setIconWidth(width.toInt());
    clientProfileObj.setIconHeight(height.toInt());
    clientProfileObj.setIconDepth(depth.toInt());
    clientProfileObj.setBdAddr(bdAddr);
    if(!startConnection.compare("true", Qt::CaseInsensitive))
        clientProfileObj.setStartBTConnection(true);
    else
        clientProfileObj.setStartBTConnection(false);

    unsigned int i;
    for (i = 0; i < ruleNodes.length(); i++) {
        clientRules.setRule(ruleNodes.at(i).firstChildElement("ruleId").text().toInt(), ruleNodes.at(i).firstChildElement("ruleValue").text());
    }
    clientProfileObj.setClientRules(clientRules);

    return clientProfileObj;
}

// Implementation function for QTmRemoteServer::getMaxNumProfiles
int QTmRemoteServer::soapGetMaxNumProfiles(const QUrl &serviceControl, int *numProfilesAllowed)
{
    //SOAP Methods invocation for GetMaxNumProfiles call for TmClientProfile:1 UPnP service
    QString body = QString("<?xml version=\"1.0\"?>"
                           "<s:Envelope"
                           " xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\""
                           " s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                           "<s:Body>"
                           "<u:GetMaxNumProfiles xmlns:u=\"urn:schemas-upnp-org:service:TmClientProfile:1\">"
                           "</u:GetMaxNumProfiles>"
                           "</s:Body>"
                           "</s:Envelope>\r\n");

#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Getting Max Num Profiles";
#endif

    QString result;

    //Send to HTTP server and get back results
    if(!performSOAPAction(serviceControl, "urn:schemas-upnp-org:service:TmClientProfile:1#GetMaxNumProfiles", body, result))
        return 0;

    //Currently we assume that we only support 1 client profile and only 1 instance of a specific application
    //can run at any given time
    int len = QString("<NumProfilesAllowed>").length();
    int start = result.indexOf("<NumProfilesAllowed>");
    int end = result.indexOf("</NumProfilesAllowed>", start);

    if ((start == -1) || (end == -1))
        return 0;

    //Extract return parameter data
    QString tempVal = result.mid(start + len, end - start - len);

    bool bOk;
    *numProfilesAllowed = tempVal.toInt(&bOk);
    return (bOk ? 1 : 0);
}

// Implementation function for QTmRemoteServer::getClientProfile
int QTmRemoteServer::soapGetClientProfile(const QUrl &serviceControl, int profileID, QDomDocument *clientProfile)
{
    //SOAP Methods invocation for GetClientProfile call for TmClientProfile:1 UPnP service
    QString body = QString("<?xml version=\"1.0\"?>"
                           "<s:Envelope"
                           " xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\""
                           " s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                           "<s:Body>"
                           "<u:GetClientProfile xmlns:u=\"urn:schemas-upnp-org:service:TmClientProfile:1\">"
                           "<ProfileID>%1</ProfileID>"
                           "</u:GetClientProfile>"
                           "</s:Body>"
                           "</s:Envelope>\r\n")
            .arg(profileID);

#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Getting Client Profile";
#endif

    QString result;

    //Send to HTTP server and get back results
    if(!performSOAPAction(serviceControl, "urn:schemas-upnp-org:service:TmClientProfile:1#GetClientProfile", body, result))
        return 0;

    //Currently we assume that we only support 1 client profile and only 1 instance of a specific application
    //can run at any given time
    int len = QString("<ClientProfile>").length();
    int start = result.indexOf("<ClientProfile>");
    int end = result.indexOf("</ClientProfile>", start);

    if ((start == -1) || (end == -1))
        return 0;

     //Extract return parameter data
    QString tempXML = result.mid(start + len, end - start -len);

    if (!(*clientProfile).setContent(tempXML)) {
        qWarning() << "QTmRemoteServer:" << "HTTP Server sent invalid SOAP response!";
        return 0;
    }
    return 1;
}

// Implementation function for QTmRemoteServer::setClientProfile
int QTmRemoteServer::soapSetClientProfile(const QUrl &serviceControl, int profileID, QString clientProfile, QDomDocument *resultProfile)
{
    //SOAP Methods invocation for GetClientProfile call for TmClientProfile:1 UPnP service
    QString body = QString("<?xml version=\"1.0\"?>"
                           "<s:Envelope"
                           " xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\""
                           " s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                           "<s:Body>"
                           "<u:SetClientProfile xmlns:u=\"urn:schemas-upnp-org:service:TmClientProfile:1\">"
                           "<ProfileID>%1</ProfileID>"
                           "<ClientProfile>%2</ClientProfile>"
                           "</u:SetClientProfile>"
                           "</s:Body>"
                           "</s:Envelope>\r\n")
            .arg(profileID).arg(clientProfile);

#ifdef QT_DEBUG_TM_REMOTE_SERVER
    qDebug() << "QTmRemoteServer:" << "Setting Client Profile";
#endif

    QString result;

    //Send to HTTP server and get back results
    if(!performSOAPAction(serviceControl, "urn:schemas-upnp-org:service:TmClientProfile:1#SetClientProfile", body, result))
        return 0;

    //Currently we assume that we only support 1 client profile and only 1 instance of a specific application
    //can run at any given time
    int len = QString("<ResultProfile>").length();
    int start = result.indexOf("<ResultProfile>");
    int end = result.indexOf("</ResultProfile>", start);

    if ((start == -1) || (end == -1))
        return 0;

    //Extract return parameter data
    QString tempXML = result.mid(start + len, end - start - len);

    if (!(*resultProfile).setContent(tempXML)) {
        qWarning() << "QTmRemoteServer:" << "HTTP Server sent invalid SOAP response!";
        return 0;
    }
    return 1;
}

void QTmRemoteServer::processPropertyChangeEvent(const QString &name, const QString &value) {
    if (name == "AppStatusUpdate") {
#ifdef QT_DEBUG_TM_REMOTE_SERVER
        qDebug() << "QTmRemoteServer:" << "AppStatusUpdate:" << value;
#endif
        QStringList strList = value.split(",");
        for (int i = 0; i < strList.length(); i++) {
            QString appId = strList.at(i);
            // Retrieve Application Status
            QTmApplication *application = applicationById(appId);
            getApplicationStatus(application);
        }
    }
}

QTmApplication* QTmRemoteServer::applicationById(QString appId)
{
    m_applicationListMutex.lock();
    foreach (QTmApplication *app, m_applicationList) {
        if (appId == app->appId()) {
            m_applicationListMutex.unlock();
            return app;
        }
    }
    m_applicationListMutex.unlock();
    return 0;
}

int QTmRemoteServer::newRemoteServerId()
{
    static int remoteServerIdCount = 0;
#ifdef QT_DEBUG_TM_UPNP
        qDebug() << "QTmRemoteServer:" << "RemoteServerId generated = " << remoteServerIdCount;
#endif
    return remoteServerIdCount++;
}

QString QTmRemoteServer::USN()
{
    return m_USN;
}
