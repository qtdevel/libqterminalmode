/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include "QTmApplication.h"

/*!
  \class QTmApplication

  \brief The QTmApplications class provides access to the encapsulated application data, derived from the UPnP Application advertisement.

  A Terminal Mode enabled UPnP control point will receive the application list, of remote applications available
  from Terminal Mode server. Each application advertisement received will contain further information about the
  application, the application provider, used audio etc. The applications from each server are uniquely identified
  by an application id. The advertisements might be signed and verified from the Terminal Mode server.

  This class provides read access to application meta information.

  Write access is limited to the internal library classes only, extracting the information from the UPnP advertisements.
 */
/*!
  \property QTmApplication::appId
  \brief Holds the unique application identifier.

  The application identier is unique for the duration of the Terminal Mode session.
  It is used within the VNC Context Information Pseudo Encoding, to link a rectangle framebuffer area to a specific application.
  The Terminal Mode RTP header extension does include this application id as well.
 */
/*!
  \property QTmApplication::name
  \brief Holds the application name.
 */
/*!
  \property QTmApplication::variants
  \brief Holds the comma separated list of supported vehicle variants
 */
/*!
  \property QTmApplication::providerName
  \brief Holds the application provider's name
 */
/*!
  \property QTmApplication::providerUrl
  \brief Holds the application provider's URL
 */
/*!
  \property QTmApplication::description
  \brief Holds the applcation description
 */
/*!
  \property QTmApplication::iconList
  \brief Holds the list of application icons.
 */
/*!
  \property QTmApplication::allowedProfileIDs
  \brief Holds the list of allowed profile IDs.
 */
/*!
  \property QTmApplication::remotingInfo
  \brief Holds the remote protocol information.
 */
/*!
  \property QTmApplication::appCertificateURL
  \brief Holds the application's certificate URL.

  This URL gives the Terminal Mode client access to the application's certificate, stored on the Terminal Mode server.
  The client can use the URL to retrieve the certificate, e.g. using HTTP get, and cross-check it.
  In general, there is no need for the client to check the certificate itself, as the server must provide all the
  relevant information. The server must check the validity of the certificate prior providing the information.

  The client may only check the certificate, if it contains proprietary elements, the server is not aware of.
 */
/*!
  \property QTmApplication::appInfo
  \brief Holds additional application information.
 */
/*!
  \property QTmApplication::displayInfo
  \brief Holds additional display information.
 */
/*!
  \property QTmApplication::audioInfo
  \brief Holds additional audio information.
 */
/*!
  \property QTmApplication::resourceStatus
  \brief Holds resource status. Valid for audio resources only.
 */
/*!
  \property QTmApplication::status
  \brief Holds status information.
 */
/*!
  \property QTmApplication::hasSignature
  \brief Holds information whether the application advertisement contained a signature.

  The value is set to true, if the application advertisement contains a signature element at the end.
  The value is set to false, otherwise.
 */
/*!
  \property QTmApplication::isValidated
  \brief Holds information whether the signture has been validated.

  The value is set to true, if the signature has been validated using the known session key.
  The value is set to false otherwise. It is set to false as well, if no signature is available.
 */
/*!
  \property QTmApplication::remoteServerId
  \brief Holds the remote server identifier.

  This id uniquely identifies a remote server. This id is useful, if more than one remote server are available,
  either via the same interface, or via a different interface.
 */


/*!
  Class Constructure.
  The identifier of the remote server is given in \a remoterServerId. \a appId gives the unique application identifier.
  Information is provided, whether the application context has a \a signature and got \a validated.
  \a parent points to the object's parent.
 */
QTmApplication::QTmApplication(int remoteServerId, QString appId, bool signature, bool validated, QObject *parent) :
        m_appId(appId),
        m_iconList(QList<QTmIcon *>()),
        m_remotingInfo(0),
        m_appInfo(0),
        m_displayInfo(0),
        m_audioInfo(0),
        m_resourceStatus(QTmGlobal::ResourceNA),
        m_status(QTmGlobal::StatusNotrunning),
        m_signature(signature),
        m_validated(validated),
        m_remoteServerId(remoteServerId) {
    Q_UNUSED(parent)
    m_name              = QString();
    m_variants          = QStringList();
    m_providerName      = QString();
    m_providerUrl       = QUrl();
    m_description       = QString();
    m_allowedProfileIDs = QList<int>();
    m_appCertificateURL = QUrl();
    m_url               = QUrl();
}


/*!
  Class destructor. Delete all internal class instances and lists.
 */
QTmApplication::~QTmApplication() {
    while (!m_iconList.isEmpty()) {
        delete m_iconList.first();
        m_iconList.removeFirst();
    }
    if (m_remotingInfo) delete m_remotingInfo;
    if (m_appInfo)      delete m_appInfo;
    if (m_displayInfo)  delete m_displayInfo;
    if (m_audioInfo)    delete m_audioInfo;
    m_variants.clear();
    m_allowedProfileIDs.clear();
}
/*!
  Returns the unique application identifier \c appId.
 */
QString QTmApplication::appId() {
    return m_appId;
}
/*!
  Returns the Application name.
 */
QString QTmApplication::name() {
    return m_name;
}
void QTmApplication::setName(QString name) {
    m_name = name;
}
/*!
  Returns the supported variants in a comma separated list.
 */
QStringList QTmApplication::variants() {
    return m_variants;
}
void QTmApplication::setVariants(QStringList variants) {
    m_variants = variants;
}
/*!
  Returns the application provider name.
 */
QString QTmApplication::providerName() {
    return m_providerName;
}
void QTmApplication::setProviderName(QString providerName) {
    m_providerName = providerName;
}
/*!
  Returns the application provider URL.
 */
QUrl QTmApplication::providerUrl() {
    return m_providerUrl;
}
void QTmApplication::setProviderUrl(QUrl providerUrl) {
    m_providerUrl = providerUrl;
}
/*!
  Returns the application description.
 */
QString QTmApplication::description() {
    return m_description;
}
void QTmApplication::setDescription(QString description) {
    m_description = description;
}
/*!
  Returns the application icon list.
 */
QList<QTmIcon *> QTmApplication::iconList() {
    return m_iconList;
}
void QTmApplication::setIconList(QList<QTmIcon *> iconList) {
    m_iconList = iconList;
}
/*!
  Returns the allowed profile IDs.
 */
QList<int> QTmApplication::allowedProfileIDs() {
    return m_allowedProfileIDs;
}
void QTmApplication::setAllowedProfileIDs(QList<int> allowedProfileIDs) {
    m_allowedProfileIDs = allowedProfileIDs;
}
/*!
  Returns the remote protocol information.
 */
QTmRemotingInfo *QTmApplication::remotingInfo() {
    return m_remotingInfo;
}
void QTmApplication::setRemotingInfo(QTmRemotingInfo *remotingInfo) {
    m_remotingInfo = remotingInfo;
}
/*!
  Returns the application certificate URL.
 */
QUrl QTmApplication::appCertificateURL() {
    return m_appCertificateURL;
}
void QTmApplication::setAppCertificateURL(QUrl appCertificatURL) {
    m_appCertificateURL = appCertificatURL;
}
/*!
  Returns the additional application information.
 */
QTmAppInfo *QTmApplication::appInfo() {
    return m_appInfo;
}
void QTmApplication::setAppInfo(QTmAppInfo *appInfo) {
    m_appInfo = appInfo;
}
/*!
  Returns the additional display information.
 */
QTmDisplayInfo *QTmApplication::displayInfo() {
    return m_displayInfo;
}
void QTmApplication::setDisplayInfo(QTmDisplayInfo *displayInfo) {
    m_displayInfo = displayInfo;
}
/*!
  Returns the additional audio information.
 */
QTmAudioInfo *QTmApplication::audioInfo() {
    return m_audioInfo;
}
void QTmApplication::setAudioInfo(QTmAudioInfo *audioInfo) {
    m_audioInfo = audioInfo;
}
/*!
  Return the resource status information. This is used for audio resources only, e.g. BT or RTP server or clients.
 */
QTmGlobal::TmResourceStatus QTmApplication::resourceStatus() {
    return m_resourceStatus;
}
void QTmApplication::setResourceStatus(QTmGlobal::TmResourceStatus resourceStatus) {
    m_resourceStatus = resourceStatus;
}
/*!
  Return the status information.
 */
QTmGlobal::TmStatus QTmApplication::status() {
    return m_status;
}
void QTmApplication::setStatus(QTmGlobal::TmStatus status) {
    m_status = status;
}
/*!
  Returns true if the application advertisement has been signed; otherwise false is returned.
 */
bool QTmApplication::signature() {
    return m_signature;
}
/*!
  Returns the result of the application advertisement's signature verification.
  Returns false if no signature is available.
 */
bool QTmApplication::validated() {
    return m_validated;
}
/*!
  Returns the URL, where the remote application can be found.
  Must be used only after the application has been launched.
 */
QUrl QTmApplication::url() {
    return m_url;
}
void QTmApplication::setUrl(QUrl url) {
    m_url = url;
}
/*!
  Returns the identifier of the remote server.
 */
int QTmApplication::remoteServerId() {
    return m_remoteServerId;
}
