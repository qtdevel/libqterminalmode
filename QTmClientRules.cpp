/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include <QHash>
#include "QTmClientRules.h"

/*!
  \class QTmClientRules

  \brief The QTmClientRules class provides access to the driver distraction rules, utilized by the TmClientProfile:1 UPnP service.

  The Terminal Mode client can specify driver distraction rules which are to be enforced by the Terminal Mode server in order to
  ensure a safe driving experience. QTmClientRules is primarily utilized through the QTmClientProfile class.

  This class represents the driver distraction rules encapsulated within the A_ARG_TYPE_ClientProfile variable of
  TmClientProfile:1 UPnP service.
*/

/*!
  Class constructor.
 */
QTmClientRules::QTmClientRules()
{

}

/*!
  Class destructor.
 */
QTmClientRules::~QTmClientRules()
{

}

/*!
  Get the rule identified by \a ruleId.
  Returns QString specifying the rule value.
 */
QString QTmClientRules::getRule(int ruleId)
{
    if(!m_rulesHash.contains(ruleId))
        return 0;
    return (QString)m_rulesHash.value(ruleId);
}

/*!
  Insert a rule identified by \a ruleId and having value
  \a ruleValue.
 */
void QTmClientRules::setRule(int ruleId, QString ruleValue)
{
    m_rulesHash.insert(ruleId, ruleValue);
}

/*!
  Delete rule identified by \a ruleId.
 */
void QTmClientRules::deleteRule(int ruleId)
{
    m_rulesHash.remove(ruleId);
}

/*!
  Get all the rules specified by the Terminal Mode client.
  Returns a QHash object containing all rules where the
  rule Id (of type int) is the key and rule value
  (of type QString) is the value.
 */
QHash<int, QString> QTmClientRules::getAllRules()
{
    return m_rulesHash;
}
