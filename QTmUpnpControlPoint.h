/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#ifndef QTMUPNPCONTROLPOINT_H
#define QTMUPNPCONTROLPOINT_H

#include <QtDebug>
#include <QDomDocument>
#include <QList>
#include <QReadWriteLock>
#include <QNetworkAccessManager>
#include <QtNetwork/QUdpSocket>
#include <netinet/in.h>

#include "QTmGlobal.h"
#include "QUpnpHttpServer.h"

#include "QTmApplication.h"
#include "QTmRemoteServer.h"
#include "QTmClientProfile.h"

//Uncomment to enable SSDP otherwise only M-SEARCH is used for UPnP Discovery
//#define ENABLE_SSDP_RECEIVE            1

class QTmRemoteServer;
class QTmUpnpControlPointTx;

class QTmUpnpControlPoint : public QObject
{
    Q_OBJECT
public:
    explicit QTmUpnpControlPoint(QObject *parent = 0);
    ~QTmUpnpControlPoint();
    bool connectUpnp(QString interfaceName, QHostAddress interfaceAddress);
    void disconnectUpnp(QHostAddress interfaceAddress);
    bool sendMSearchQuery(QHostAddress interfaceAddress);
    QTmRemoteServer* remoteServerById(int remoteServerId);
    QTmRemoteServer* remoteServerByUsn(const QString &usn);

signals:
    void remoteServerDeviceAdded(QTmRemoteServer *remoteServerDevice);
    void remoteServerDeviceRemoved(QTmRemoteServer *remoteServerDevice);

private slots:
    void readDiscoveryMessage();
    void readDeviceDescription(QNetworkReply *reply);

private:
    int setupUpnpIPv4(QString interfaceName, QHostAddress interfaceAddress);
    int setupUpnpIPv6(QString interfaceName, QHostAddress interfaceAddress);
    void readMessage(QUdpSocket *socket);
    void parseMessage(const QString &buffer);
    static QHostAddress getLocalAddress(QHostAddress address);

    QList<QTmRemoteServer*> m_serverList;                        /**< List of remote server devices*/
    QReadWriteLock          m_serverLock;

    QUdpSocket             *m_socket;                            /**< Multi-cast socket for listening to UPnP SSDP advertisements */
    QHash<QHostAddress, QTmUpnpControlPointTx*> m_socketMap;     /**< Map of multi-cast sockets for sending UPnP M-SEARCH queries */
    QReadWriteLock          m_socketLock;
    QUpnpHttpServer         m_eventingHttpServer;                /**< HTTP Server of UPnP Control Point - used for receiving UPnP event notification messages from Terminal Mode server */
    QNetworkAccessManager   m_netManager;
    friend class            QTmUpnpControlPointTx;
};

class QTmUpnpControlPointTx : public QObject
{
    Q_OBJECT
public:
    explicit QTmUpnpControlPointTx(QTmUpnpControlPoint *controlPoint, QUdpSocket *socket);
    ~QTmUpnpControlPointTx();

    bool sendMSearchQuery();

private slots:
    void readUnicastResponse();

private:
    QTmUpnpControlPoint    *m_controlPoint;
    QUdpSocket             *m_socketS;
    struct sockaddr_in      groupSock;
    struct sockaddr_in6     groupSock6;
};

#endif // QTMUPNPCONTROLPOINT_H
