/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA 02111-1307, USA.
**
****************************************************************************/

#include <QStringList>
#include "QTmNetworkObserver.h"

QTmNetworkObserver::QTmNetworkObserver(QObject *parent, bool monitorIPv4, bool monitorIPv6):
    QObject(parent)
{
    m_monitorIPv4 = monitorIPv4;
    m_monitorIPv6 = monitorIPv6;
    connect(&m_configManager, SIGNAL(configurationAdded(const QNetworkConfiguration&)),
            this,             SLOT  (addConfiguration(const QNetworkConfiguration&)),
            Qt::AutoConnection);
    connect(&m_configManager, SIGNAL(configurationChanged(const QNetworkConfiguration&)),
            this,             SLOT  (updateConfiguration(const QNetworkConfiguration&)),
            Qt::AutoConnection);
    connect(&m_configManager, SIGNAL(configurationRemoved(const QNetworkConfiguration&)),
            this,             SLOT  (removeConfiguration(const QNetworkConfiguration&)),
            Qt::AutoConnection);
}

QTmNetworkObserver::~QTmNetworkObserver() {}

bool QTmNetworkObserver::monitorIPv4()
{
    return m_monitorIPv4;
}

bool QTmNetworkObserver::monitorIPv6()
{
    return m_monitorIPv6;
}

void QTmNetworkObserver::setMonitorIPv4(bool enabled)
{
    m_monitorIPv4 = enabled;
}

void QTmNetworkObserver::setMonitorIPv6(bool enabled)
{
    m_monitorIPv6 = enabled;
}

void QTmNetworkObserver::checkNetworks() {
    QList<QNetworkConfiguration> configs = m_configManager.allConfigurations();
    foreach (QNetworkConfiguration config, configs) {
        updateConfiguration(config);
    }
}

void QTmNetworkObserver::addConfiguration(const QNetworkConfiguration &config) {
    if (!config.isValid())
        return;
#ifdef QT_DEBUG_TM_NETWORK_OBSERVER
    qDebug() << "QTmNetworkObserver:" << "add network configuration" << config.name();
#endif
    update(config.name());
}

void QTmNetworkObserver::updateConfiguration(const QNetworkConfiguration &config) {
#ifdef QT_DEBUG_TM_NETWORK_OBSERVER
    qDebug() << "QTmNetworkObserver:" << "update network configuration" << config.name() << config.isValid();
#endif
    if (config.isValid())
    {
        update(config.name());
    }
    else
    {
        remove(config.name());
    }
}

void QTmNetworkObserver::removeConfiguration(const QNetworkConfiguration &config) {
#ifdef QT_DEBUG_TM_NETWORK_OBSERVER
    qDebug() << "QTmNetworkObserver:" << "remove network configuration" << config.name();
#endif
    remove(config.name());
}


void QTmNetworkObserver::update(const QString &name1) {
    //Tokenize into list of strings (To extract interface names which have "auto" before them. Example: "Auto eth0")
    QStringList tokenList = name1.split(" ");
    if(tokenList.length() == 0)
        return;
    QString name = tokenList.last();
    QNetworkInterface interface = interfaceFromName(name);
    if (!interface.isValid()) {
#ifdef QT_DEBUG_TM_NETWORK_OBSERVER
        qDebug() << "QTmNetworkObserver: interface" << name << "is not valid!";
#endif
        remove(name);
        return;
    }
#ifdef QT_DEBUG_TM_NETWORK_OBSERVER
    qDebug() << "QTmNetworkObserver:" << "update network interface" << interface.name() << "(" << interface.humanReadableName() << ")";
#endif
    // add addresses
    QList<QNetworkAddressEntry> entries = interface.addressEntries();
    foreach (QNetworkAddressEntry entry, entries) {
        if (m_addressHash.contains(name, entry))
            continue;

       //Based on monitoring settings allow IPv4 and/or IPv6 interface addresses to be used
       if ((entry.ip().protocol() == QAbstractSocket::IPv4Protocol && m_monitorIPv4) || (entry.ip().protocol() == QAbstractSocket::IPv6Protocol && m_monitorIPv6)) {
#ifdef QT_DEBUG_TM_NETWORK_OBSERVER
            qDebug() << "QTmNetworkObserver:" << "add address" << entry.ip().toString();
#endif
            m_addressHash.insert(name, entry);
            emit addedAddress(name, entry);
        }
#ifdef QT_DEBUG_TM_NETWORK_OBSERVER
        else
        {
            if(m_monitorIPv4 == true)
                qDebug() << "QTmNetworkObserver:" << entry.ip().toString() << " is not an IPv4 address";
            if(m_monitorIPv6 == true)
                qDebug() << "QTmNetworkObserver:" << entry.ip().toString() << " is not an IPv6 address";
        }
#endif
    }
    // remove addresses
    QMultiHash<QString, QNetworkAddressEntry>::iterator i = m_addressHash.find(name);
    while (i != m_addressHash.end() && i.key() == name) {
        QNetworkAddressEntry address = i.value();
        if (!entries.contains(address)) {
#ifdef QT_DEBUG_TM_NETWORK_OBSERVER
            qDebug() << "QTmNetworkObserver:" << "remove address" << address.ip().toString();
#endif
            emit removedAddress(address);
            i = m_addressHash.erase(i);
        } else {
            ++i;
        }
    }
}

void QTmNetworkObserver::remove(const QString &name) {
    QMultiHash<QString, QNetworkAddressEntry>::iterator i;
    for (i = m_addressHash.find(name); i != m_addressHash.end() && i.key() == name; ++i) {
#ifdef QT_DEBUG_TM_NETWORK_OBSERVER
        qDebug() << "QTmNetworkObserver:" << "remove address" << i.value().ip().toString();
#endif
        emit removedAddress(i.value());
    }
    m_addressHash.remove(name);
}

QNetworkInterface QTmNetworkObserver::interfaceFromName(const QString &name) {
    QList<QNetworkInterface> interfaces = QNetworkInterface::allInterfaces();
    foreach (QNetworkInterface interface, interfaces) {
        if (name == interface.name() || name == interface.humanReadableName()) {
            return interface;
        }
    }
    return QNetworkInterface();
}


QNetworkConfiguration::BearerType QTmNetworkObserver::bearerType(QString interfaceName) {
    QMultiHash<QString, QNetworkAddressEntry>::iterator i;

    QList<QNetworkConfiguration> networkConfigs = m_configManager.allConfigurations();
#ifdef QT_DEBUG_TM_NETWORK_OBSERVER
    qDebug() << "QNetworkConfiguration::BearerType:" << "Lookup" << interfaceName;
#endif
    foreach (QNetworkConfiguration networkConfig, networkConfigs) {
        QStringList tokenList = networkConfig.name().split(" ");
        if(tokenList.length() == 0)
            continue;
        QString name = tokenList.last();
        if (name == interfaceName) {
#ifdef QT_DEBUG_TM_NETWORK_OBSERVER
            qDebug() << "QNetworkConfiguration::BearerType:" << "--> Identified network configuration" << networkConfig.bearerTypeName();
#endif
            return networkConfig.bearerType();
        }
    }
#ifdef QT_DEBUG_TM_NETWORK_OBSERVER
    qDebug() << "QNetworkConfiguration::BearerType:" << "--> Cannot identify network configuration";
#endif
    return QNetworkConfiguration::BearerUnknown;
}
